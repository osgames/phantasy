/* 
 * File:   MenuController.cpp
 * Author: User
 * 
 * Created on March 30, 2012, 11:03 PM
 */

#include "MenuController.h"

MenuController::MenuController() {
    
}

MenuController::MenuController(const MenuController& orig) {
}

MenuController::~MenuController() {
}

int MenuController::returnChoiceWithRangeCheck(int menuRange, int menuX, int menuY, SDL_Surface *drawSurface, bool returnOutOfRange, InputController *inputController) {
    
    inputController->allButtonsToUnpress();
    
    //TODO framerate sync
    //TODO button animation
    
    int outputChoice=0;
    
    SDL_Rect buttonRect;
    

    buttonRect.w=kMenuButtonSize-4;
    buttonRect.h=kMenuButtonSize-4;
    
    while(true) {
 

        
        buttonRect.x=menuX+kLeftMenuButtonOffset+2;

        for (int a=0;a<menuRange;a++) {
            buttonRect.y=menuY+kUpMenuButtonOffset+2+a*20; 
            if (a==outputChoice)
                SDL_FillRect(drawSurface, &buttonRect, 0xFFFF0000);
            else
                SDL_FillRect(drawSurface, &buttonRect, 0xFF000000);        
        }
        
        
        
        inputController->pollEvents();
        if (inputController->getEscapeButton()) {
            inputController->allButtonsToUnpress();
            return kMenuChoiceExit;
        }
        else if (inputController->getApplicationQuit()) {
            return kMenuChoiceExit;
        }
        switch (inputController->getLastCommand()) {
            
            case kCommandGoUp:
                
                outputChoice--;
                inputController->allButtonsToUnpress();
                break;
                
            case kCommandGoDown:
                
                outputChoice++;
                inputController->allButtonsToUnpress();
                break;
                
            case kCommandYesButton:
                
                inputController->allButtonsToUnpress();
                return outputChoice;
            
        }
        
        if (outputChoice<0)
            if (returnOutOfRange)
                return -1;
            else
                outputChoice=menuRange-1;
    
        if (outputChoice>menuRange-1)
            if (returnOutOfRange)
                return menuRange-1;
            else
                outputChoice=0;
    
    
        SDL_Flip(drawSurface);       
        
    }
    
}

int MenuController::returnChoice(int menuRange, int menuX, int menuY, SDL_Surface *drawSurface, InputController *inputController) {
    
    return MenuController::returnChoiceWithRangeCheck(menuRange, menuX, menuY, drawSurface, false, inputController);
    
}
