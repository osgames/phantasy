/* 
 * File:   FileSystemManager.h
 * Author: User
 *
 * Created on May 6, 2012, 12:39 AM
 */

#ifndef FILESYSTEMMANAGER_H
#define	FILESYSTEMMANAGER_H

#include <stdlib.h>
#include "Const.h"
#include "debuglog.h"
#ifdef WINDOWS32_BUILD
#include "win32-dirent.h"
#else
#include "dirent.h"
#endif

#include <map>
#include <stdio.h>
#include <string.h>

#include "ConfigParser.h"

using namespace std;

class FileSystemManager {
public:
    FileSystemManager();
    FileSystemManager(const FileSystemManager& orig);
    virtual ~FileSystemManager();
    
    static int getDirectoryList(char *directoryPath, char **gameModulesDirectoryList);
    static void getParsedConfigFilesFromParentDirectory(char *parentDirectoryPath,map<string,string>parsedConfigFiles[kMaxGameModules]);
    
    
private:

    
};

#endif	/* FILESYSTEMMANAGER_H */

