/* 
 * File:   GameSkill.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 7:13 PM
 */

#ifndef GAMESKILL_H
#define	GAMESKILL_H

#include "GameDatabaseItem.h"

class GameSkill : public GameDatabaseItem {
public:
    GameSkill();
    GameSkill(const GameSkill& orig);
    virtual ~GameSkill();
private:

};

#endif	/* GAMESKILL_H */

