/* 
 * File:   StringParser.h
 * Author: User
 *
 * Created on July 21, 2012, 8:50 PM
 */

#ifndef STRINGPARSER_H
#define	STRINGPARSER_H

#include "debuglog.h"
#include "sys/types.h"

#include <vector>
#include <string>

using namespace std;

class StringParser {
public:
    StringParser();
    StringParser(const StringParser& orig);
    virtual ~StringParser();
    
    static vector<string> splitTextToVectorBySeparator(char *inputMultilineText, char *splitByCharacter);
    
private:

};

#endif	/* STRINGPARSER_H */

