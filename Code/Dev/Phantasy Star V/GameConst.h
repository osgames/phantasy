/* 
 * File:   GameConst.h
 * Author: User
 *
 * Created on March 31, 2012, 12:32 AM
 */

#ifndef GAMECONST_H
#define	GAMECONST_H

#include <sys/types.h>

#define kMaximumItemsInventory 20;

#ifdef	__cplusplus
extern "C" {
#endif

#define kMessageInventoryEmpty "Inventory are empty"
#define kMessageTeamEmpty "Player team are empty"

#define kHeroStatusHealth "Health"
#define kHeroStatusWindowName "Name"
#define kHeroStatusWindowJob "Job"
#define kHeroStatusWindowMoney "Money"
#define kHeroStatusWindowAttackPower "Attack Power"
#define kHeroStatusWindowDefencePower "Defence Power"
#define kHeroStatusWindowHealth "Health"
#define kHeroStatusWindowTechPoints "Tech Points"
#define kHeroStatusWindowMagicPoints "Magic Points"
#define kHeroStatusExperience "Experience"
#define kHeroStatusLevel "Level"
    
    
    struct ShopItem {
        
        void* item;
        int price;
        
    };  
    
    
    
enum ShopBuySellMenu {

    kGameplayBuy,
    kGameplaySell,
    kShopBuySellMenuLength
    
};

enum YouSureMenu {

    kMenuItemYes,
    kMenuItemNo,
    kYouSureMenuLength
    
};

enum {
    kGameModuleMainMenuChoiceNewGame,
    kGameModuleMainMenuChoiceLoadGame,
    kGameModuleMainMenuChoiceSaveGame,
    kGameModuleMainMenuChoiceOptions,
    kGameModuleMainMenuCredits,
    kGameModuleMainMenuQuit,
    kGameMenuMainMenuLength
};

enum GameplayMenu {
    
    kGameplayMenuStatus,
    kGameplayMenuInventory,
    kGameplayMenuTechnique,
    kGameplayMenuEquipment,
    kGameplayMenuMagic,
    kGameplayMenuSkills,
    kGameplayMenuLength
    
};

enum GameItemTypes {
    
    kGameItemTypeHealLivingCreatures,
    kGameItemTypeHealAnything,
    kGameItemTypeSmallGun,
    kGameItemGirlLightGear,
    kGameItemTypeLength
    
};

enum GameItemEquipPositions {
    
    kGameItemEquipPositionHead,
    kGameItemEquipPositionRightHand,
    kGameItemEquipPositionLeftHand,
    kGameItemEquipPositionBody,
    kGameItemEquipPositionOneHand,   
    kGameItemEquipPositionTwoHands,
    kGameItemEquipPositionLength
    
};

enum PlayerBattleModeMainMenu {
    
    kBattleMenuAttack,
    kBattleMenuRunaway,
    kBattleMenuLength
    
};

enum BattleModeActionID {
    
    kActionNothing,
    kActionDefence,
    kActionAttack,
    kActionRunaway,
    kActionUseTech,
    kActionUseMagic,
    kActionUseItem
    
};

    typedef enum GameItemType {
    
        kGameItemTypeUnknown,
        kGameItemTypeSword,
        kGameItemTypeHeal,
        kGameItemTypeGear,
        kGameItemTypeDagger,
        kGameItemTypeShotgun
                
    
    } GameItemType;

#define kGameItemNameLength 64
#define kGameItemHumanReadableLength 256
    
    
struct ShowItemResultStruct {
    
    uint type;
    
};
    
struct GameItemStruct {
    
    char name[64];
    char humanReadableName[256];
    GameItemType gameItemType;
    uint minimumEffect;
    uint maximumEffect;
    uint price;    
    
};

struct BattleModeAction {
    
    void *master;
    void *target;
    
    int action;
    int actionAdditionalIndex;
    
    
};
    
    enum BattleModeResult {
        
        kVictory,
        kLose,
        kRunaway
        
    };
    
    enum BattleModeSide {
        
        kPlayerTeam,
        kEnemyTeam,
        kNeutral
        
    };
    
enum {
    
    kMenuInventoryButton
    
};

enum {
    kItemMenuUse,
    kItemMenuDescription,
    kItemMenuDrop,
    kItemMenuCount
};

extern const char *YesNoMenuString[];
extern const char *BuySellMenuString[];
extern const char *GameplayMenuString[];
extern const char *PlayerBattleModeMainMenuString[];
extern const char *kGameMenuMainMenuItems[];
extern const char *kItemMenu[];
extern const char *kGameItemTypesStrings[];
extern const char *kGameItemEquipPositionsStrings[];

#ifdef	__cplusplus
}
#endif

#endif
