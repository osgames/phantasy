/* 
 * File:   TilesRender.cpp
 * Author: demensdeum
 * 
 * Created on December 3, 2011, 1:03 AM
 * 
 * Tiles Render renders tiles on SDL surface
 * 
 */

#include "TilesRender.h"


TilesRender::TilesRender() {
}

TilesRender::TilesRender(const TilesRender& orig) {
}

TilesRender::~TilesRender() {
}

void TilesRender::drawTile(Tile *tileToDraw,int drawX,int drawY,SDL_Surface *drawSurface) {
    
    //draw tile
    //debugLog("draw tile");
    
    SDL_Surface *tileImage=tileToDraw->getImage();
    SDL_Rect source;
    source.x=0;
    source.y=0;
    source.w=tileImage->w;
    source.h=tileImage->h;
    
    SDL_Rect destination;
    destination.x=drawX;
    destination.y=drawY;
    destination.w=source.w;
    destination.h=source.h;
    
    SDL_BlitSurface(tileImage,&source,drawSurface,&destination);
    
}

void TilesRender::renderTilesFromPositionToSurface(GameObjectPosition *position,TilesArray *tilesArray,SDL_Surface *drawSurface) {
    
    int surfaceWidth=drawSurface->w;
    int surfaceHeight=drawSurface->h;

    
    int tileX=position->getX();
    int tileY=position->getY();
    
    int drawX=0;
    int drawY=0;
    
    bool draw=true;
    
    debugLog("begin render");
    
    while (draw) {
        
        //draw tiles
        Tile *nowTile=tilesArray->getTile(tileX,tileY);
        
        //debugLog("in loop");
        
        this->drawTile(nowTile,drawX,drawY,drawSurface);

        
        if (drawX>surfaceWidth) {
            
            if (drawY>surfaceHeight) {
            
                //stop drawing
                debugLog("stop");
                break;
                
            }
            else {
                
                drawX=0;
                drawY+=nowTile->getHeight();
                tileY++;
                
            }
            
        }
        else {
            
            drawX+=nowTile->getWidth();
            tileX++;
            
        }

        
        
    }
    
    
    
}
