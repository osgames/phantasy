/* 
 * File:   PlayersProfilesManager.h
 * Author: demensdeum
 *
 * Created on January 27, 2013, 12:21 AM
 */

#ifndef PLAYERSPROFILESMANAGER_H
#define	PLAYERSPROFILESMANAGER_H

#include "Const.h"
#include "FileSystemManager.h"

class PlayersProfilesManager {
public:
    PlayersProfilesManager();
    PlayersProfilesManager(const PlayersProfilesManager& orig);
    virtual ~PlayersProfilesManager();
    
    static char** getPlayersProfiles();
    static char** getSavegamesForPlayerProfileForGameModule(char *playerProfileName,char *gameModuleName);
    static bool isGameModuleResourceExistForPath(char *gameModuleResourcePath, char *playerProfileName);
    static void addNewPlayerProfile(char *newPlayerName);
    static void removePlayerProfile(char *playerNameToRemove);    
    
private:

};

#endif	/* PLAYERSPROFILESMANAGER_H */

