/* 
 * File:   GameDatabase.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:06 PM
 */

#ifndef GAMEDATABASE_H
#define	GAMEDATABASE_H

#include <map>
#include "Const.h"
#include "debuglog.h"
#include "ConfigParser.h"
#include <string>
#include <cstdlib>
#include "StringParser.h"
#include "ConfigParser.h"

using namespace std;

class GameDatabase {
public:
    GameDatabase();
    GameDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory);
    GameDatabase(const GameDatabase& orig);
    virtual ~GameDatabase();
    
  
    
    void loadDatabaseFromDatabaseFile();
  
    
private:

    string databaseFilePath;
    string databaseResourcesDirectory;
    

    void loadItemFromResourcesDirectoryWithName(string itemName);
    virtual void addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary);  
    void loadItemsFromListString(string rawListString);  
    
};

#endif	/* GAMEDATABASE_H */

