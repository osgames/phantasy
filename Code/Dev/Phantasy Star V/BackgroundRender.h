/* 
 * File:   BackgroundRender.h
 * Author: User
 *
 * Created on May 13, 2012, 12:01 AM
 */

//TODO implement background rendering with image, like in Phantasy Star 3 and Phantasy Star 4

#ifndef BACKGROUNDRENDER_H
#define	BACKGROUNDRENDER_H

#include <SDL/SDL.h>

class BackgroundRender {
public:
    BackgroundRender();
    BackgroundRender(const BackgroundRender& orig);
    virtual ~BackgroundRender();
    
    void render(SDL_Surface *drawSurface);
    
private:

};

#endif	/* BACKGROUNDRENDER_H */

