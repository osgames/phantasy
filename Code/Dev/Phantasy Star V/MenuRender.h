/* 
 * File:   MenuRender.h
 * Author: demensdeum
 *
 * Created on March 25, 2012, 6:05 PM
 */

#include <SDL/SDL.h>

#ifndef MENURENDER_H
#define	MENURENDER_H

class MenuRender {
public:
    MenuRender();
    MenuRender(const MenuRender& orig);
    virtual ~MenuRender();
    
    static void makeMenuWithButtonsOrNot(char **items, int drawX, int drawY, SDL_Surface *drawSurface, bool selectRectangles);
    static void makeMenu(char **items, int drawX, int drawY, SDL_Surface *drawSurface);
    static void makeWindow(char **items, int drawX, int drawY, SDL_Surface *drawSurface);
    
private:

};

#endif	/* MENURENDER_H */

