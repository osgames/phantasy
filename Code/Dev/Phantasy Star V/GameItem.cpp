/* 
 * File:   GameItem.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:49 PM
 */

#include "GameItem.h"

GameItem::GameItem() {
    equipment=false;
}

GameItem::GameItem(const GameItem& orig) {
}

GameItem::~GameItem() {
}

//get
int GameItem::getPrice() {
    return price;
}

int GameItem::getItemType() {
    return itemType;
}

int GameItem::getEquipmentPosition() {
    return equipmentPosition;
}
    
bool GameItem::getEquipment() {
    return equipment;
}

//set
void GameItem::setPrice(int newPrice) {
    price=newPrice;
}

void GameItem::setItemType(int newItemType) {
    itemType=newItemType;
}
void GameItem::setEquipmentPosition(int newEquipPosition) {
    equipmentPosition=newEquipPosition;
}

void GameItem::setEquipment(bool newEquipment) {
    equipment=newEquipment;
}