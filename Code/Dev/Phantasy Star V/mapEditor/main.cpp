#include <cstdlib>
#include "../ModulesManager.h"
#include "../MapEditor.h"

#include "../GameModuleSettings.h"

#include "../debuglog.h"
#include <map>

#include "../ugl.h"
#include "../ScriptEngine.h"
#include "../Const.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    ModulesManager *modulesManager=new ModulesManager();
    modulesManager->refreshModulesList();
    
    MapEditor *mapEditor=new MapEditor();
    mapEditor->startMapEditor();
    
    return 0;
}
