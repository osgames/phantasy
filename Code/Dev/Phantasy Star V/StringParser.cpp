/* 
 * File:   StringParser.cpp
 * Author: User
 * 
 * Created on July 21, 2012, 8:50 PM
 */

#include "StringParser.h"

StringParser::StringParser() {
}

StringParser::StringParser(const StringParser& orig) {
}

StringParser::~StringParser() {
}

vector<string> StringParser::splitTextToVectorBySeparator(char *inputMultilineText, char *splitByCharacter) {
    
    string multilineString=inputMultilineText;
    vector<string>outputVector;
    
    debugLog("Parse string:"<<multilineString<<"\n");
    
    size_t leftPosition=0;
    size_t rightPosition=0;
    
    while (rightPosition!=string::npos) {
        
        rightPosition=multilineString.find(splitByCharacter,leftPosition);
        string newScript=multilineString.substr(leftPosition,rightPosition-leftPosition);
        debugLog("new parsed word:"<<newScript<<"\n");
        debugLog("Left position:"<<leftPosition<<"\n");
        debugLog("Right position:"<<rightPosition<<"\n");
        debugLog("String npos:"<<string::npos<<"\n");
        
        leftPosition=rightPosition+1;
        
        if (newScript.length()>0)
                outputVector.push_back(newScript);
        
    }
    
    return outputVector;
    
}
