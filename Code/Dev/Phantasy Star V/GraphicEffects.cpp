/* 
 * File:   GraphicEffects.cpp
 * Author: demensdeum
 * 
 * Created on December 17, 2011, 10:30 PM
 */

#include "GraphicEffects.h"
#include "GameEngine.h"

GraphicEffects::GraphicEffects() {
}

GraphicEffects::GraphicEffects(const GraphicEffects& orig) {
}

GraphicEffects::~GraphicEffects() {
}

void GraphicEffects::fadeIn(SDL_Surface* drawSurface, void *callback) {
    
    //fade from black
    
    SDL_Surface* originalSurface = SDL_ConvertSurface(drawSurface, drawSurface->format, SDL_SWSURFACE);
    

    
    for (int a=0;a<255;a++) {
        
        SDL_FillRect(drawSurface, NULL, SDL_MapRGBA(drawSurface->format, 0, 0, 0, 0));
        SDL_SetAlpha(originalSurface, SDL_SRCALPHA, a);
        SDL_BlitSurface(originalSurface,NULL,drawSurface,NULL);
        
        //TODO callback
        SDL_Flip(drawSurface);
    
        //callback after every frame
        //((GameMaster*)callback)->callbackTest();
        
    }
    
    SDL_FreeSurface(originalSurface);
    
}

void GraphicEffects::fadeOut(SDL_Surface *drawSurface, void* callback) {
    
    //fade out to black
    
    SDL_Surface* originalSurface = SDL_ConvertSurface(drawSurface, drawSurface->format, SDL_SWSURFACE);
    

    
    for (int a=255;a>0;a--) {
        
        SDL_FillRect(drawSurface, NULL, SDL_MapRGBA(drawSurface->format, 0, 0, 0, 0));
        SDL_SetAlpha(originalSurface, SDL_SRCALPHA, a);
        SDL_BlitSurface(originalSurface,NULL,drawSurface,NULL);
        
        //TODO callback
        SDL_Flip(drawSurface);
    
        //callback after every frame
        //((GameMaster*)callback)->callbackTest();
        
    }
    
    SDL_FreeSurface(originalSurface);
    
    
}

void GraphicEffects::rotateZoomAndFadeOut(SDL_Surface* drawSurface, void* callback) {
    
  
        
        SDL_Surface* originalSurface = SDL_ConvertSurface(drawSurface, drawSurface->format, SDL_SWSURFACE);

        double zoom=1;
        
        
        for (int a=0;a<31;a++) {
            

                SDL_Surface *zoomedSurface=rotozoomSurface(originalSurface,a*2,zoom+0.03*a,SMOOTHING_ON); 
                SDL_SetAlpha(zoomedSurface, SDL_SRCALPHA, 255-a*8);

                SDL_Rect source;
                source.x=(zoomedSurface->w-originalSurface->w)/2;
                source.y=(zoomedSurface->h-originalSurface->h)/2;
                source.w=drawSurface->w;
                source.h=drawSurface->h;                
                

                SDL_BlitSurface(zoomedSurface,&source,drawSurface,NULL);
                boxRGBA(drawSurface,0,0,originalSurface->w,originalSurface->h,0,0,0,a*8);

                SDL_Flip(drawSurface);
                
        SDL_FreeSurface(zoomedSurface);
            
        }
        
        SDL_FreeSurface(originalSurface);
        
    
}
