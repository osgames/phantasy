/* 
 * File:   GameTechniqueDatabase.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:53 PM
 */

#ifndef GAMETECHNIQUEDATABASE_H
#define	GAMETECHNIQUEDATABASE_H

#include "GameDatabase.h"
#include "GameTechnique.h"

class GameTechniqueDatabase : public GameDatabase {
public:
    GameTechniqueDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory);
    GameTechniqueDatabase(const GameTechniqueDatabase& orig);
    virtual ~GameTechniqueDatabase();
    
    GameTechnique* getGameTechniqueWithName(string nowItemName);
    vector<GameTechnique*> returnGameTechniqueFromRawString(string itemsListRawString);       
    
private:

    vector<GameTechnique*>gameTechnique;
    void addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary);    
    
};

#endif	/* GAMETECHNIQUEDATABASE_H */

