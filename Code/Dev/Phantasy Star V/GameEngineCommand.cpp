/* 
 * File:   GameEngineCommand.cpp
 * Author: demensdeum
 * 
 * Created on June 24, 2012, 9:15 PM
 */

#include "GameEngineCommand.h"

GameEngineCommand::GameEngineCommand() {
}

GameEngineCommand::GameEngineCommand(const GameEngineCommand& orig) {
}

GameEngineCommand::~GameEngineCommand() {
}

string GameEngineCommand::getArgument() {
    return argument;
}

int GameEngineCommand::getCommandType() {
    
    return commandType;
    
}

void GameEngineCommand::setArgument(string newArgument) {
    
    argument=newArgument;
}

void GameEngineCommand::setCommandType(int newCommandType) {
    
    commandType=newCommandType;
    
}