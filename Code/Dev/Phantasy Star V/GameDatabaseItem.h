/* 
 * File:   GameDatabaseItem.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:07 PM
 */

#ifndef GAMEDATABASEITEM_H
#define	GAMEDATABASEITEM_H

#include <string>

using namespace std;

class GameDatabaseItem {
public:
    GameDatabaseItem();
    GameDatabaseItem(const GameDatabaseItem& orig);
    virtual ~GameDatabaseItem();
    
    //get
    string getName();
    string getHumanReadableName();
    string getDescription();
    
    //set
    void setName(string newName);
    void setHumanReadableName(string newHumanReadableName);
    void setDescription(string newDescription);
    

    
private:
    
    string name;
    string humanReadableName;
    string description;

};

#endif	/* GAMEDATABASEITEM_H */

