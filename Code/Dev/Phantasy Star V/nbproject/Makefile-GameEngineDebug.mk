#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=GameEngineDebug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/InputController.o \
	${OBJECTDIR}/GameModuleSettings.o \
	${OBJECTDIR}/GameMagic.o \
	${OBJECTDIR}/GameObject.o \
	${OBJECTDIR}/GameSkillsDatabase.o \
	${OBJECTDIR}/ScriptEngine.o \
	${OBJECTDIR}/BattleMode.o \
	${OBJECTDIR}/GameManager.o \
	${OBJECTDIR}/FileSystemManager.o \
	${OBJECTDIR}/GameTechniqueDatabase.o \
	${OBJECTDIR}/GameDatabaseItem.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/RandomGenerator.o \
	${OBJECTDIR}/GameEngineCommand.o \
	${OBJECTDIR}/DialogRender.o \
	${OBJECTDIR}/GameMenu.o \
	${OBJECTDIR}/ResourcesManager.o \
	${OBJECTDIR}/MenuRender.o \
	${OBJECTDIR}/GameSkill.o \
	${OBJECTDIR}/ObjectsArray.o \
	${OBJECTDIR}/GameItem.o \
	${OBJECTDIR}/MenuController.o \
	${OBJECTDIR}/ConfigParser.o \
	${OBJECTDIR}/GraphicsSettings.o \
	${OBJECTDIR}/GameMagicDatabase.o \
	${OBJECTDIR}/GameEngine.o \
	${OBJECTDIR}/GraphicEffects.o \
	${OBJECTDIR}/ObjectsRender.o \
	${OBJECTDIR}/Hero.o \
	${OBJECTDIR}/ScriptEngineAPI.o \
	${OBJECTDIR}/RenderLayer.o \
	${OBJECTDIR}/MapsManager.o \
	${OBJECTDIR}/PlayerTeam.o \
	${OBJECTDIR}/GameplayMenuMaster.o \
	${OBJECTDIR}/Fighter.o \
	${OBJECTDIR}/StringParser.o \
	${OBJECTDIR}/ModulesManager.o \
	${OBJECTDIR}/ObjectsSector.o \
	${OBJECTDIR}/Shop.o \
	${OBJECTDIR}/GameTechnique.o \
	${OBJECTDIR}/TilesArray.o \
	${OBJECTDIR}/SDLSurfaceSaverToPNG.o \
	${OBJECTDIR}/BackgroundRender.o \
	${OBJECTDIR}/Tile.o \
	${OBJECTDIR}/GameModulesManager.o \
	${OBJECTDIR}/ObjectsMaster.o \
	${OBJECTDIR}/MapMaster.o \
	${OBJECTDIR}/savepng.o \
	${OBJECTDIR}/GameObjectPosition.o \
	${OBJECTDIR}/GameItemsDatabase.o \
	${OBJECTDIR}/GameConst.o \
	${OBJECTDIR}/TilesRender.o \
	${OBJECTDIR}/GameDatabase.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-O2
CXXFLAGS=-O2

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L../../../../../../cygwin/usr/local/lib -lSDL_image -lSDL_gfx -lSDL_ttf `pkg-config --libs sdl` `pkg-config --libs gl` -lSDL_mixer  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/phantasy_star_v

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/phantasy_star_v: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/phantasy_star_v ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/InputController.o: InputController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/InputController.o InputController.cpp

${OBJECTDIR}/GameModuleSettings.o: GameModuleSettings.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameModuleSettings.o GameModuleSettings.cpp

${OBJECTDIR}/GameMagic.o: GameMagic.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameMagic.o GameMagic.cpp

${OBJECTDIR}/GameObject.o: GameObject.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameObject.o GameObject.cpp

${OBJECTDIR}/GameSkillsDatabase.o: GameSkillsDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameSkillsDatabase.o GameSkillsDatabase.cpp

${OBJECTDIR}/ScriptEngine.o: ScriptEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ScriptEngine.o ScriptEngine.cpp

${OBJECTDIR}/BattleMode.o: BattleMode.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/BattleMode.o BattleMode.cpp

${OBJECTDIR}/GameManager.o: GameManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameManager.o GameManager.cpp

${OBJECTDIR}/FileSystemManager.o: FileSystemManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/FileSystemManager.o FileSystemManager.cpp

${OBJECTDIR}/GameTechniqueDatabase.o: GameTechniqueDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameTechniqueDatabase.o GameTechniqueDatabase.cpp

${OBJECTDIR}/GameDatabaseItem.o: GameDatabaseItem.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameDatabaseItem.o GameDatabaseItem.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/RandomGenerator.o: RandomGenerator.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/RandomGenerator.o RandomGenerator.cpp

${OBJECTDIR}/GameEngineCommand.o: GameEngineCommand.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameEngineCommand.o GameEngineCommand.cpp

${OBJECTDIR}/DialogRender.o: DialogRender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/DialogRender.o DialogRender.cpp

${OBJECTDIR}/GameMenu.o: GameMenu.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameMenu.o GameMenu.cpp

${OBJECTDIR}/ResourcesManager.o: ResourcesManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ResourcesManager.o ResourcesManager.cpp

${OBJECTDIR}/MenuRender.o: MenuRender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/MenuRender.o MenuRender.cpp

${OBJECTDIR}/GameSkill.o: GameSkill.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameSkill.o GameSkill.cpp

${OBJECTDIR}/ObjectsArray.o: ObjectsArray.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ObjectsArray.o ObjectsArray.cpp

${OBJECTDIR}/GameItem.o: GameItem.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameItem.o GameItem.cpp

${OBJECTDIR}/MenuController.o: MenuController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/MenuController.o MenuController.cpp

${OBJECTDIR}/ConfigParser.o: ConfigParser.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ConfigParser.o ConfigParser.cpp

${OBJECTDIR}/GraphicsSettings.o: GraphicsSettings.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GraphicsSettings.o GraphicsSettings.cpp

${OBJECTDIR}/GameMagicDatabase.o: GameMagicDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameMagicDatabase.o GameMagicDatabase.cpp

${OBJECTDIR}/GameEngine.o: GameEngine.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameEngine.o GameEngine.cpp

${OBJECTDIR}/GraphicEffects.o: GraphicEffects.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GraphicEffects.o GraphicEffects.cpp

${OBJECTDIR}/ObjectsRender.o: ObjectsRender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ObjectsRender.o ObjectsRender.cpp

${OBJECTDIR}/Hero.o: Hero.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/Hero.o Hero.cpp

${OBJECTDIR}/ScriptEngineAPI.o: ScriptEngineAPI.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ScriptEngineAPI.o ScriptEngineAPI.c

${OBJECTDIR}/RenderLayer.o: RenderLayer.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/RenderLayer.o RenderLayer.cpp

${OBJECTDIR}/MapsManager.o: MapsManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/MapsManager.o MapsManager.cpp

${OBJECTDIR}/PlayerTeam.o: PlayerTeam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/PlayerTeam.o PlayerTeam.cpp

${OBJECTDIR}/GameplayMenuMaster.o: GameplayMenuMaster.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameplayMenuMaster.o GameplayMenuMaster.cpp

${OBJECTDIR}/Fighter.o: Fighter.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/Fighter.o Fighter.cpp

${OBJECTDIR}/StringParser.o: StringParser.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/StringParser.o StringParser.cpp

${OBJECTDIR}/ModulesManager.o: ModulesManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ModulesManager.o ModulesManager.cpp

${OBJECTDIR}/ObjectsSector.o: ObjectsSector.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ObjectsSector.o ObjectsSector.cpp

${OBJECTDIR}/Shop.o: Shop.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/Shop.o Shop.cpp

${OBJECTDIR}/GameTechnique.o: GameTechnique.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameTechnique.o GameTechnique.cpp

${OBJECTDIR}/TilesArray.o: TilesArray.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/TilesArray.o TilesArray.cpp

${OBJECTDIR}/SDLSurfaceSaverToPNG.o: SDLSurfaceSaverToPNG.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/SDLSurfaceSaverToPNG.o SDLSurfaceSaverToPNG.cpp

${OBJECTDIR}/BackgroundRender.o: BackgroundRender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/BackgroundRender.o BackgroundRender.cpp

${OBJECTDIR}/Tile.o: Tile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/Tile.o Tile.cpp

${OBJECTDIR}/GameModulesManager.o: GameModulesManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameModulesManager.o GameModulesManager.cpp

${OBJECTDIR}/ObjectsMaster.o: ObjectsMaster.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/ObjectsMaster.o ObjectsMaster.cpp

${OBJECTDIR}/MapMaster.o: MapMaster.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/MapMaster.o MapMaster.cpp

${OBJECTDIR}/savepng.o: savepng.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/savepng.o savepng.c

${OBJECTDIR}/GameObjectPosition.o: GameObjectPosition.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameObjectPosition.o GameObjectPosition.cpp

${OBJECTDIR}/GameItemsDatabase.o: GameItemsDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameItemsDatabase.o GameItemsDatabase.cpp

${OBJECTDIR}/GameConst.o: GameConst.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.c) -g `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameConst.o GameConst.c

${OBJECTDIR}/TilesRender.o: TilesRender.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/TilesRender.o TilesRender.cpp

${OBJECTDIR}/GameDatabase.o: GameDatabase.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} $@.d
	$(COMPILE.cc) -g -DDEBUG `pkg-config --cflags sdl` `pkg-config --cflags gl`    -MMD -MP -MF $@.d -o ${OBJECTDIR}/GameDatabase.o GameDatabase.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/phantasy_star_v

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
