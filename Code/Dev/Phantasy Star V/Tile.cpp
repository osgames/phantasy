/* 
 * File:   Tile.cpp
 * Author: demensdeum
 * 
 * Created on December 4, 2011, 4:13 PM
 */

#include "Tile.h"

Tile::Tile() {
    
    image=NULL;
    tileType=kTileTypeUnknown;
    
}

Tile::Tile(const Tile& orig) {
}

Tile::~Tile() {
    
    if (image)
        SDL_FreeSurface(image);
    
}

void Tile::setTileType(int newType) {
    
    tileType=newType;
    
}

int Tile::getTileType() {
    
    return tileType;
    
}

void Tile::setImage(SDL_Surface *newImage) {

    image=newImage;
    
}

void Tile::setImage(char *imagePath) {
    
    SDL_Surface *tempImage=IMG_Load(imagePath);
    if (tempImage==NULL)
        debugLog("Error loading object image:"<<imagePath<<"\n");
    image=SDL_DisplayFormatAlpha(tempImage);
    SDL_FreeSurface(tempImage);
            
}

    
SDL_Surface* Tile::getImage() {
    
    return image;
    
}

int Tile::getWidth() {
    
    return image->w;
    
}

int Tile::getHeight() {
    
    return image->h;
    
}

void Tile::setFullPath(string newFullPath) {
    
    fullPath=newFullPath;
    
}

string Tile::getFullPath() {
    
    return fullPath;
    
}

