/* 
 * File:   PlayerTeam.h
 * Author: User
 *
 * Created on June 29, 2012, 11:14 PM
 */

#ifndef PLAYERTEAM_H
#define	PLAYERTEAM_H

#include <vector>
#include "GameItem.h"
#include "Hero.h"
#include "StringParser.h"
#include "Fighter.h"

using namespace std;

class PlayerTeam {
public:
    PlayerTeam();
    PlayerTeam(const PlayerTeam& orig);
    virtual ~PlayerTeam();
    
    void addHero(Hero *newHero);
    int getHeroesCount();
    Hero* getHeroAtIndex(int index);
    
    signed long long getMoney();
    
    bool checkInventorySpace();
    void addItem(GameItem *newItem);
    int getItemsCount();
    GameItem* getItemAtIndex(int index);
    static PlayerTeam* playerTeamFromHeroesNames(char *gameModule, string heroesNames, GameItemsDatabase *gameItemsDatabase,GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase);
    void addItemsSystem(vector<GameItem*>newItems);
    void addItemSystem(GameItem* newGameItem);
    GameObject* getLeaderGameObject();
    vector<Fighter>getFighters();
    
    void takeMoney(int amount);
    void giveItem(GameItem *nowItem);
    void giveMoney(int amount);
    void removeItemAtIndex(int index);
    void dropItemFromInventory(int itemIndex);
    ShowItemResultStruct useItemFromInventory(int itemIndex);
    void equipItemAtIndexForHeroAtIndex(int equipItemIndex, int heroIndex);
    void equipItemAtIndexForHeroAtIndexSystem(int equipItemIndex, int heroIndex);
    void unequipItemAtEquipmentPositionForHeroAtIndex(int equipItemIndex, int heroIndex);
    
private:

    vector<GameItem*>items;
    vector<Hero*>heroes;
    signed long long money;
    
    
};

#endif	/* PLAYERTEAM_H */

