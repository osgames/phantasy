/* 
 * File:   GameModuleSettings.cpp
 * Author: demensdeum
 * 
 * Created on December 18, 2011, 3:20 PM
 */

#include "GameModuleSettings.h"

GameModuleSettings::GameModuleSettings() {
}

GameModuleSettings::GameModuleSettings(const GameModuleSettings& orig) {
}

GameModuleSettings::~GameModuleSettings() {
}

char *GameModuleSettings::getTitle() {
    
    return gameModuleTitle;
    
}

void GameModuleSettings::setTitle(const char *newGameModuleTitle) {
    
    strncpy(gameModuleTitle,newGameModuleTitle,kMaxStringLength);
    
}

void GameModuleSettings::setTitle(char *newGameModuleTitle) {
    
    strncpy(gameModuleTitle,newGameModuleTitle,kMaxStringLength);
    
}

void GameModuleSettings::setPath(const char *newGameModulePath) {
 
    debugLog("New gameModule path "<<newGameModulePath<<"\n");    
    strncpy(gameModulePath,newGameModulePath,kMaxStringLength);    
    
}

void GameModuleSettings::setPath(char *newGameModulePath) {
    
    debugLog("New gameModule path "<<newGameModulePath<<"\n");
    strncpy(gameModulePath,newGameModulePath,kMaxStringLength);    
    
}

char* GameModuleSettings::getPath() {
    
    return gameModulePath;    
    
}


char *GameModuleSettings::getIcon() {
    
    //TODO implementation
    
    return NULL;
    
}