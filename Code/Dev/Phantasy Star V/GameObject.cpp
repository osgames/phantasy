/* 
 * File:   GameObject.cpp
 * Author: demensdeum
 * 
 * Created on December 7, 2011, 9:36 PM
 */

#include "GameObject.h"
#include "debuglog.h"
#include "Const.h"


#include <SDL/SDL_image.h>
#include <SDL/SDL_gfxBlitFunc.h>
#include <list>


GameObject::GameObject() {
    
    animationFrameNow=0;
    gamePosition=NULL;
    animated=false;
    called=false;
    calledByGiveItem=false;
    touched=false;
    calledByObject=NULL;
    calledByItemID=kObjectIDNone;
    touchedByObject=NULL;
    scriptIndex=kScriptIndexNone;
    parent=false;//child by default
    scriptIndex=-1;
    animationFrameTimeNow=0;
    image=NULL;
    animationFrame=NULL;
    playerTeamMember=false;
}

void GameObject::setPlayerTeamMember() {
    playerTeamMember=true;
}

void GameObject::setNotPlayerTeamMember() {
    playerTeamMember=false;
}

bool GameObject::isPlayerTeamMember() {
    return playerTeamMember;
}

GameObject::GameObject(const GameObject& orig) {
}

GameObject::~GameObject() {
    
    
    if (image && parent)
        SDL_FreeSurface(image);    
    
    if (gamePosition)
        delete gamePosition;
    
    if (animationFrame)
        SDL_FreeSurface(animationFrame);
    
}

bool GameObject::isTouched() {
    
    return touched;
    
}

GameObject* GameObject::getTouchedBy() {
    
    return touchedByObject;
    
}

void GameObject::setTouchedBy(GameObject *touchedBy) {
    
    touchedByObject=touchedBy;
    
}

void GameObject::touch() {
    
    touched=true;
    
}

void GameObject::untouch() {
    
    touched=false;
    
}

void GameObject::setParent() {
    
    parent=true;
    
}

void GameObject::setChild() {
    
    parent=false;
    
}

void GameObject::setScriptIndex(int newScriptIndex) {
    
    scriptIndex=newScriptIndex;
    
}

bool GameObject::isCalled() {
    
    return called;
    
}

GameObject* GameObject::getCalledBy() {
    
    return calledByObject;
    
}

int GameObject::getScriptIndex() {
    
    return scriptIndex;
    
}

GameObject::GameObjectParentStruct GameObject::exportAsParent() {
    
    GameObject::GameObjectParentStruct outputStruct;
    memset(&outputStruct,0,sizeof(GameObject::GameObjectParentStruct));
    strncpy(outputStruct.name,name.c_str(),kMaxObjectNameLength);
    strncpy(outputStruct.fullPath,fullPath.c_str(),kShortStringLength);
    outputStruct.isAnimated=animated;
    outputStruct.animationFrameSize=animationFrameSize;
    outputStruct.animationFramerate=animationFramerate;
    outputStruct.maximumAnimationFrames=maximumAnimationFrames;
            
    return outputStruct;
    
}

GameObject::GameObjectStruct GameObject::exportAsGameObject() {
    
    GameObject::GameObjectStruct outputStruct;
    
    strncpy(outputStruct.parentName,parentName.c_str(),kMaxObjectNameLength);
    outputStruct.animationDirection=animationDirection;
    outputStruct.positionX=this->getX();
    outputStruct.positionY=this->getY();
    outputStruct.scriptIndex=this->getScriptIndex();
    
    return outputStruct;
    
}

void GameObject::setCalledBy(GameObject *newCalledByObject) {
    
    calledByObject=newCalledByObject;
    debugLog("Called by:"<<calledByObject->getName()<<"\n");
    
}

void GameObject::uncall() {
    
    called=false;
    calledByObject=NULL;
    
}

void GameObject::call() {
    
    debugLog("Object called:"<<this->getName()<<"\n");
    called=true;
    
}

void GameObject::setImage(SDL_Surface *newImage) {
    
    image=newImage;
    
}

void GameObject::setImage(char *imagePath) {
    
    SDL_Surface *tempImage=IMG_Load(imagePath);
    if (tempImage==NULL)
        debugLog("Error loading object image:"<<imagePath<<"\n");
    image=SDL_DisplayFormatAlpha(tempImage);
    SDL_FreeSurface(tempImage);
            
}

void GameObject::setFullPath(string newFullPath) {
    
    fullPath=newFullPath;
    
}

string GameObject::getFullPath() {
    
    return fullPath;
    
}

bool GameObject::isAnimated() {
    
    return animated;
    
}

int GameObject::getAnimationFrameSize() {
    return animationFrameSize;
}

int GameObject::getAnimationFramerate() {
    return animationFramerate;
}

int GameObject::getMaximumAnimationFrames() {
    return maximumAnimationFrames;
}

int GameObject::getAnimationDirection() {
    return animationDirection;
}

GameObject* GameObject::getCopy() {
    
    //allocate new object in memory with same properties
    //also does not create copy of SDL_Image
    
    GameObject *outputObject=new GameObject();
    
    outputObject->setImage(this->getImageWithoutAnimation());
    outputObject->setName(this->getName());
    outputObject->setParentName(this->getParentName());
    
    if (this->getPosition()!=NULL) {
        GameObjectPosition *outputObjectPosition=new GameObjectPosition(this->getPosition()->getX(),this->getPosition()->getY());
        outputObject->setPosition(outputObjectPosition);
    }
    
    if (this->isAnimated()) {
            
        outputObject->setAnimation(this->getAnimationFrameSize(),this->getAnimationFramerate(),this->getMaximumAnimationFrames(),this->getAnimationDirection());
        
    }
    
    return outputObject;
    
}

GameObject* GameObject::loadObjectFromFile(char* objectDirectory) {
    
    char imagePath[kMaxStringLength];
    snprintf(imagePath,kMaxStringLength,"%s/%s",objectDirectory,kObjectImageFileDefaultName);
    
    char configPath[kMaxStringLength];
    snprintf(configPath,kMaxStringLength,"%s/%s",objectDirectory,kGameModuleConfigFileName);
    
    map<string,string>objectConfigParsed=ConfigParser::parseConfigFile(configPath,NULL);
    
    //to object properties
    string newObjectName=objectConfigParsed[kObjectParserKeyName];
    
    string rawNewObjectAnimated=objectConfigParsed[kObjectParserKeyAnimated];
    string rawNewObjectAnimationFrameSize=objectConfigParsed[kObjectParserKeyAnimationFrameSize];
    string rawNewObjectAnimationFramerate=objectConfigParsed[kObjectParserKeyAnimationFramerate];
    string rawNewObjectMaximumAnimationFrames=objectConfigParsed[kObjectParserKeyMaximumAnimationFrames];
    string rawNewObjectAnimationDirection=objectConfigParsed[kObjectParserKeyAnimationDirection];

    bool newObjectAnimated=false;
    int newObjectAnimationFrameSize=0;
    int newObjectAnimationFramerate=0;
    int newObjectMaximumAnimationFrames=0;
    int newObjectAnimationDirection=0;
    
    
    if (rawNewObjectAnimationDirection==kObjectParserKeyDirectionDown)
        newObjectAnimationDirection=kObjectDirectionDown;    

    if (rawNewObjectAnimationDirection==kObjectParserKeyDirectionUp)
        newObjectAnimationDirection=kObjectDirectionUp;
    
    if (rawNewObjectAnimationDirection==kObjectParserKeyDirectionRight)
        newObjectAnimationDirection=kObjectDirectionRight;
    
    if (rawNewObjectAnimationDirection==kObjectParserKeyDirectionLeft)
        newObjectAnimationDirection=kObjectDirectionLeft;
    
    
    
    
    debugLog("Object animated: "<<newObjectAnimated<<"\n");
    
    if(rawNewObjectAnimated==kConfigParserTrueValue) {
        
        newObjectAnimated=true;
        newObjectAnimationFrameSize=atoi(rawNewObjectAnimationFrameSize.c_str());
        newObjectAnimationFramerate=atoi(rawNewObjectAnimationFramerate.c_str());
        newObjectMaximumAnimationFrames=atoi(rawNewObjectMaximumAnimationFrames.c_str());
        newObjectAnimationDirection=atoi(rawNewObjectAnimationDirection.c_str());
        
    }
    
    
    debugLog("Load object on path: "<<imagePath<<"\n");
    GameObject *loadedObject=new GameObject();    
        
    debugLog("Object load success for path: "<<imagePath<<"\n");
       
    loadedObject->setImage(imagePath);
        
    //set properties
    loadedObject->setName(newObjectName);
    loadedObject->setParentName(newObjectName);
    loadedObject->setFullPath(objectDirectory);
    if (newObjectAnimated) {
            
        loadedObject->setAnimation(newObjectAnimationFrameSize,newObjectAnimationFramerate,newObjectMaximumAnimationFrames,newObjectAnimationDirection);
            
    }
    //parent by default
    loadedObject->setParent();

    return loadedObject;
    
}

void GameObject::setAnimation(int newAnimationFrameSize, int newAnimationFramerate, int newMaximumAnimationFrames,int newAnimationDirection) {
    
    //TODO add animation disable
    //TODO reinitialize animation frame
    animated=true;
    
    animationFrameSize=newAnimationFrameSize;
    animationFramerate=newAnimationFramerate;
    animationDirection=newAnimationDirection;
    maximumAnimationFrames=newMaximumAnimationFrames;
    
 
    //TODO fix alpha
    //init animation frame
    animationFrame=SDL_CreateRGBSurface(SDL_SWSURFACE,animationFrameSize,animationFrameSize,32,image->format->Rmask,image->format->Gmask,image->format->Bmask,image->format->Amask);    
    
}

void GameObject::setPosition(GameObjectPosition* newGamePosition) {
    
    gamePosition=newGamePosition;
    
}

GameObjectPosition* GameObject::getPosition() {
    
    return gamePosition;
    
}

SDL_Surface* GameObject::returnAnimationFrame() {

    SDL_Rect backgroundRect;
    backgroundRect.x=0;
    backgroundRect.y=0;
    backgroundRect.w=animationFrame->w;
    backgroundRect.h=animationFrame->h;         
        

    
    //TODO fix animation
    
    SDL_Rect source;
    source.x=animationFrameNow*animationFrameSize;
    source.y=animationDirection*animationFrameSize;
    source.w=animationFrameSize;
    source.h=animationFrameSize;
    
    SDL_Rect destination;
    destination.x=0;
    destination.y=0;
    destination.w=animationFrameSize;
    destination.h=animationFrameSize;
    
    //TODO fix blue color around alpha
    //or use colorKey
    
    //clear surface
    SDL_FillRect(animationFrame, &destination,0x000000FF);
    SDL_gfxBlitRGBA(image,&source,animationFrame,&destination);    
    
    //animate
    if (animationFrameTimeNow<animationFramerate)
        animationFrameTimeNow++;
    else {
        
        //switch frame
        if (animationFrameNow<maximumAnimationFrames)
            animationFrameNow++;
        else
            animationFrameNow=0;
        
        animationFrameTimeNow=0;
        
    }
    
    return animationFrame;
    
}
    

SDL_Surface* GameObject::getImageWithoutAnimation() {
    
    return image;
    
}
SDL_Surface* GameObject::getIcon() {
    
    //TODO icon implementation
    
    if (image==NULL)
        debugLog("Can't return icon: Image is NULL\n");
        
    return this->getImage();
}

SDL_Surface* GameObject::getImage() {
    
    
    if (!animated) {
        
        if (image==NULL)
            debugLog("Image is NULL\n");
        
        return image;
    }
    else {
        return returnAnimationFrame();
    }
}

int GameObject::getWidth() {
    
    if (!animated)
        return image->w;
    else return animationFrameSize;
    
}

int GameObject::getHeight() {
    
    if (!animated)
        return image->h;
    else return animationFrameSize;
    
}

string GameObject::getParentName() {
    
    return parentName;
    
}

void GameObject::setParentName(string newParentName) {
    
    parentName=newParentName;
    
}

string GameObject::getName() {
    
    return name;
    
}

void GameObject::setName(string newName) {
    
    name=newName;
    
}

void GameObject::setNowCommand(int newCommand) {
    
    nowCommand=newCommand;
    
}

int GameObject::getNowCommand() {
    
    return nowCommand;
    
}

int GameObject::getMovementTraceCount() {
    return movementTrace.size();
}

GameObject::GameObjectStruct GameObject::getLastGameObjectStructFromMovementTrace() {
    
    return movementTrace.back();
    
}

void GameObject::clearMovementTrace() {
    movementTrace.clear();
}

void GameObject::setX(int newX) {
    
    //TODO remove old GameObjectPosition class
    if (gamePosition==NULL)
        gamePosition=new GameObjectPosition();
    
    gamePosition->setX(newX);
    
    GameObjectStruct newMovementTrace;
    newMovementTrace.positionX=newX;
    newMovementTrace.positionY=this->getY();
    
    movementTrace.push_front(newMovementTrace);
    if (movementTrace.size()>64) {
        movementTrace.pop_back();
    }    
    
}

void GameObject::setY(int newY) {
    
    if (gamePosition==NULL)
        gamePosition=new GameObjectPosition();
    
    gamePosition->setY(newY);

    GameObjectStruct newMovementTrace;
    newMovementTrace.positionX=this->getX();
    newMovementTrace.positionY=newY;
    
    movementTrace.push_front(newMovementTrace);
    if (movementTrace.size()>64) {
        movementTrace.pop_back();
    }     
    
}

void GameObject::goUp() {
    
    GameObjectPosition *position=this->getPosition();
    
    int newPositionY=position->getY()-1;
    this->setY(newPositionY);
    
    animationDirection=kObjectDirectionUp;
    
    
}

void GameObject::setAnimationDirection(int newAnimationDirection) {
    
    animationDirection=newAnimationDirection;
    
}

void GameObject::goLeft() {
    
    GameObjectPosition *position=this->getPosition();
    
    int newPositionX=position->getX()-1;
    this->setX(newPositionX);
    
    animationDirection=kObjectDirectionLeft;  
    

    
}

void GameObject::goDown() {
    
    GameObjectPosition *position=this->getPosition();
    
    int newPositionY=position->getY()+1;
    this->setY(newPositionY);
    
    animationDirection=kObjectDirectionDown;    
    
}

int GameObject::getX() {

    GameObjectPosition *position=this->getPosition();
    return position->getX();
    
}

int GameObject::getY() {

    GameObjectPosition *position=this->getPosition();
    return position->getY();
    
}

bool GameObject::hitTest(int hitOnX, int hitOnY) {
    
    debugLog("Hit test\n");
    
    int nowX=gamePosition->getX();
    int nowY=gamePosition->getY();
    
    int width=this->getWidth();
    int height=this->getHeight();
    
    int leftBorder=nowX-(width/2);
    int rightBorder=nowX+(width/2);
    int upBorder=nowY-height;
    int downBorder=nowY;
    
    if (hitOnX<leftBorder)
        return false;
    if (hitOnX>rightBorder)
        return false;
    if (hitOnY<upBorder)
        return false;
    if (hitOnY>downBorder)
        return false;
    
#ifdef DEBUG
    printf("hit on X:%d\nhit on Y:%d\nobjectX:%d\nobjectY:%d\nleftBorder:%d\nrightBorder:%d\nupBorder:%d\ndownBorder:%d\n",
            hitOnX,hitOnY,nowX,nowY,leftBorder,rightBorder,upBorder,downBorder);
#endif
    debugLog("hit\n");
    
    return true;
    
}

void GameObject::goRight() {
    
    GameObjectPosition *position=this->getPosition();
    
    int newPositionX=position->getX()+1;
    this->setX(newPositionX);
    
  
    
}

void GameObject::live() {
    
    //debugLog("NowCommand:"<<nowCommand);
    

    
}
