/* 
 * File:   SDLSurfaceSaverToPNG.cpp
 * Author: User
 * 
 * Created on July 28, 2012, 1:51 PM
 */

#include "SDLSurfaceSaverToPNG.h"
#include "unistd.h"

SDLSurfaceSaverToPNG::SDLSurfaceSaverToPNG() {
}

SDLSurfaceSaverToPNG::SDLSurfaceSaverToPNG(const SDLSurfaceSaverToPNG& orig) {
}

SDLSurfaceSaverToPNG::~SDLSurfaceSaverToPNG() {
}

string SDLSurfaceSaverToPNG::getAlphaNumericString(string inputString) {
    
  string outputString;
    
  for (unsigned int a=0; a<inputString.length();a++)
  {
    char nowChar=inputString[a];
    if (isalnum(nowChar)) outputString += nowChar;
  }

  return outputString;
    
}

void SDLSurfaceSaverToPNG::saveSurface(SDL_Surface *surfaceToSave) {
    
    const time_t timer = time(NULL);
    string rawTimeString=ctime(&timer);
    string timeForFilename=SDLSurfaceSaverToPNG::getAlphaNumericString(rawTimeString);

    char currentWorkingDirectory[1024];
    getcwd(currentWorkingDirectory,1024);        
    
    char outputFilepath[1024];
    char outputDirectory[1024];
        
    snprintf(outputDirectory,1024,"%s/%s",currentWorkingDirectory,kSCREENSHOTS_DIRECTORY);
    
    struct stat st;
    if(stat(outputDirectory,&st)) {
        if (mkdir(outputDirectory, S_IRWXU | S_IRWXG | S_IRWXO)) {
            debugLog("Error creating directory:"<<outputDirectory<<"\n");
            return;
        }
    }
    
    
    snprintf(outputFilepath,1024,"%s/%s.%s.v%s%s",outputDirectory,kSCREENSHOT_FILENAME,timeForFilename.c_str(),kGameVersion,kSCREENSHOT_FILE_EXTENSION);
    
#ifdef WINDOWS32_BUILD
    ugl::switchSlashesToWin32(outputFilepath);
#endif    
    
    SDL_Surface *tmp = SDL_PNGFormatAlpha(surfaceToSave);
    SDL_SavePNG(tmp, outputFilepath);
    SDL_FreeSurface(tmp);
    debugLog("Screenshot saved: "<<outputFilepath<<"\n");    
    
   
    
}
