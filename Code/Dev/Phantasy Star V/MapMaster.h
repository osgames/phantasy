/* 
 * File:   MapMaster.h
 * Author: demensdeum
 *
 * Created on March 17, 2012, 2:02 PM
 */

#ifndef MAPMASTER_H
#define	MAPMASTER_H

#include <stdio.h> 
#include <string.h>
#include <queue>
#include "Tile.h"
#include "GameObject.h"
#include "RenderLayer.h"
#include "ResourcesManager.h"
#include "DialogMessage.h"
#include <map>

using namespace std;

class MapMaster {
public:
    MapMaster();
    MapMaster(const MapMaster& orig);
    virtual ~MapMaster();
    
    void loadRenderLayer(char *filename, long int *returnPosition);
    void saveRenderLayer(char *filename);
    
    static void saveMap(char* filePath, vector<Tile*>&tiles, vector<GameObject*>& gameObjects, RenderLayer** renderLayers, MapMaster **mapLayers, int arrangementMap[][kDefaultMapSize*2][kDefaultMapSize*2], int wallsArrangementMap[][kDefaultMapSize], int solidMap[kDefaultMapSize*4][kDefaultMapSize*4], vector<string>&scripts);
    static void loadMap(char* filePath, vector<Tile*>&tiles, vector<GameObject*>& gameObjects, RenderLayer** renderLayers, MapMaster **mapLayers, int arrangementMap[][kDefaultMapSize*2][kDefaultMapSize*2], int wallsArrangementMap[][kDefaultMapSize], int solidMap[kDefaultMapSize*4][kDefaultMapSize*4], vector<string>&scripts, std::map<string,string>&mapProperties);
    static GameObject* getParentObjectByName(char *parentName, vector<GameObject*>&gameObjects);
    static void loadGameObject(GameObject::GameObjectStruct gameObjectStruct, RenderLayer** renderLayers, vector<GameObject*>gameObjects);
    static queue<DialogMessage> loadDialogFile(char *dialogFilePath);
    
    void generateMap();
    
    int** getMap();
    int** getSolidMap();
    
    void newBlankDefaultMap();
    void newBlankInvisibleMap();
    
    int getTile(int tileX, int tileY);
    void setTile(int tileX, int tileY, int tileNumber);
    //void fixTilesArrangement();
    
private:
    
    int **map;
    

};

#endif	/* MAPMASTER_H */

