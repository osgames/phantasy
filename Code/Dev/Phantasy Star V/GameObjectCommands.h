/* 
 * File:   GameObjectCommands.h
 * Author: User
 *
 * Created on March 10, 2012, 2:37 PM
 */

#ifndef GAMEOBJECTCOMMANDS_H
#define	GAMEOBJECTCOMMANDS_H

enum {
    
    kCommandNone,
    kCommandGoLeft,
    kCommandGoRight,
    kCommandGoUp,
    kCommandGoDown,
    kCommandYesButton,
    kCommandCallObjectAtFront
    
};

#endif	/* GAMEOBJECTCOMMANDS_H */

