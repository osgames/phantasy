/* 
 * File:   debuglog.h
 * Author: demensdeum
 *
 * Created on December 4, 2011, 7:48 PM
 */

#include <iostream>

#ifdef DEBUG
#define debugLog(str) do { std::cout << str ; } while( false )
#else
#define debugLog(str) do { } while ( false )
#endif


