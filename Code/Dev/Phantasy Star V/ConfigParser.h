/* 
 * File:   ConfigParser.h
 * Author: User
 *
 * Created on May 6, 2012, 12:20 PM
 */

#ifndef CONFIGPARSER_H
#define	CONFIGPARSER_H

#include "debuglog.h"
#include <map>
#include <string>
#include <stdio.h>
#include <string.h>
#include "Const.h"

using namespace std;

class ConfigParser {
public:
    ConfigParser();
    ConfigParser(const ConfigParser& orig);
    virtual ~ConfigParser();
    static map<string,string> parseConfigFile(char *configFilePath, char *gameModulePath=NULL);
    
private:

};

#endif	/* CONFIGPARSER_H */

