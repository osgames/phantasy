/* 
 * File:   ConfigParser.cpp
 * Author: User
 * 
 * Created on May 6, 2012, 12:20 PM
 */

#include "ConfigParser.h"
#include "unistd.h"

ConfigParser::ConfigParser() {
}

ConfigParser::ConfigParser(const ConfigParser& orig) {
}

ConfigParser::~ConfigParser() {
}

map<string,string> ConfigParser::parseConfigFile(char *configFilePath, char *gameModulePath) {

    map<string,string>parsedConfigFile;
    
    /*parsedConfigFile["name"]="testValue";
    debugLog("Config file parsed\n"<<parsedConfigFile["name"]<<"\n");*/
    
    FILE *configFile;
    configFile=fopen(configFilePath,"r");
    
#ifdef DEBUG
    char currentWorkingDirectory[1024];
    getcwd(currentWorkingDirectory,1024);
    debugLog("Current path:"<<currentWorkingDirectory<<"\n");    
#endif
    
    if(configFile==NULL) {
        debugLog("Can't open config file:"<<configFilePath<<"\n");

        return parsedConfigFile;
    }
    else debugLog("Config file opened:"<<configFilePath<<"\n");

    char nowString[kMaxStringLength];
    int charCount=0;
    
    //set config path
    if (gameModulePath!=NULL)
        parsedConfigFile["system_pathForConfigParser"]=gameModulePath;
    
    while(true) {
        
        char nowChar=fgetc(configFile);
        if (nowChar==EOF || nowChar=='\n') {
            nowString[charCount]=NULL;
            
            char *key=strtok(nowString,"=");
            char *value=strtok(NULL,"=");
            
            if (key!=NULL && value!=NULL) {
                parsedConfigFile[key]=value;
                debugLog("Parsed key:"<<key<<"\nValue:"<<parsedConfigFile[key]<<"\n");
            }
            
        }
        else {
            nowString[charCount]=nowChar;
            charCount++;
        }
        
        if (nowChar==EOF)
            break;
        if (nowChar=='\n') {
            nowString[0]=NULL;
            charCount=0;
        }
    }
        
    fclose(configFile);
    
    debugLog("Parse end\n");
    
    return parsedConfigFile;
    
}