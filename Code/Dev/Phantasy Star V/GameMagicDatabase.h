/* 
 * File:   GameMagicDatabase.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:50 PM
 */

#ifndef GAMEMAGICDATABASE_H
#define	GAMEMAGICDATABASE_H

#include "GameDatabase.h"
#include "GameMagic.h"

class GameMagicDatabase : public GameDatabase {
public:
    GameMagicDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory);
    GameMagicDatabase(const GameMagicDatabase& orig);
    virtual ~GameMagicDatabase();
    
    GameMagic* getGameMagicWithName(string nowItemName);
    vector<GameMagic*> returnGameMagicFromRawString(string itemsListRawString);    
    
private:
    
    vector<GameMagic*>gameMagic;
    void addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary);

};

#endif	/* GAMEMAGICDATABASE_H */

