/* 
 * File:   GameTechnique.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:53 PM
 */

#ifndef GAMETECHNIQUE_H
#define	GAMETECHNIQUE_H

#include "GameItem.h"

class GameTechnique : public GameItem {
public:
    GameTechnique();
    GameTechnique(const GameTechnique& orig);
    virtual ~GameTechnique();
private:

};

#endif	/* GAMETECHNIQUE_H */

