/* 
 * File:   ModulesManager.cpp
 * Author: demensdeum
 * 
 * Created on December 3, 2011, 12:58 AM
 * 
 */

#include "ModulesManager.h"



ModulesManager::ModulesManager() {
}

ModulesManager::ModulesManager(const ModulesManager& orig) {
}

ModulesManager::~ModulesManager() {
}

char** ModulesManager::getGameModulesDirectoryList() {
    
    char *gameModulesDirectoryList[kMaxGameModules];
    int itemsCount=0;
    
    DIR *gameModulesDIR;
    gameModulesDIR = opendir(kGameModulesPath);
    
    if (gameModulesDIR==NULL) {
        debugLog("Can't open game modules directory:"<<kGameModulesPath<<"\n");
        return NULL;
    }
    
    struct dirent *nowDirectory;
    
    while (nowDirectory = readdir (gameModulesDIR)) {
     
        char *directoryName=nowDirectory->d_name;
        
        if (!strcmp(directoryName,".") || !strcmp(directoryName,".."))
            continue;
            
        debugLog("Game modules directory contains:"<<directoryName<<"\n");

        gameModulesDirectoryList[itemsCount]=directoryName;
        if (itemsCount<kMaxGameModules-2)
            itemsCount++;
        else
            break;
        
        
    }
    
    gameModulesDirectoryList[itemsCount-1]=NULL;
    
    return gameModulesDirectoryList; 
    
}

void ModulesManager::refreshModulesList() {
    
   char **gameModulesDirectoryList=this->getGameModulesDirectoryList();
    
}
