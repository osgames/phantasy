/* 
 * File:   GameTechniqueDatabase.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:53 PM
 */

#include "GameTechniqueDatabase.h"

GameTechniqueDatabase::GameTechniqueDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory) : GameDatabase(newDatabaseFilePath,newDatabaseResourcesDirectory) {
}

GameTechniqueDatabase::GameTechniqueDatabase(const GameTechniqueDatabase& orig) {
}

GameTechniqueDatabase::~GameTechniqueDatabase() {
}

GameTechnique* GameTechniqueDatabase::getGameTechniqueWithName(string nowItemName) {
    for (int a=0;a<gameTechnique.size();a++) {
        GameTechnique *nowItem=gameTechnique[a];
        if (nowItem->getName()==nowItemName) return nowItem;
    }
    
    return NULL;    
}

vector<GameTechnique*> GameTechniqueDatabase::returnGameTechniqueFromRawString(string itemsListRawString) {
    
    vector<GameTechnique*>outputVector;
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)itemsListRawString.c_str(),",");
    
    if (parsedItemsList.size()<1) {
        debugLog("Error. Parsed items list size:"<<parsedItemsList.size()<<"\n");
        exit(4);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItemName=parsedItemsList[a];
        GameTechnique* outputItem=this->getGameTechniqueWithName(nowItemName);
        if (!outputItem) {
            debugLog("Cannot find item with name:"<<nowItemName<<"\n");
            exit(3);
        }
        outputVector.push_back(outputItem);
        
    }
    
    return outputVector;    
    
}      
void GameTechniqueDatabase::addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary) {
    
    if (gameDatabaseDictionary["name"].empty()) {
        debugLog("Error loading item. Name key is empty.");
        exit(3);
    }
    
    if (gameDatabaseDictionary["humanReadableName"].empty()) {
        debugLog("Error loading item. Human Readable Name key is empty.");
        exit(3);
    }    
    
    if (gameDatabaseDictionary["description"].empty()) {
        debugLog("Error loading item. Description key is empty.");
        exit(3);
    }    
    
    GameTechnique *newGameTechnique=new GameTechnique();
    newGameTechnique->setName(gameDatabaseDictionary["name"]);
    newGameTechnique->setHumanReadableName(gameDatabaseDictionary["humanReadableName"]);
    newGameTechnique->setDescription(gameDatabaseDictionary["description"]);
    
    gameTechnique.push_back(newGameTechnique);      
    
}
