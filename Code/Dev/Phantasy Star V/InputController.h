/* 
 * File:   InputController.h
 * Author: User
 *
 * Created on March 10, 2012, 1:58 PM
 */

#include <SDL/SDL.h>

#ifndef INPUTCONTROLLER_H
#define	INPUTCONTROLLER_H

class InputController {
public:
    InputController();
    InputController(const InputController& orig);
    virtual ~InputController();
    void pollEvents();
    int getLastCommand();
    void allButtonsToUnpress();
    
    bool getWindowResize();
    int getWindowWidth();
    int getWindowHeight();
    
    bool getApplicationQuit();
    
    bool getLeftMouseClick();
    bool getRightMouseClick();
    int  getMouseX();
    int  getMouseY();
    
    bool getYesButton();
    
    void unpressMouse();
    bool getUseButton();
    void unpressUseButton();
    bool getEscapeButton();
    bool getDeleteButton();
    
    bool getMasterConsoleButton();
    
    bool getMouseScrollUp();
    bool getMouseScrollDown();
    
    bool getControlButton();
    
    bool getScreenshotButton();
    bool getRefreshMapButton();
    
private:

    bool applicationQuit;
    
    bool windowResize;
    bool masterConsoleButton;
    
    int windowWidth;
    int windowHeight;
    
    bool mouseLeftClick;
    bool mouseRightClick;
    int mouseX;
    int mouseY;
     
    bool deleteButton;
    bool escapeButton;
    bool goLeft;
    bool goRight;
    bool goUp;
    bool goDown;
    bool useButton;
    bool menuButton;
    bool yesButton;
    bool noButton;
    bool controlButton;
    
    int cursorX;
    int cursorY;
    
    int cursorPress;
    int lastCommand;
    
    bool mouseScrollUp;
    bool mouseScrollDown;
    
    bool screenshotButton;
    bool refreshMapButton;
    
};

#endif	/* INPUTCONTROLLER_H */

