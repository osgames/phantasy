/* 
 * File:   ObjectsMaster.cpp
 * Author: demensdeum
 * 
 * Created on December 18, 2011, 3:54 PM
 */

#include "ObjectsMaster.h"


ObjectsMaster::ObjectsMaster(GameEngine *newGameEngine) {
    objectsCount=0;
    gameEngine=newGameEngine;
}

ObjectsMaster::ObjectsMaster(const ObjectsMaster& orig) {
}

ObjectsMaster::~ObjectsMaster() {
}

GameObject **ObjectsMaster::getObjects() {
    
    return objects;
    
}

void ObjectsMaster::goRightObject(GameObject *nowObject) {
    
    int solidMapX=nowObject->getX()/kSolidSize;
    int solidMapY=nowObject->getY()/kSolidSize;
    
    solidMapX++;
    
    if (solidMapX<1)
        return;    
    
    //TODO add cell border check
    nowObject->setAnimationDirection(kObjectDirectionRight);    
    int nowCell=(*solidMap)[solidMapY][solidMapX];
    
    if (nowCell==kEmptySolid) {
        nowObject->goRight();
    }
    else if (nowCell!=kSolid && nowCell>=0) {
        gameEngine->touchSolid(nowCell,nowObject);
    }
    
}

void ObjectsMaster::goLeftObject(GameObject *nowObject) {
    
    int solidMapX=nowObject->getX()/kSolidSize;
    int solidMapY=nowObject->getY()/kSolidSize;
    
    solidMapX--;
    
    if (solidMapX<1)
        return;
    
    //TODO add cell border check
    nowObject->setAnimationDirection(kObjectDirectionLeft);    
    int nowCell=(*solidMap)[solidMapY][solidMapX];
    
    if (nowCell==kEmptySolid) {
        nowObject->goLeft();
    }
    else if (nowCell!=kSolid && nowCell>=0) {
        gameEngine->touchSolid(nowCell,nowObject);
    }
    
}

void ObjectsMaster::goDownObject(GameObject *nowObject) {
    
    int solidMapX=nowObject->getX()/kSolidSize;
    int solidMapY=nowObject->getY()/kSolidSize;

    solidMapY++;
    
    if (solidMapY>kDefaultMapSize-2)
        return;    
    
    //TODO add cell border check
    nowObject->setAnimationDirection(kObjectDirectionDown);    
    int nowCell=(*solidMap)[solidMapY][solidMapX];
    
    if (nowCell==kEmptySolid) {
        nowObject->goDown();
    }
    else if (nowCell!=kSolid && nowCell>=0) {
        gameEngine->touchSolid(nowCell,nowObject);
    }

    
}

void ObjectsMaster::goUpObject(GameObject *nowObject) {
    

    
    int solidMapX=nowObject->getX()/kSolidSize;
    int solidMapY=nowObject->getY()/kSolidSize;
    
    debugLog("nowObjectX:"<<nowObject->getX()<<"\nnowObjectY:"<<nowObject->getY()<<"\nkSolidSize:"<<kSolidSize<<"\nsolidMapX:"<<solidMapX<<"\nsolidMapY:"<<solidMapY<<"\n");    
    
    solidMapY--;
    
    if (solidMapY<1)
        return;
    
    
    //TODO add cell border check
    nowObject->setAnimationDirection(kObjectDirectionUp);
    int nowCell=(*solidMap)[solidMapY][solidMapX];
    
    if (nowCell==kEmptySolid) {
        nowObject->goUp();
    }
    else if (nowCell!=kSolid && nowCell>=0) {
        gameEngine->touchSolid(nowCell,nowObject);
    }
    
}


void ObjectsMaster::callObjectAtFront(GameObject *callerObject) {
    
    int objectX=callerObject->getX();
    int objectY=callerObject->getY();
    //get direction
    int direction=callerObject->getAnimationDirection();    
    int targetX=0;
    int targetY=0;
    //get target X,Y
    switch (direction) {
        
        case kObjectDirectionLeft:
            targetX=objectX-kTileSize/2;
            break;
        
        case kObjectDirectionRight:
            targetX=objectX+kTileSize/2;
            break;
            
        case kObjectDirectionDown:
            targetY=objectY+kTileSize/2;
            break;
            
        case kObjectDirectionUp:
            targetY=objectY-kTileSize/2;
            break;
            
    }
    
    //found object
    for (int a=0;a<objectsCount;a++) {
        
        debugLog("a:"<<a<<"\n");
        
        GameObject *nowObject=objects[a];
        //check if not caller
        if (nowObject->getName()==callerObject->getName())
            continue;
        
        if(nowObject->hitTest(targetX,targetY)) {
            
            nowObject->call();
            nowObject->setCalledBy(callerObject);
            return;//only single object can be called
            
        }
        
        
        
    }
    
    //if object wasn't called
    int solidMapX=callerObject->getX()/kSolidSize;
    int solidMapY=callerObject->getY()/kSolidSize;    

    switch (callerObject->getAnimationDirection()) {
        
        case kObjectDirectionDown:
            solidMapY++;
            break;
            
        case kObjectDirectionUp:
            solidMapY--;
            break;
            
        case kObjectDirectionRight:
            solidMapX++;
            break;
            
        case kObjectDirectionLeft:
            solidMapX--;
            break;
        
    }
    
    if (solidMapX>=kDefaultMapSize*4) return;
    if (solidMapY>=kDefaultMapSize*4) return;
    if (solidMapX<0) return;
    if (solidMapY<0) return;
    
    int nowCell=(*solidMap)[solidMapY][solidMapX];
    
    gameEngine->callSolid(nowCell,callerObject);
    
}

void ObjectsMaster::processObjectCommand(GameObject *nowObject) {
    
    int nowCommand=nowObject->getNowCommand();
    nowObject->setNowCommand(kCommandNone);
      
    switch (nowCommand) {
        
        case kCommandGoRight:
            goRightObject(nowObject);
            break;
            
         case kCommandGoLeft:
            goLeftObject(nowObject);
            break;
 
         case kCommandGoDown:
            goDownObject(nowObject);
            break;
            
         case kCommandGoUp:
            goUpObject(nowObject);
            break;           
            
        case kCommandCallObjectAtFront:
            callObjectAtFront(nowObject);
            break;
            
    }
    
}

void ObjectsMaster::handleInput(InputController *inputController, GameObject *playerObject) {

    if (!inputController)
        return;
    if (!playerObject)
        return;
    
        switch (inputController->getLastCommand()) {

            case kCommandGoLeft:
                playerObject->setNowCommand(kCommandGoLeft);
                break;

            case kCommandGoRight:
                playerObject->setNowCommand(kCommandGoRight);
                break;

            case kCommandGoUp:
                playerObject->setNowCommand(kCommandGoUp);
                break;

            case kCommandGoDown:
                playerObject->setNowCommand(kCommandGoDown);
                break;

    }
        
    if (inputController->getUseButton()) {
        debugLog("Use button\n");
        playerObject->setNowCommand(kCommandCallObjectAtFront);
        inputController->unpressUseButton();
    }

}

void ObjectsMaster::playerTeamFollowLeader(PlayerTeam *playerTeam) {
    
    GameObject *leaderGameObject=playerTeam->getLeaderGameObject();
    int heroesCount=playerTeam->getHeroesCount();
    
    for (int a=1;a<heroesCount;a++) {
        
        
        Hero *nowHero=playerTeam->getHeroAtIndex(a);
        Hero *previousHero=playerTeam->getHeroAtIndex(a-1);
        
        GameObject *nowHeroGameObject=nowHero->getGameObject();
        GameObject *previousHeroGameObject=previousHero->getGameObject();
        
        if (previousHeroGameObject->getMovementTraceCount()==64) {
            
            GameObject::GameObjectStruct previousHeroGameObjectStruct=previousHeroGameObject->getLastGameObjectStructFromMovementTrace();
            
            //check if same coordinates
            if (nowHeroGameObject->getX()==previousHeroGameObjectStruct.positionX && nowHeroGameObject->getY()==previousHeroGameObjectStruct.positionY) continue;
            
            {
                
                int nowHeroX=nowHeroGameObject->getX();
                int nowHeroY=nowHeroGameObject->getY();
                
                int targetPositionX=previousHeroGameObjectStruct.positionX;
                int targetPositionY=previousHeroGameObjectStruct.positionY;
                
                if (nowHeroX<targetPositionX) {
                    nowHeroGameObject->setNowCommand(kCommandGoRight);
                }
                else if (nowHeroX>targetPositionX) {
                    nowHeroGameObject->setNowCommand(kCommandGoLeft);
                }
                else if (nowHeroY<targetPositionY) {
                    nowHeroGameObject->setNowCommand(kCommandGoDown);
                }
                else if (nowHeroY>targetPositionY) {
                    nowHeroGameObject->setNowCommand(kCommandGoUp);
                }                
                
                
            }
            
            
            
            //nowHeroGameObject->setX(previousHeroGameObjectStruct.positionX);
            //nowHeroGameObject->setY(previousHeroGameObjectStruct.positionY);
        }
        
        
        /*Hero *nowHero=playerTeam->getHeroAtIndex(a);
        
        
        
        GameObject *nowHeroGameObject=nowHero->getGameObject();
        
        
        
        //skip team leader
        if (nowHeroGameObject==leaderGameObject) {
            continue;
        }
        
        int minimalWidthFarForFollow=nowHeroGameObject->getImage()->w*nowHero->getPriorityAtTeam();
        int minimalHeightFarForFollow=nowHeroGameObject->getImage()->h*nowHero->getPriorityAtTeam();
        
        //Todo correct leader following
        int farFromLeaderByX=abs(leaderGameObject->getX()-nowHeroGameObject->getX());
        int farFromLeaderByY=abs(leaderGameObject->getY()-nowHeroGameObject->getY());
        
        debugLog("farFromLeaderByX: "<<farFromLeaderByX<<"\n");
        debugLog("farFromLeaderByY: "<<farFromLeaderByY<<"\n");
        debugLog("minimalWidthFarForFollow: "<<minimalWidthFarForFollow<<"\n");
        debugLog("minimalHeightFarForFollow: "<<minimalHeightFarForFollow<<"\n");
        
        if (farFromLeaderByX>minimalWidthFarForFollow || farFromLeaderByY>minimalHeightFarForFollow) { 
        
                if (nowHeroGameObject->getX()<leaderGameObject->getX()) {
                        nowHeroGameObject->setNowCommand(kCommandGoRight);
                }
                
                if (nowHeroGameObject->getX()>leaderGameObject->getX()) {
                        nowHeroGameObject->setNowCommand(kCommandGoLeft);
                }                
                
                if (nowHeroGameObject->getY()<leaderGameObject->getY()) {
                        nowHeroGameObject->setNowCommand(kCommandGoDown);
                }
                
                if (nowHeroGameObject->getY()>leaderGameObject->getY()) {
                        nowHeroGameObject->setNowCommand(kCommandGoUp);
                }                
                
                
        
        }*/
        
    }
    
}

void ObjectsMaster::liveStep() {
    
    int a=0;
    
    for (a=0;a<objectsCount;a++) {
        
        GameObject *nowObject=objects[a];
        nowObject->live();
        this->processObjectCommand(nowObject);
        
    }
    
}

void ObjectsMaster::clearObjectsCount() {
    
    objectsCount=0;
    
}

void ObjectsMaster::loadObject(GameObject *newObject) {
    
    if (newObject==NULL) {
        debugLog("newObject are NULL\n");
    }
    
    objects[objectsCount]=newObject;
    objectsCount++;
    
}

void ObjectsMaster::setSolidMap(int (*newSolidMap)[kDefaultMapSize*4][kDefaultMapSize*4]) {
    
    solidMap=newSolidMap;
    
}
