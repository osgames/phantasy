/* 
 * File:   ObjectsArray.h
 * Author: demensdeum
 *
 * Created on December 7, 2011, 1:30 AM
 */

#include "ObjectsSector.h"
#include "GameObject.h"
#include <vector>


#ifndef OBJECTSARRAY_H
#define	OBJECTSARRAY_H


class ObjectsArray {
public:
    ObjectsArray();
    ObjectsArray(const ObjectsArray& orig);
    virtual ~ObjectsArray();
    ObjectsArray(ObjectsSector *newUnknownSector);
    
    ObjectsSector* getSector(int positionX,int positionY);
    void addLine();
    void makeMatrix(int matrixWidth, int matrixHeight,int tileWidth, int tileHeight);
    void addObjectsSector(ObjectsSector *newObjectsSector);    
    void addObject(GameObject *newGameObject, int positionX, int positionY);   
    
private:
    
    vector<vector<ObjectsSector*> > objectsMatrix;
    ObjectsSector *unknownSector;

    
};

#endif	/* OBJECTSARRAY_H */

