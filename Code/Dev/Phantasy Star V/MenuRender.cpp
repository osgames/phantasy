/* 
 * File:   MenuRender.cpp
 * Author: demensdeum
 * 
 * Created on March 25, 2012, 6:05 PM
 */

#include "MenuRender.h"
#include "debuglog.h"
#include "Const.h"
#include "SDL/SDL_ttf.h"

#include <SDL/SDL_gfxBlitFunc.h>

MenuRender::MenuRender() {
}

MenuRender::MenuRender(const MenuRender& orig) {
}

MenuRender::~MenuRender() {
}

void MenuRender::makeMenuWithButtonsOrNot(char **items, int drawX, int drawY, SDL_Surface *drawSurface, bool withButtons) {
    
    TTF_Font *font = TTF_OpenFont("./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf",18);
    if (font==NULL)
        debugLog("Can't load font ./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf\nMaybe SDL_TTF not inited\n");
        
    SDL_Color fColor;
    
    fColor.r = fColor.g = fColor.b = 255; 
    
    SDL_Surface *menuCanvas=SDL_CreateRGBSurface(SDL_SWSURFACE,kMenuCanvasMaxSize,kMenuCanvasMaxSize,32,kRmask,kGmask,kBmask,kAmask);
    SDL_Surface *labelsCanvas=SDL_CreateRGBSurface(SDL_SWSURFACE,kMenuCanvasMaxSize,kMenuCanvasMaxSize,32,kRmask,kGmask,kBmask,kAmask);
    
    int index=0;
    
    int labelMaxWidth=0;
    int labelDrawY=0;
    
    while(true) {
        
        char *nowItemLabel=items[index];
        if (nowItemLabel!=NULL) {
                
            
                debugLog("Render text:"<<nowItemLabel<<"\n");
                SDL_Surface *drawText=TTF_RenderUTF8_Blended(font, nowItemLabel, fColor);
                
                if (drawText==NULL)
                    debugLog("drawText are NULL\n");
                
                if (drawText->w>labelMaxWidth)
                    labelMaxWidth=drawText->w;
    
                SDL_Rect destination;
                destination.x=0;
                destination.y=labelDrawY;
                destination.w=drawText->w;
                destination.h=drawText->h;                   
                
                SDL_gfxBlitRGBA(drawText,NULL,labelsCanvas,&destination);   
                SDL_FreeSurface(drawText);
                
                labelDrawY+=20;
                
                index++;
        }
        else
            break;
       
    }
    
    //make menu border
    
    int menuWidth=kLeftMenuOffset+labelMaxWidth+kRightMenuOffset;
    int menuHeight=kUpMenuOffset+labelDrawY+kDownMenuOffset;
    
    SDL_Rect borderRect;
    borderRect.x=0;
    borderRect.y=0;
    borderRect.w=menuWidth+kMenuBorder*2;
    borderRect.h=menuHeight+kMenuBorder*2;    
    
    SDL_Rect menuRect;
    menuRect.x=kMenuBorder;
    menuRect.y=kMenuBorder;
    menuRect.w=menuWidth;
    menuRect.h=menuHeight;  

    SDL_Rect labelRect;
    labelRect.x=kLeftMenuOffset;
    labelRect.y=kUpMenuOffset;
    labelRect.w=labelMaxWidth;
    labelRect.h=labelDrawY;
    
    SDL_Rect drawRect;
    drawRect.x=drawX;
    drawRect.y=drawY;
    drawRect.w=menuWidth+kMenuBorder*2;
    drawRect.h=menuHeight+kMenuBorder*2;    
    
    
    //draw to surface
    
    

    SDL_FillRect(menuCanvas, &borderRect, 0xFFFFFFFF);    
    SDL_FillRect(menuCanvas, &menuRect, 0xFFFF0000);
    SDL_gfxBlitRGBA(labelsCanvas,NULL,menuCanvas,&labelRect);
    
    //draw buttons
    
    if (withButtons) {
    
        SDL_Rect buttonBorderRect;
        buttonBorderRect.x=kLeftMenuButtonOffset;
        buttonBorderRect.y=kUpMenuButtonOffset;
        buttonBorderRect.w=kMenuButtonSize;
        buttonBorderRect.h=kMenuButtonSize;

        SDL_Rect buttonRect;
        buttonRect.x=kLeftMenuButtonOffset+2;
        buttonRect.y=kUpMenuButtonOffset+2;
        buttonRect.w=kMenuButtonSize-4;
        buttonRect.h=kMenuButtonSize-4;    

        for (int a=0;a<index;a++) {


            SDL_FillRect(menuCanvas, &buttonBorderRect, 0xFFFFFFFF); 
            SDL_FillRect(menuCanvas, &buttonRect, 0xFFFF0000);

            buttonBorderRect.y+=20;        
            buttonRect.y+=20;

        }
    
    }
    
    SDL_BlitSurface(menuCanvas,NULL,drawSurface,&drawRect);
             
    SDL_FreeSurface(labelsCanvas);
    SDL_FreeSurface(menuCanvas); 
    
    TTF_CloseFont(font);
    
}

void MenuRender::makeMenu(char **items, int drawX, int drawY, SDL_Surface *drawSurface) {
    MenuRender::makeMenuWithButtonsOrNot(items, drawX, drawY, drawSurface, true);
}
void MenuRender::makeWindow(char **items, int drawX, int drawY, SDL_Surface *drawSurface) {
    MenuRender::makeMenuWithButtonsOrNot(items, drawX, drawY, drawSurface, false);
}
