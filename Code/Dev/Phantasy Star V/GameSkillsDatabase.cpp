/* 
 * File:   GameSkillsDatabase.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 7:14 PM
 */

#include "GameSkillsDatabase.h"

#include "GameDatabase.h"

GameSkillsDatabase::GameSkillsDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory) : GameDatabase(newDatabaseFilePath,newDatabaseResourcesDirectory) {
}

GameSkillsDatabase::GameSkillsDatabase(const GameSkillsDatabase& orig) {
}

GameSkillsDatabase::~GameSkillsDatabase() {
}

GameSkill* GameSkillsDatabase::getGameSkillWithName(string nowItemName) {
    
    for (int a=0;a<gameSkills.size();a++) {
        GameSkill *nowItem=gameSkills[a];
        if (nowItem->getName()==nowItemName) return nowItem;
    }
    
    return NULL;
    
}

vector<GameSkill*> GameSkillsDatabase::returnGameSkillsFromRawString(string itemsListRawString) {
    
    vector<GameSkill*>outputVector;
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)itemsListRawString.c_str(),",");
    
    if (parsedItemsList.size()<1) {
        debugLog("Error. Parsed items list size:"<<parsedItemsList.size()<<"\n");
        exit(4);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItemName=parsedItemsList[a];
        GameSkill* outputItem=this->getGameSkillWithName(nowItemName);
        if (!outputItem) {
            debugLog("Cannot find item with name:"<<nowItemName<<"\n");
            exit(3);
        }
        outputVector.push_back(outputItem);
        
    }
    
    return outputVector;
    
    
}


void GameSkillsDatabase::addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary) {
    
    if (gameDatabaseDictionary["name"].empty()) {
        debugLog("Error loading item. Name key is empty.");
        exit(3);
    }
    
    if (gameDatabaseDictionary["humanReadableName"].empty()) {
        debugLog("Error loading item. Human Readable Name key is empty.");
        exit(3);
    }    
    
    if (gameDatabaseDictionary["description"].empty()) {
        debugLog("Error loading item. Description key is empty.");
        exit(3);
    }    
    
    GameSkill *newGameSkill=new GameSkill();
    newGameSkill->setName(gameDatabaseDictionary["name"]);
    newGameSkill->setHumanReadableName(gameDatabaseDictionary["humanReadableName"]);
    newGameSkill->setDescription(gameDatabaseDictionary["description"]);
    
    gameSkills.push_back(newGameSkill);
    
}