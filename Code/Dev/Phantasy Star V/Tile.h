/* 
 * File:   Tile.h
 * Author: demensdeum
 *
 * Created on December 4, 2011, 4:13 PM
 */



#ifndef TILE_H
#define	TILE_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "Const.h"
#include <string>
#include "debuglog.h"

using namespace std;

class Tile {
public:
    Tile();
    Tile(const Tile& orig);
    virtual ~Tile();
    
    struct TileSerializeStruct {
        //TODO add other properties
        char imagePath[kMaxStringLength];
    };    
    
    int getWidth();
    int getHeight();
    SDL_Surface* getImage();
    void setImage(SDL_Surface *newImage);
    
    void setTileType(int newType);
    int getTileType();
    
    TileSerializeStruct serializePropertiesToStruct();
    void deserializeFromStruct(TileSerializeStruct importedStruct);
    
    string getFullPath();
    void setFullPath(string newFullPath);
    
    void setImage(char *imagePath);
    
    
private:
    
    SDL_Surface *image;
    int tileType;
    string fullPath;
    

};

#endif	/* TILE_H */

