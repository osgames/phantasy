/* 
 * File:   GameDatabaseItem.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:07 PM
 */

#include "GameDatabaseItem.h"

GameDatabaseItem::GameDatabaseItem() {
}

GameDatabaseItem::GameDatabaseItem(const GameDatabaseItem& orig) {
}

GameDatabaseItem::~GameDatabaseItem() {
}

//get 
string GameDatabaseItem::getName() {
    return name;
}

string GameDatabaseItem::getHumanReadableName() {
    return humanReadableName;
}

string GameDatabaseItem::getDescription() {
    return description;
}

//set
void GameDatabaseItem::setName(string newName) {
    name=newName;
}
void GameDatabaseItem::setHumanReadableName(string newHumanReadableName) {
    humanReadableName=newHumanReadableName;
}
void GameDatabaseItem::setDescription(string newDescription) {
    description=newDescription;
}