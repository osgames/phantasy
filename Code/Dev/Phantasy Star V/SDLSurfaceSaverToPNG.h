/* 
 * File:   SDLSurfaceSaverToPNG.h
 * Author: User
 *
 * Created on July 28, 2012, 1:51 PM
 */



#ifndef SDLSURFACESAVERTOPNG_H
#define	SDLSURFACESAVERTOPNG_H

#include "SDL/SDL.h"
#include "savepng.h"
#include "debuglog.h"
#include "time.h"
#include <string>
#include "version.h"
#include "sys/stat.h"

#ifdef WINDOWS32_BUILD
#include "ugl.h"
#endif

#define kSCREENSHOTS_DIRECTORY "screenshots"
#define kSCREENSHOT_FILENAME "screenshot"
#define kSCREENSHOT_FILE_EXTENSION ".png"

using namespace std;

class SDLSurfaceSaverToPNG {
public:
    SDLSurfaceSaverToPNG();
    SDLSurfaceSaverToPNG(const SDLSurfaceSaverToPNG& orig);
    virtual ~SDLSurfaceSaverToPNG();
    
    static string getAlphaNumericString(string inputString);
    static void saveSurface(SDL_Surface *surfaceToSave);
    
private:

};

#endif	/* SDLSURFACESAVERTOPNG_H */

