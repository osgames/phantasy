/* 
 * File:   Hero.h
 * Author: User
 *
 * Created on June 29, 2012, 11:15 PM
 */

#ifndef HERO_H
#define	HERO_H

#include <string>
#include <map>
#include "GameItem.h"
#include "SDL/SDL.h"
#include "debuglog.h"
#include "ConfigParser.h"
#include "GameObject.h"
#include "GameMagic.h"
#include "GameTechnique.h"
#include "GameSkill.h"
#include "GameMagicDatabase.h"
#include "GameTechniqueDatabase.h"
#include "GameSkillsDatabase.h"
#include "GameItemsDatabase.h"

using namespace std;

class Hero {
public:
    Hero();
    Hero(const Hero& orig);
    virtual ~Hero();
    
    //get
    string getName();
    string getJob();
    
    int getAge();
    
    int getNowHealth();
    int getMaxHealth();
    
    int getNowTechPoints();
    int getMaxTechPoints();
    
    int getNowMagicPoints();
    int getMaxMagicPoints();
    
    int getStrength();
    int getMental();
    int getAgility();
    int getDexterity();
    
    int getMinAttackPower();
    int getMinDefencePower();
    
    int getMaxAttackPower();
    int getMaxDefencePower();    
    
    int getNowExperience();
    int getMaxExperience();
    int getNowLevel();

    //set    
    void setName(string newName);
    void setJob(string newJob);
    
    void setAge(int newAge);
    
    void setNowHealth(int newNowHealth);
    void setMaxHealth(int newMaxHealth);
    
    void setNowTechPoints(int newNowTechPoints);
    void setMaxTechPoints(int newMaxTechPoints);
    
    void setStrength(int newStrength);
    void setMental(int newMental);
    void setAgility(int newAgility);
    void setDexterity(int newDexterity);
    
    void setMinAttackPower(int newMinAttackPower);
    void setMinDefencePower(int newMinDefencePower);
    
    void setNowExperience(int newNowExperience);
    void setMaxExperience(int newMaxExperience);
    void setNowLevel(int newNowLevel);
    
    void setNowMagicPoints(int newNowMagicPoints);
    void setMaxMagicPoints(int newMaxMagicPoints);
    
    void setMaxAttackPower(int newMaxAttackPower);
    void setMaxDefencePower(int newMaxDefencePower);
    
    void addExperience(int newExperience);
    
    GameItem* getHelmet();
    GameItem* getLeftArm();
    GameItem* getRightArm();
    GameItem* getBoots();
    
    SDL_Surface *getIcon();
    
    //get
    int getGameMagicCount();
    GameMagic* getGameMagicAtIndex(int index);
    //set
    void addGameMagic(GameMagic *newGameMagic);
    void addGameMagicSystem(vector<GameMagic*>newGameMagic);
    
    vector<GameTechnique*>gameTechnique;
    //get
    int getGameTechniqueCount();
    GameTechnique* getGameTechniqueAtIndex(int index);
    //set
    void addGameTechnique(GameTechnique *newGameTechnique);
    void addGameTechniqueSystem(vector<GameTechnique*>newGameTechnique);
    

    //get
    int getGameSkillsCount();
    GameSkill* getGameSkillAtIndex(int index);
    //set
    void addGameSkill(GameSkill *newGameSkill);
    void addGameSkillsSystem(vector<GameSkill*>newGameSkills);
    
    int magicCount();
    GameMagic* magicAtIndex(int magicIndex);
    
    int techniqueCount();
    GameTechnique* techniqueAtIndex(int techniqueIndex);
    
    void damage(int damagePower);
    
    void putHelmet(GameItem *newHelmet);
    void putLeftArm(GameItem *newLeftArm);
    void putRightArm(GameItem *newRightArm);
    void putBoots(GameItem *newBoots);
    
    void setPriorityAtTeam(int newPriority);
    int getPriorityAtTeam();
    
    bool isAlive();
    
    //equipment specialization
    bool canEquipItemWithSpecialization(int itemEquipSpecialization);
    void addEquipmentSpecialization(int newEquipmentSpecialization);
    
    //get equipment
    GameItem* getHeadEquipment();
    GameItem* getRightHandEquipment();
    GameItem* getLeftHandEquipment();
    GameItem* getBodyEquipment();
    
    //set equipment
    void setHeadEquipment(GameItem *newHeadEquipment);
    void setRightHandEquipment(GameItem *newRightHandEquipment);
    void setLeftHandEquipment(GameItem *newLeftHandEquipment);
    void setBodyEquipment(GameItem *newBodyEquipment);
    
    //free
    void freeHeadEquipment();
    void freeRightHandEquipment();
    void freeLeftHandEquipment();
    void freeBodyEquipment();
    
    //import export
    static Hero *loadHeroFromFilepath(char *filepath, char *gameModule, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase);
    static Hero *loadHeroFileFromName(char *gameModule, string heroName, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase);
    
    GameObject *getGameObject();
    void setGameObject(GameObject *newGameObject);
    
protected:

    bool alive;
    string name;
    string job;

    vector<GameMagic*>gameMagic;
    vector<GameSkill*>gameSkills;    
    
    int age;
    
    int nowHealth;
    int maxHealth;
    
    int nowTechPoints;
    int maxTechPoints;
    
    int nowMagicPoints;
    int maxMagicPoints;
    
    int strength;
    int mental;
    int agility;
    int dexterity;
    
    int minAttackPower;
    int minDefencePower;
    
    int maxAttackPower;
    int maxDefencePower;
    
    int nowExperience;
    int maxExperience;
    int nowLevel;
    
    //equipment
    GameItem *headEquipment;
    GameItem *rightHandEquipment;
    GameItem *leftHandEquipment;
    GameItem *bodyEquipment;
    
    //specialiasation
    vector<int>equipmentSpecialization;
    
    int priorityAtTeam;
    
    GameObject* gameObject;
    
};

#endif	/* HERO_H */

