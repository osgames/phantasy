/* 
 * File:   ObjectsRender.cpp
 * Author: demensdeum
 * 
 * Created on December 17, 2011, 8:52 PM
 */

#include "ObjectsRender.h"
#include "debuglog.h"


ObjectsRender::ObjectsRender() {
}

ObjectsRender::ObjectsRender(const ObjectsRender& orig) {
}

ObjectsRender::~ObjectsRender() {
}



void ObjectsRender::drawSector(ObjectsSector *sectorToDraw, int drawX, int drawY, SDL_Surface *drawSurface) {
    
    //TODO full implementation  
    
    int count=sectorToDraw->getSize();
    
    for (int a=0;a<count;a++) {
        
        GameObject *nowObject=sectorToDraw->getObjectAt(a);
        SDL_Surface *objectImage=nowObject->getImage();

        SDL_Rect source;
        source.x=0;
        source.y=0;
        source.w=objectImage->w;
        source.h=objectImage->h;
    
        SDL_Rect destination;
        destination.x=drawX;
        destination.y=drawY;
        destination.w=source.w;
        destination.h=source.h;         
        
        SDL_BlitSurface(objectImage,&source,drawSurface,&destination);
        
    }
    
    
}

void ObjectsRender::renderObjectsFromPositionToSurface(GameObjectPosition* cameraPosition, ObjectsArray* objectsArray,SDL_Surface *drawSurface) {
    
    int surfaceWidth=drawSurface->w;
    int surfaceHeight=drawSurface->h;
    
    int sectorX=cameraPosition->getX();
    int sectorY=cameraPosition->getY();
    
    int drawX=0;
    int drawY=0;
    
    bool draw=true;
    
    while(draw) {
        
        ObjectsSector *nowSector=objectsArray->getSector(sectorX,sectorY);
        
        this->drawSector(nowSector,drawX,drawY,drawSurface);
        
        if (drawX>surfaceWidth) {
            
            if (drawY>surfaceHeight) {
            
                break;
                
            }
            else {
                
                drawX=0;
                drawY+=nowSector->getHeight();
                sectorY++;
                
            }
            
        }
        else {
            
            drawX+=nowSector->getWidth();
            sectorX++;
            
        }        
        
        
    }
    
}
