/* 
 * File:   MapMaster.cpp
 * Author: demensdeum
 * 
 * Created on March 17, 2012, 2:02 PM
 */

#define alternativeIOMode

#include "MapMaster.h"
#include <cstddef>
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <queue>
#include <map>

#include "debuglog.h"
#include "Const.h"

int** Make2DintArray(int arraySizeX, int arraySizeY) {  
    int** theArray;  
    theArray = (int**) malloc(arraySizeX*sizeof(int*));  
    int i=0;
    for (i = 0; i < arraySizeX; i++)  
       theArray[i] = (int*) malloc(arraySizeY*sizeof(int));
    
       return theArray;      
}




MapMaster::MapMaster() {
    
    map=NULL;
    
}

MapMaster::MapMaster(const MapMaster& orig) {
}

MapMaster::~MapMaster() {
    
    if (map!=NULL)
        free(map);
    
}

queue<DialogMessage> loadDialogFile(char *dialogFilePath) {
    
    queue<DialogMessage>outputMessages;
    
    FILE *inputFile=NULL;
    
    inputFile=fopen(dialogFilePath,"r");
    
    if (!inputFile) {
        debugLog("Error loading dialog file:"<<dialogFilePath<<"\n");
        return outputMessages;
    }
    
    int messagesCount=0;
    
    if (fread(&messagesCount,1,sizeof(messagesCount),inputFile)!=sizeof(messagesCount)) {
        
        debugLog("Error loading messages count\n");
        return outputMessages;
    }
    
    for (int a=0;a<messagesCount;a++) {
        
        DialogMessage loadedMessage;
        
        if (fread(&loadedMessage,1,sizeof(DialogMessage),inputFile)!=sizeof(loadedMessage)) {
            
            debugLog("Error loading message\n");
            return outputMessages;
            
        }
        
        outputMessages.push(loadedMessage);
        
    }
    
    fclose(inputFile);
    
    return outputMessages;
    
}

void MapMaster::setTile(int tileX, int tileY, int tileNumber) {
    
    if (map!=NULL)
        map[tileY][tileX]=tileNumber;
    
}

int MapMaster::getTile(int tileX, int tileY) {
    
    if (map!=NULL)
        return map[tileY][tileX];
        
    return -1;
    
}

/*void MapMaster::fixTilesArrangement() {
    
    if (map!=NULL)
        for (int a=0;a<kDefaultMapSize;a++)
            for (int b=0;b<kDefaultMapSize;b++) {
                
                int nowTile=map[a][b];
                
                if (nowTile!=-1) {
                    
                    int nowTileIndex=nowTile/kDefaultTilesInTileset;
                    
                    
                }
                
            }
    
}*/

void MapMaster::newBlankInvisibleMap() {
    
    map=Make2DintArray(kDefaultMapSize,kDefaultMapSize);
   
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++) {
                map[a][b]=-1;   
        }
    
}

void MapMaster::newBlankDefaultMap() {
    
    map=Make2DintArray(kDefaultMapSize,kDefaultMapSize);
   
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++) {
                map[a][b]=kDefaultTile;   
        }
    
}

void MapMaster::loadRenderLayer(char *filename, long int *returnPosition) {
    
    long int stopPosition=0;
    memcpy(&stopPosition,returnPosition,sizeof(unsigned long));
    
    FILE *mapFile;
    
    if (map==NULL) {
    //TODO free map memory
    map=Make2DintArray(kDefaultMapSize,kDefaultMapSize);
    }
    
    debugLog("map[0][0]="<<map[0][0]<<"\n");
    
    if((mapFile=fopen(filename, "rb"))==NULL) {
        printf("Cannot open map file: %s\n",filename);
        return;
    }    
    
    fseek(mapFile,stopPosition,SEEK_SET);
    debugLog("set read map from:"<<ftell(mapFile)<<"\n");
    
    for (int a=0;a<kDefaultMapSize;a++)
       for (int b=0;b<kDefaultMapSize;b++)
           if (fread(&map[a][b],sizeof(int),1,mapFile)!=1) {
               debugLog("error while load map\n");   
#ifdef DEBUG
               perror("error occured");
#endif
               return;
           }
    
    stopPosition=ftell(mapFile);
    
    fclose(mapFile);
    
    memcpy(returnPosition,&stopPosition,sizeof(unsigned long));
    
}

void MapMaster::saveMap(char* filePath, vector<Tile*>&tiles, vector<GameObject*>& gameObjects, RenderLayer** renderLayers, MapMaster **mapLayers, int arrangementMap[][kDefaultMapSize*2][kDefaultMapSize*2], int wallsArrangementMap[][kDefaultMapSize], int solidMap[kDefaultMapSize*4][kDefaultMapSize*4], vector<string>&scripts) {
    
    int tilesCount=0;
    int gameObjectsParentCount=0;
    int gameObjectsCount=0;
    int scriptsCount=0;
    
    //write data
    for (int a=0;a<kMaxGraphicsLayers;a++)
        mapLayers[a]->saveRenderLayer(filePath);
    
    FILE *mapFile=NULL;
    mapFile=fopen(filePath,"a");
    
    if (mapFile==NULL) {
        debugLog("error loading map");
        return;
    }    
    
    //save arrangement map
    for (int a=0;a<kArrangementMapCount;a++)
        for (int b=0;b<kDefaultMapSize*2;b++)
            for (int c=0;c<kDefaultMapSize*2;c++)
                if (fwrite(&arrangementMap[a][b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }
                    
    
    //save wall arrangement map
        for (int b=0;b<kDefaultMapSize;b++)
            for (int c=0;c<kDefaultMapSize;c++)
                if (fwrite(&wallsArrangementMap[b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }    
    
    //solid map
         for (int b=0;b<kDefaultMapSize*4;b++)
            for (int c=0;c<kDefaultMapSize*4;c++)
                if (fwrite(&solidMap[b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }  
    
    //write tiles count
    tilesCount=tiles.size();
    if (fwrite(&tilesCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error write map\n");
        goto error;
    }  
    
    //save tiles
    for (int a=0;a<tiles.size();a++) {
        Tile *nowTile=tiles[a];
        char exportTilePath[kLengthTilePath];
        strncpy(exportTilePath,nowTile->getFullPath().c_str(),kLengthTilePath);
        if (fwrite(exportTilePath,kLengthTilePath,1,mapFile)!=1) {
                debugLog("error write map\n");
                goto error;
        }  
        
    }
    
    //save objects parents
    gameObjectsParentCount=gameObjects.size();
    if (fwrite(&gameObjectsParentCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error write map\n");
        goto error;
    }  
    
    for (int a=0;a<gameObjectsParentCount;a++) {
        GameObject *nowParent=gameObjects[a];
        char exportParentPath[kLengthTilePath];
        strncpy(exportParentPath,nowParent->getFullPath().c_str(),kLengthTilePath);
        if (fwrite(exportParentPath,kLengthTilePath,1,mapFile)!=1) {
                debugLog("error write map\n");
                goto error;
        }  
        
    }
     
    //save game objects
    gameObjectsCount=renderLayers[kObjectsRenderLayer]->getObjectsCount();
    if (fwrite(&gameObjectsCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error write map\n");
        goto error;
    }  

    for (int a=0;a<gameObjectsCount;a++) {
        GameObject::GameObjectStruct nowGameObject=renderLayers[kObjectsRenderLayer]->getGameObject(a)->exportAsGameObject();
        if (fwrite(&nowGameObject,sizeof(GameObject::GameObjectStruct),1,mapFile)!=1) {
                debugLog("error write map\n");
                goto error;           
        }  
    }
    
    //save scripts
    scriptsCount=scripts.size();
    
    if (fwrite(&scriptsCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error write map\n");
        goto error;
    }
    
    for (int a=0;a<scriptsCount;a++) {
        
        string nowString=scripts[a];
        char scriptString[kMaxScriptLength];
        strncpy(scriptString,nowString.c_str(),kMaxScriptLength);
        
        if (fwrite(scriptString,kMaxScriptLength,1,mapFile)!=1) {
            
            debugLog("error write map\n");
            goto error;
            
        }
            
        
    }
        
    
error:
    fclose(mapFile);    
    
}

GameObject* MapMaster::getParentObjectByName(char *parentName, vector<GameObject*>&gameObjects) {
    
    for (int a=0;a<gameObjects.size();a++) {
        
        GameObject *nowObject=gameObjects[a];
        const char *nowObjectName=nowObject->getName().c_str();
        
        if (strncmp(parentName,nowObjectName,kMaxObjectNameLength)==0) {
            
            return nowObject;
            
        }
        
    }
    
    debugLog("Can't find parent object for name:"<<parentName<<"\n");
    return NULL;    
    
}

void MapMaster::loadGameObject(GameObject::GameObjectStruct gameObjectStruct, RenderLayer** renderLayers, vector<GameObject*>gameObjects) {
    
    GameObject *parentObject=MapMaster::getParentObjectByName(gameObjectStruct.parentName,gameObjects);
    if (parentObject==NULL)
        return;
    
    GameObject *newObject=parentObject->getCopy();
    if (newObject==NULL)
        return;
    
    newObject->setX(gameObjectStruct.positionX);
    newObject->setY(gameObjectStruct.positionY);
    //newObject->setPosition(new GameObjectPosition(decorationStruct.positionX,decorationStruct.positionY));
    newObject->setAnimationDirection(gameObjectStruct.animationDirection);
    newObject->setScriptIndex(gameObjectStruct.scriptIndex);
    renderLayers[kObjectsRenderLayer]->loadObject(newObject);
    
    
}

void MapMaster::loadMap(char* filePath, vector<Tile*>&tiles, vector<GameObject*>& gameObjects, RenderLayer** renderLayers, MapMaster **mapLayers, int arrangementMap[][kDefaultMapSize*2][kDefaultMapSize*2], int wallsArrangementMap[][kDefaultMapSize], int solidMap[kDefaultMapSize*4][kDefaultMapSize*4], vector<string>&scripts, std::map<string,string>&mapProperties) {

    debugLog("open map system\n");
    

    
    //free memory
    //remove all tiles
    renderLayers[kObjectsRenderLayer]->removeAllObjects();  
    for (int a=0;a<tiles.size();a++)
        delete tiles[a];
    tiles.clear();
    for (int a=0;a<gameObjects.size();a++)
        delete gameObjects[a];
    gameObjects.clear();    
    
    //write data
    long int mapLoadStopPosition=0;
    int tilesCount=0;
    int gameObjectsParentCount=0;
    int gameObjectsCount=0;
    int scriptsCount=0;
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        debugLog("Map load position:"<<mapLoadStopPosition<<"\n");
        mapLayers[a]->loadRenderLayer(filePath,&mapLoadStopPosition);
    }
    
    FILE *mapFile=NULL;
    mapFile=fopen(filePath,"r");
    
    if (mapFile==NULL) {
        debugLog("error loading map");
        return;
    }
    fseek(mapFile,mapLoadStopPosition,SEEK_SET);
    
    //save arrangement map
    for (int a=0;a<kArrangementMapCount;a++)
        for (int b=0;b<kDefaultMapSize*2;b++)
            for (int c=0;c<kDefaultMapSize*2;c++)
                if (fread(&arrangementMap[a][b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }
                    
    
    //save wall arrangement map
        for (int b=0;b<kDefaultMapSize;b++)
            for (int c=0;c<kDefaultMapSize;c++)
                if (fread(&wallsArrangementMap[b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }    
    
    //solid map
         for (int b=0;b<kDefaultMapSize*4;b++)
            for (int c=0;c<kDefaultMapSize*4;c++)
                if (fread(&solidMap[b][c],sizeof(int),1,mapFile)!=1) {
                    debugLog("error write map\n");
                    goto error;
                }  

    //write tiles count
    if (fread(&tilesCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error write map\n");
        goto error;
    }  
    


    
    //load tiles
    for (int a=0;a<tilesCount;a++) {
        char importTilePath[kLengthTilePath];
        if (fread(importTilePath,kLengthTilePath,1,mapFile)!=1) {
                debugLog("error write map\n");
                goto error;
        }  
        debugLog("Imported tile full path:"<<importTilePath<<"\n");
        ResourcesManager::loadTile((char*)importTilePath,tiles);
        
    }
    
    if (fread(&gameObjectsParentCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error read map\n");
        goto error;
    }  
    
    //read parents
    for (int a=0;a<gameObjectsParentCount;a++) {
        char importParentPath[kLengthTilePath];
        if (fread(importParentPath,kLengthTilePath,1,mapFile)!=1) {
                debugLog("error write map\n");
                goto error;
        }  
        
        debugLog("Imported object parent full path:"<<importParentPath<<"\n");
        ResourcesManager::loadObject((char*)importParentPath,gameObjects);
        
    }  
    
   //save game objects
    gameObjectsCount=renderLayers[kObjectsRenderLayer]->getObjectsCount();
    if (fread(&gameObjectsCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error open map\n");
        goto error;
    }  

    for (int a=0;a<gameObjectsCount;a++) {
        GameObject::GameObjectStruct nowGameObject;
        if (fread(&nowGameObject,sizeof(GameObject::GameObjectStruct),1,mapFile)!=1) {
                debugLog("error open map\n");
                goto error;           
        }
        MapMaster::loadGameObject(nowGameObject, renderLayers, gameObjects);
    }
     
    scripts.clear();
    
    //load scripts
    if (fread(&scriptsCount,sizeof(int),1,mapFile)!=1) {
        debugLog("error load scripts\n");
        goto error;
    }
    
    for (int a=0;a<scriptsCount;a++) {
        
        char scriptString[kMaxScriptLength];
        
        if (fread(scriptString,kMaxScriptLength,1,mapFile)!=1) {
            
            debugLog("error load scripts\n");
            goto error;
            
        }
        
        string loadedScript=scriptString;
        scripts.push_back(loadedScript);
            
        
    }

    //loading map config file

        
    char mapPropertiesFilePath[512];
    snprintf(mapPropertiesFilePath,512,"%s.ini",filePath);
        
    mapProperties=ConfigParser::parseConfigFile(mapPropertiesFilePath,NULL);

    
error:
    fclose(mapFile);    


}

void MapMaster::saveRenderLayer(char *filename) {
    
    FILE *mapFile;
    
    //TODO free map memory
    
    if((mapFile=fopen(filename, "ab"))==NULL) {
        printf("Cannot open map file.\n");
    }    
    
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++)
            if (fwrite(&map[a][b],sizeof(int),1,mapFile)!=1)
                debugLog("error while save map");
    
        debugLog("Last byte on write:"<<ftell(mapFile)<<"\n");
    
    fclose(mapFile);
    
}

void MapMaster::generateMap() {
    
    map=Make2DintArray(kDefaultMapSize,kDefaultMapSize);
   
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++)
                map[a][b]=rand()%4;

    
    
    
}

int** MapMaster::getMap() {
    
    return map;
    
}
