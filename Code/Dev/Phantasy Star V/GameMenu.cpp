/* 
 * File:   GameMenu.cpp
 * Author: demensdeum
 * 
 * Created on January 27, 2013, 12:35 AM
 */

#include "GameMenu.h"
#include "MenuController.h"

GameMenu::GameMenu() {
}

GameMenu::GameMenu(const GameMenu& orig) {
}

GameMenu::~GameMenu() {
}

void GameMenu::getPlayerProfileNameFromPlayerByMenu(char *outputPlayerProfileName) {
    //TODO implement
    snprintf(outputPlayerProfileName,256,"user");
}

void GameMenu::getGameModuleNameFromPlayerByMenu(char *playerProfileName,char *outputGameModuleName) {
    //TODO implement
    snprintf(outputGameModuleName,256,"rebirthdarkpr");
}

int GameMenu::getGameModuleMainMenuChoiceFromPlayer(char *playerProfileName, char *gameModuleName, SDL_Surface *drawSurface, InputController *inputController) {
    
    //draw background
    char backgroundPath[512];
    snprintf(backgroundPath,512,"%s/%s/%s",kGameModulesPath,gameModuleName,kBackgroundMenuImagePath);
    
    debugLog("Menu background image path: "<<backgroundPath<<"\n");

    char musicPath[512];
    snprintf(musicPath,512,"%s/%s/%s",kGameModulesPath,gameModuleName,kMenuMusicPath);
    
    debugLog("Music path:"<<musicPath<<"\n");
    
    Mix_Music *menuMusic = NULL;
    menuMusic=Mix_LoadMUS(musicPath);
    
    Mix_PlayMusic(menuMusic, 0);
    
    SDL_Surface *menuBackgroundImage=IMG_Load(backgroundPath);
    SDL_BlitSurface(menuBackgroundImage,NULL,drawSurface,NULL);
    SDL_FreeSurface(menuBackgroundImage);
    
    {
        
        TTF_Font *font = TTF_OpenFont("./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf",18);
        if (font==NULL)
        debugLog("Can't load font ./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf\nMaybe SDL_TTF not inited\n");
        
        SDL_Color fColor;
        fColor.r = fColor.g = fColor.b = 255; 
        
        char buildVersionText[256];
        snprintf(buildVersionText,256,"Algo Engine %s",kGameVersion);
        
        SDL_Surface *drawText=TTF_RenderUTF8_Blended(font, buildVersionText, fColor);
        
        SDL_Rect destinationRect;
        destinationRect.w=drawText->w;
        destinationRect.h=drawText->h;
        destinationRect.x=drawSurface->w-drawText->w;
        destinationRect.y=drawSurface->h-drawText->h;
        
        destinationRect.x-=20;
        destinationRect.y-=10;
        
        SDL_BlitSurface(drawText,NULL,drawSurface,&destinationRect);
        
    }
    
    //TODO implement
    MenuRender::makeMenu((char**)kGameMenuMainMenuItems,100,100,drawSurface);
    GraphicEffects::fadeIn(drawSurface,NULL);
    MenuController::returnChoice(kGameMenuMainMenuLength,100,100,drawSurface,inputController);
}