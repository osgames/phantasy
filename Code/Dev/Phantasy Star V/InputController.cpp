/* 
 * File:   InputController.cpp
 * Author: User
 * 
 * Created on March 10, 2012, 1:58 PM
 * 
 * 
 * 
 */

#include "InputController.h"
#include "debuglog.h"
#include "GameObjectCommands.h"

InputController::InputController() {
    
    applicationQuit=false;
    windowResize=false;    
    mouseLeftClick=false;
    mouseRightClick=false;
     
    goLeft=false;
    goRight=false;
    goUp=false;
    goDown=false;
    useButton=false;
    menuButton=false;
    yesButton=false;
    noButton=false;
    useButton=false;
    escapeButton=false;
    
    cursorX=false;
    cursorY=false;
    
    cursorPress=false;
    lastCommand=false;
    
    deleteButton=false;
    masterConsoleButton=false; //f5
    
    mouseScrollUp=false;
    mouseScrollDown=false;
    
    screenshotButton=false;
    refreshMapButton=false;

    
}

InputController::InputController(const InputController& orig) {
}

InputController::~InputController() {
}

bool InputController::getMouseScrollUp() {
    
    return mouseScrollUp;
    
}
bool InputController::getMouseScrollDown() {

    return mouseScrollDown;
    
}

void InputController::allButtonsToUnpress() {
    
    mouseLeftClick=false;
    mouseRightClick=false;
    goLeft=false;
    goRight=false;
    goDown=false;
    goUp=false;    
    yesButton=false;
    useButton=false;
    escapeButton=false;
    masterConsoleButton=false;
    mouseScrollUp=false;
    mouseScrollDown=false;
    screenshotButton=false; 
    refreshMapButton=false;
    controlButton=false;
    
}

bool InputController::getScreenshotButton() {
    
    return screenshotButton;
    
}

bool InputController::getMasterConsoleButton() {
    
    return masterConsoleButton;
    
}

bool InputController::getUseButton() {
    
    return useButton;
    
}

void InputController::unpressUseButton() {
    
    useButton=false;
    
}

bool InputController::getYesButton() {
    
    return yesButton;
    
}

int InputController::getLastCommand() {
    
    if (goLeft)
        lastCommand=kCommandGoLeft;
    else
    if (goRight)
        lastCommand=kCommandGoRight;
    else
    if (goUp)
        lastCommand=kCommandGoUp;
    else
    if (goDown)
        lastCommand=kCommandGoDown;
    else
    if (yesButton)
        lastCommand=kCommandYesButton;
    else
        lastCommand=kCommandNone;
    
    return lastCommand;
    
}

void InputController::unpressMouse() {
    
    mouseLeftClick=false;
    
}

bool InputController::getLeftMouseClick() {
    
    return mouseLeftClick;
    
}

bool InputController::getRightMouseClick() {
    
    return mouseRightClick;
    
}

int InputController::getMouseY() {
    
    return mouseY;
    
}

int InputController::getMouseX() {
    
    return mouseX;
    
}

int InputController::getWindowWidth() {
    
    return windowWidth;
    
}

int InputController::getWindowHeight() {
    
    return windowHeight;
    
}

bool InputController::getWindowResize() {
    
    bool outputWindowResize=false;
    
    if (windowResize)
        outputWindowResize=true;
    
    windowResize=false;
    
    return outputWindowResize;
    
}

bool InputController::getDeleteButton() {
    
    return deleteButton;
    
}

bool InputController::getApplicationQuit() {
    
    return applicationQuit;
    
}

bool InputController::getEscapeButton() {
    
    return escapeButton;
            
}

bool InputController::getRefreshMapButton() {
    
    return refreshMapButton;
    
}

bool InputController::getControlButton() {
    return controlButton;
}

void InputController::pollEvents() {
    
  SDL_Event event;    
    
  while( SDL_PollEvent( &event ) ){
    /* We are only worried about SDL_KEYDOWN and SDL_KEYUP events */
    switch( event.type ){
        
        case SDL_MOUSEMOTION:
            
            mouseX=event.button.x;
            mouseY=event.button.y;
            break;
            
        case SDL_MOUSEBUTTONDOWN:
            
            if (event.button.button==SDL_BUTTON_LEFT)
                mouseLeftClick=true;
            
            else if (event.button.button==SDL_BUTTON_RIGHT)
                mouseRightClick=true;
            
            else if (event.button.button==SDL_BUTTON_WHEELDOWN)
                mouseScrollDown=true;
            
            else if (event.button.button==SDL_BUTTON_WHEELUP)
                mouseScrollUp=true;
            
            break;
            
        case SDL_MOUSEBUTTONUP:
            
            if (event.button.button==SDL_BUTTON_LEFT)
                mouseLeftClick=false;
            
            else if (event.button.button==SDL_BUTTON_RIGHT)
                mouseRightClick=false;
            
            break;

        
      case SDL_KEYDOWN:
        
          switch( event.key.keysym.sym ){
          
              case SDLK_F6:
                  masterConsoleButton=true;
                  break;
                  
              case SDLK_F5:
                  refreshMapButton=true;
                  break;
                  
              case SDLK_F12:
                  screenshotButton=true;
                  break;
                  
              case SDLK_LEFT:
                  goLeft=true;
                  break;
                  
              case SDLK_RIGHT:
                  goRight=true;
                  break;
                  
              case SDLK_UP:
                  goUp=true;
                  break;
                  
              case SDLK_DOWN:
                  goDown=true;
                  break;
                  
              case SDLK_RETURN:
                  yesButton=true;
                  break;
                
              case SDLK_SPACE:
                  useButton=true;
                  break;
                  
              case SDLK_ESCAPE:
                  escapeButton=true;
                  break;
              
              case SDLK_DELETE:
                  deleteButton=true;
                  break;
                  
              case SDLK_LCTRL:
                  controlButton=true;
                  break;
                  
              default:
                  break;
              
          }
          
        break;

      case SDL_KEYUP:
          switch( event.key.keysym.sym ){
          
              case SDLK_F6:
                  masterConsoleButton=false;
                  break;              
              
              case SDLK_F5:
                  refreshMapButton=false;
                  break;                  
                  
              case SDLK_F12:
                  screenshotButton=false;
                  break;                  
                  
              case SDLK_LEFT:
                  debugLog("left");
                  goLeft=false;
                  break;
                  
              case SDLK_RIGHT:
                  debugLog("right");
                  goRight=false;
                  break;
                  
              case SDLK_UP:
                  debugLog("up");
                  goUp=false;
                  break;
                  
              case SDLK_DOWN:
                  debugLog("down");
                  goDown=false;
                  break;

              case SDLK_RETURN:
                  yesButton=false;
                  break;   
                  
              case SDLK_SPACE:
                  useButton=false;
                  break;

              case SDLK_ESCAPE:
                  escapeButton=false;
                  break;
                  
              case SDLK_DELETE:
                  deleteButton=false;
                  break;     
                  
              case SDLK_LCTRL:
                  controlButton=false;
                  break;                  
                  
              default:
                  debugLog("Unknown key:"<<event.key.keysym.sym<<"\n");
                  break;
                  
          }
        break;

      case SDL_QUIT:
                    
        applicationQuit=true;
        break;
        
      case SDL_VIDEORESIZE:

        windowResize=true;
          
        windowWidth=event.resize.w;
        windowHeight=event.resize.h;
        break;
        
        
      default:
        break;
    }
  }    
    
}
