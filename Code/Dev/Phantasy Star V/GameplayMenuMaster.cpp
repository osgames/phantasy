/* 
 * File:   GameplayMenuMaster.cpp
 * Author: User
 * 
 * Created on June 30, 2012, 9:04 AM
 */

#include "GameplayMenuMaster.h"

GameplayMenuMaster::GameplayMenuMaster() {
}

GameplayMenuMaster::GameplayMenuMaster(const GameplayMenuMaster& orig) {
}

GameplayMenuMaster::~GameplayMenuMaster() {
}

int GameplayMenuMaster::youSureMenu(SDL_Surface *drawSurface,int menuX,int menuY, InputController *inputController) {
    
    MenuRender::makeMenu((char**)YesNoMenuString,menuX,menuY,drawSurface);
   return MenuController::returnChoice(kYouSureMenuLength,menuX,menuY,drawSurface,inputController); 
    
}

void GameplayMenuMaster::showMoney(SDL_Surface *drawSurface, int menuX,int menuY, PlayerTeam *playerTeam) {
    
signed long long money=playerTeam->getMoney();
        //char moneyString[kShortStringLength];
        //snprintf(moneyString,kShortStringLength,"%ll",money);
        
        debugLog("Money:"<<money<<"\n");
        
            char **outputStatusStrings;
            outputStatusStrings=(char**)malloc(2*sizeof(char*));
            outputStatusStrings[0]=(char*)malloc(kShortStringLength);//money
            outputStatusStrings[1]=NULL;
            
            snprintf(outputStatusStrings[0],kShortStringLength,"%s: %lld",kHeroStatusWindowMoney,money);
            
            //TODO implement heroes select
            //TODO correct window position

            //show status for all
            MenuRender::makeWindow(outputStatusStrings,menuX,menuY,drawSurface);
            
            free(outputStatusStrings[0]);
            free(outputStatusStrings[1]);
            free(outputStatusStrings);        
        


        SDL_Flip(drawSurface);    
    
}

void GameplayMenuMaster::showStatus(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController) {

    int heroesCount=playerTeam->getHeroesCount();
    if (heroesCount==0) {
        
        DialogRender::renderDialogBox((drawSurface->w/2)-kDialogBoxWidth/2,drawSurface->h-(kDialogBoxHeight+10),kMessageTeamEmpty,drawSurface);        
        SDL_Flip(drawSurface);
               
    }
    else {
        
        
        
        for (int a=0;a<heroesCount;a++) {
            
            Hero *nowHero=playerTeam->getHeroAtIndex(a);
            
            if (nowHero==NULL) {
                debugLog("nowHero are NULL");
                return;
            }
            
            if (nowHero->getName().empty()) {
                debugLog("Show status window hero name are empty.");
                return;
            }

            if (nowHero->getJob().empty()) {
                debugLog("Show status window hero job are empty.");
                return;
            }            
            
            char **outputStatusStrings;
            outputStatusStrings=(char**)malloc(11*sizeof(char*));
            outputStatusStrings[0]=(char*)malloc(kShortStringLength);//name
            outputStatusStrings[1]=(char*)malloc(kShortStringLength);//job
            outputStatusStrings[2]=(char*)malloc(kShortStringLength);//attack power
            outputStatusStrings[3]=(char*)malloc(kShortStringLength);//defence power
            outputStatusStrings[4]=(char*)malloc(kShortStringLength);//health
            outputStatusStrings[5]=(char*)malloc(kShortStringLength);//tech points
            outputStatusStrings[6]=(char*)malloc(kShortStringLength);//magic points
            outputStatusStrings[7]=(char*)malloc(kShortStringLength);//health
            outputStatusStrings[8]=(char*)malloc(kShortStringLength);//experience
            outputStatusStrings[9]=(char*)malloc(kShortStringLength);//level
            outputStatusStrings[10]=NULL;//NULL
            
            snprintf(outputStatusStrings[0],kShortStringLength,"%s: %s",kHeroStatusWindowName,nowHero->getName().c_str());
            snprintf(outputStatusStrings[1],kShortStringLength,"%s: %s",kHeroStatusWindowJob,nowHero->getJob().c_str());
            snprintf(outputStatusStrings[2],kShortStringLength,"%s: %d/%d",kHeroStatusWindowAttackPower,nowHero->getMinAttackPower(),nowHero->getMaxAttackPower());
            snprintf(outputStatusStrings[3],kShortStringLength,"%s: %d/%d",kHeroStatusWindowDefencePower,nowHero->getMinDefencePower(),nowHero->getMaxDefencePower());
            snprintf(outputStatusStrings[4],kShortStringLength,"%s: %d",kHeroStatusWindowHealth,nowHero->getNowHealth());
            snprintf(outputStatusStrings[5],kShortStringLength,"%s: %d",kHeroStatusWindowTechPoints);
            snprintf(outputStatusStrings[6],kShortStringLength,"%s: %d",kHeroStatusWindowMagicPoints);
            snprintf(outputStatusStrings[7],kShortStringLength,"%s: %d/%d",kHeroStatusHealth,nowHero->getNowHealth(),nowHero->getMaxHealth());
            snprintf(outputStatusStrings[8],kShortStringLength,"%s: %d/%d",kHeroStatusExperience,nowHero->getNowExperience(),nowHero->getMaxExperience());
            snprintf(outputStatusStrings[9],kShortStringLength,"%s: %d",kHeroStatusLevel,nowHero->getNowLevel());
            
            //TODO implement heroes select
            //TODO correct window position

            //show status for all
            MenuRender::makeWindow(outputStatusStrings,100+300*a,100,drawSurface);
            
            free(outputStatusStrings[0]);
            free(outputStatusStrings[1]);
            free(outputStatusStrings[2]);
            free(outputStatusStrings);
            
        }
        
        GameplayMenuMaster::showMoney(drawSurface,menuX+340,menuY+340,playerTeam);
        
    }
    
    while (true) {
        inputController->pollEvents();
        if (inputController->getUseButton() || inputController->getEscapeButton() || inputController->getYesButton()) {
            inputController->allButtonsToUnpress();
            return;
        }
        else if (inputController->getApplicationQuit()) {
            return;
        }

    }
    
}

void GameplayMenuMaster::showItemUseResult(ShowItemResultStruct showItemResultStruct,SDL_Surface *drawSurface, PlayerTeam *playerTeam) {
    debugLog("Show item use is not implemented");
}

void GameplayMenuMaster::showDescriptionForGameItem(GameItem *gameItem, SDL_Surface *drawSurface, int drawX, int drawY, InputController *inputController) {
    
        //draw empty inventory message
    
        DialogRender::renderDialogBox((drawSurface->w/2)-kDialogBoxWidth/2,drawSurface->h-(kDialogBoxHeight+10),(char*)gameItem->getDescription().c_str(),drawSurface);        
        inputController->allButtonsToUnpress();
        SDL_Flip(drawSurface);
        while (true) {
            inputController->pollEvents();
            if (inputController->getUseButton() || inputController->getEscapeButton() || inputController->getYesButton()) {
                inputController->allButtonsToUnpress();
                return;
            }
            else if (inputController->getApplicationQuit()) {
                return;
            }
            
        }
    
}

void GameplayMenuMaster::showItemMenu(SDL_Surface *drawSurface, PlayerTeam *playerTeam, InputController *inputController, int itemIndex) {
    
    MenuRender::makeMenu((char**)kItemMenu,10,10,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(kItemMenuCount,10,10,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return;
    
    switch (choice) {
        case kItemMenuUse:
            GameplayMenuMaster::showItemUseResult(playerTeam->useItemFromInventory(itemIndex),drawSurface,playerTeam);
            break;
        case kItemMenuDescription:
            GameplayMenuMaster::showDescriptionForGameItem(playerTeam->getItemAtIndex(itemIndex),drawSurface,10,100,inputController);
            break;
        case kItemMenuDrop:
            playerTeam->dropItemFromInventory(itemIndex);
            break;
             
    }
    
}

//only draws inventory
void GameplayMenuMaster::drawInventory(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController) {
    
    int inventoryCount=playerTeam->getItemsCount();
    char *inventoryNames[inventoryCount+1];
    
    if (inventoryCount==0) {
        
        //draw empty inventory message
        DialogRender::renderDialogBox((drawSurface->w/2)-kDialogBoxWidth/2,drawSurface->h-(kDialogBoxHeight+10),kMessageInventoryEmpty,drawSurface);        
        inputController->allButtonsToUnpress();
        SDL_Flip(drawSurface);
        while (true) {
            inputController->pollEvents();
            if (inputController->getUseButton() || inputController->getEscapeButton() || inputController->getYesButton()) {
                inputController->allButtonsToUnpress();
                return;
            }
            else if (inputController->getApplicationQuit()) {
                return;
            }
            
        }
        
    }
    
    int a=0;
    for (;a<inventoryCount;a++) {
        
        GameItem *nowGameItem=playerTeam->getItemAtIndex(a);
        inventoryNames[a]=(char*)nowGameItem->getHumanReadableName().c_str();
        
    }
    inventoryNames[a]=NULL;
    
    MenuRender::makeMenu((char**)inventoryNames,10,10,drawSurface);
    SDL_Flip(drawSurface);

    
}

void GameplayMenuMaster::showInventory(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController) {
    
    GameplayMenuMaster::drawInventory(drawSurface,menuX,menuY,playerTeam,inputController);
    int inventoryCount=playerTeam->getItemsCount();
    int choice=MenuController::returnChoice(inventoryCount,10,10,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return;
    GameplayMenuMaster::showItemMenu(drawSurface,playerTeam,inputController,choice);
    
}

void GameplayMenuMaster::showTeamListMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {

    int heroesCount=playerTeam->getHeroesCount();
    char *heroesNames[heroesCount];
    
    int a=0;
    for (;a<heroesCount;a++) {
        
        Hero* nowHero=playerTeam->getHeroAtIndex(a);
        heroesNames[a]=(char*)nowHero->getName().c_str();
        
    }
    heroesNames[a]=NULL;
    
    MenuRender::makeMenu((char**)heroesNames,drawX,drawY,drawSurface);
    SDL_Flip(drawSurface);
}

int GameplayMenuMaster::selectTeamMember(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {
    if (playerTeam->getHeroesCount()==1) return 0;
    GameplayMenuMaster::showTeamListMenu(drawSurface,drawX,drawY,playerTeam,inputController);
    int choice=MenuController::returnChoice(playerTeam->getHeroesCount(),drawX,drawY,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return kMenuChoiceExit;
    return choice;
    
}

void GameplayMenuMaster::showMagicMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {
    
    int heroIndex=GameplayMenuMaster::selectTeamMember(drawSurface,drawX,drawY,playerTeam,inputController);
    if (heroIndex==kMenuChoiceExit) return;
    Hero *nowHero=playerTeam->getHeroAtIndex(heroIndex);
    
    int magicCount=nowHero->getGameMagicCount();
    char *gameTechniqueNames[magicCount];
    
    int a=0;
    for (;a<magicCount;a++) {
        
        GameMagic* nowGameMagic=nowHero->getGameMagicAtIndex(a);
        gameTechniqueNames[a]=(char*)nowGameMagic->getHumanReadableName().c_str();
        
    }
    gameTechniqueNames[a]=NULL;
    
    MenuRender::makeMenu((char**)gameTechniqueNames,drawX,drawY,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(magicCount,drawX,drawY,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return;
    
}

void GameplayMenuMaster::showSkillsMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {
    
    int heroIndex=GameplayMenuMaster::selectTeamMember(drawSurface,drawX,drawY,playerTeam,inputController);
    if (heroIndex==kMenuChoiceExit) return;
    Hero *nowHero=playerTeam->getHeroAtIndex(heroIndex);
    
    int skillsCount=nowHero->getGameSkillsCount();
    char *gameSkillsNames[skillsCount];
    
    int a=0;
    for (;a<skillsCount;a++) {
        
        GameSkill* nowGameSkill=nowHero->getGameSkillAtIndex(a);
        gameSkillsNames[a]=(char*)nowGameSkill->getHumanReadableName().c_str();
        
    }
    gameSkillsNames[a]=NULL;
    
    MenuRender::makeMenu((char**)gameSkillsNames,drawX,drawY,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(skillsCount,drawX,drawY,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return;    

}

void GameplayMenuMaster::showTechniqueMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {
    
    int heroIndex=GameplayMenuMaster::selectTeamMember(drawSurface,drawX,drawY,playerTeam,inputController);
    if (heroIndex==kMenuChoiceExit) return;
    Hero *nowHero=playerTeam->getHeroAtIndex(heroIndex);
    
    int techniqueCount=nowHero->getGameTechniqueCount();
    char *gameTechniqueNames[techniqueCount];
    
    int a=0;
    for (;a<techniqueCount;a++) {
        
        GameTechnique* nowGameTechnique=nowHero->getGameTechniqueAtIndex(a);
        gameTechniqueNames[a]=(char*)nowGameTechnique->getHumanReadableName().c_str();
        
    }
    gameTechniqueNames[a]=NULL;
    
    MenuRender::makeMenu((char**)gameTechniqueNames,drawX,drawY,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(techniqueCount,drawX,drawY,drawSurface,inputController);   
    if (choice==kMenuChoiceExit) return;
    
    
}

void GameplayMenuMaster::drawEquipmentOfHeroAtIndex(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController, int heroIndex) {
    
    Hero *targetHero=playerTeam->getHeroAtIndex(heroIndex);
    if (!targetHero) return;
    
    string headEquipmentString="<none>";
    string rightHandEquipmentString="<none>";
    string leftHandEquipmentString="<none>";
    string bodyEquipmentString="<none>";
    
    
    GameItem *headEquipment=targetHero->getHeadEquipment();
    GameItem *rightHandEquipment=targetHero->getRightHandEquipment();
    GameItem *leftHandEquipment=targetHero->getLeftHandEquipment();
    GameItem *bodyEquipment=targetHero->getBodyEquipment();
    
    if (headEquipment) headEquipmentString=headEquipment->getHumanReadableName();
    if (rightHandEquipment) rightHandEquipmentString=rightHandEquipment->getHumanReadableName();
    if (leftHandEquipment) leftHandEquipmentString=leftHandEquipment->getHumanReadableName();
    if (bodyEquipment) bodyEquipmentString=bodyEquipment->getHumanReadableName();
    
    char *outputEquipmentStrings[5];
    outputEquipmentStrings[0]=(char*)headEquipmentString.c_str();
    outputEquipmentStrings[1]=(char*)rightHandEquipmentString.c_str();
    outputEquipmentStrings[2]=(char*)leftHandEquipmentString.c_str();
    outputEquipmentStrings[3]=(char*)bodyEquipmentString.c_str();
    outputEquipmentStrings[4]=NULL;
            
    MenuRender::makeWindow(outputEquipmentStrings,drawX,drawY,drawSurface);
            
    
    
}

void GameplayMenuMaster::showEquipmentMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController) {
    
    int heroIndex=GameplayMenuMaster::selectTeamMember(drawSurface,drawX,drawY,playerTeam,inputController);
    if (heroIndex==kMenuChoiceExit) return;    
    
    //TODO unequip items
    //TODO show good/bad hero status difference for item
    //TODO show only equip items
    GameplayMenuMaster::drawEquipmentOfHeroAtIndex(drawSurface,drawX+300,drawY,playerTeam,inputController,heroIndex);
    GameplayMenuMaster::drawInventory(drawSurface,drawX,drawY,playerTeam,inputController);
    int inventoryCount=playerTeam->getItemsCount();
    int equipItemIndex=MenuController::returnChoice(inventoryCount,10,10,drawSurface,inputController);   
    if (equipItemIndex==kMenuChoiceExit) return;
    playerTeam->equipItemAtIndexForHeroAtIndex(equipItemIndex,heroIndex);
    
    
}

void GameplayMenuMaster::gameplayMenu(SDL_Surface *drawSurface, InputController *inputController, PlayerTeam *playerTeam) {
    
    //draw menu
    MenuRender::makeMenu((char**)GameplayMenuString,10,10,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(kGameplayMenuLength,10,10,drawSurface,inputController);
    
    switch (choice) {
        
        case kGameplayMenuStatus:
            GameplayMenuMaster::showStatus(drawSurface,20,20,playerTeam,inputController);
            break;
            
        case kGameplayMenuInventory:
            GameplayMenuMaster::showInventory(drawSurface,20,20,playerTeam,inputController);
            break;
        
        case kGameplayMenuTechnique:
            GameplayMenuMaster::showTechniqueMainMenu(drawSurface,20,20,playerTeam,inputController);
            break;
            
        case kGameplayMenuMagic:
            GameplayMenuMaster::showMagicMainMenu(drawSurface,20,20,playerTeam,inputController);
            break;
            
        case kGameplayMenuSkills:
            GameplayMenuMaster::showSkillsMainMenu(drawSurface,20,20,playerTeam,inputController);
            break;
            
        case kGameplayMenuEquipment:
            GameplayMenuMaster::showEquipmentMainMenu(drawSurface,20,20,playerTeam,inputController);
            break;
    }
    
}

void GameplayMenuMaster::showShopGoodsMenu(vector<ShopItem>goods, SDL_Surface *drawSurface,int menuX, int menuY, InputController *inputController) {
    
    char **names=(char**)malloc(sizeof(char*)*goods.size()+1);
    
    for (int a=0;a<goods.size()+1;a++) {
        
        names[a]=(char*)malloc(kShortStringLength);
        if (a==goods.size()) {
            names[a]=NULL;
        }
        else {
            snprintf(names[a],kShortStringLength,((GameItem*)goods[a].item)->getHumanReadableName().c_str());
        }
        
    }
    
    MenuRender::makeMenu((char**)names,menuX,menuY,drawSurface);
    SDL_Flip(drawSurface);    
    
}

void GameplayMenuMaster::showFighterStatusWindow(Fighter *nowFighter,SDL_Surface *drawSurface, int menuX,int menuY) {
    
            char **outputStatusStrings;
            outputStatusStrings=(char**)malloc(3*sizeof(char*));
            outputStatusStrings[0]=(char*)malloc(kShortStringLength);//name
            outputStatusStrings[1]=(char*)malloc(kShortStringLength);//health
            outputStatusStrings[2]=NULL;//NULL    
    
            snprintf(outputStatusStrings[0],kShortStringLength,"%s",nowFighter->getName().c_str());
            snprintf(outputStatusStrings[1],kShortStringLength,"%d/%d",nowFighter->getNowHealth(),nowFighter->getMaxHealth());
            
            MenuRender::makeWindow(outputStatusStrings,menuX,menuY,drawSurface);
            
            free(outputStatusStrings[2]);
            free(outputStatusStrings[1]);
            free(outputStatusStrings[0]);
            free(outputStatusStrings);
            
}

BattleModeAction GameplayMenuMaster::playerBattleModeEnemyChoice(SDL_Surface *drawSurface, InputController *inputController, vector<Fighter>&fighters, int masterFighterIndex) {
    
    vector<Fighter*>enemies;
    BattleModeAction outputAction;
    outputAction.action=kActionNothing;
    
    for (int a=0;a<fighters.size();a++) {
        
        Fighter *nowFighter=&fighters[a];
        if (nowFighter->getBattleModeSide()==kEnemyTeam && nowFighter->isAlive()) enemies.push_back(nowFighter);
        
    }
    
    char **names=(char**)malloc(sizeof(char*)*enemies.size()+1);
    
    for (int a=0;a<enemies.size()+1;a++) {
        
        names[a]=(char*)malloc(kShortStringLength);
        if (a==enemies.size()) {
            names[a]=NULL;
        }
        else {
            snprintf(names[a],kShortStringLength,enemies[a]->getName().c_str());
        }
        
    }
    
    MenuRender::makeMenu((char**)names,10,10,drawSurface);
    SDL_Flip(drawSurface);
    int choice=MenuController::returnChoice(enemies.size(),10,10,drawSurface,inputController);    
    
    if (choice==kMenuChoiceExit) return outputAction;
    outputAction.action=kActionAttack;
    outputAction.master=&fighters[masterFighterIndex];
    outputAction.target=enemies[choice];
    
}

BattleModeAction GameplayMenuMaster::playerBattleModeMenuForFighter(SDL_Surface *drawSurface, InputController *inputController, vector<Fighter>&fighters, int masterFighterIndex) {
    
    while (true) {
    
        MenuRender::makeMenu((char**)PlayerBattleModeMainMenuString,10,10,drawSurface);
        SDL_Flip(drawSurface);
        int choice=MenuController::returnChoice(kBattleMenuLength,10,10,drawSurface,inputController);

        BattleModeAction outputAction;
    
        switch (choice) {
        
           case kBattleMenuAttack:
               outputAction=GameplayMenuMaster::playerBattleModeEnemyChoice(drawSurface,inputController,fighters,masterFighterIndex);
               if (outputAction.action!=kActionNothing) return outputAction;
               continue;//TODO attack menu and others
                
            
           case kBattleMenuRunaway:
               outputAction.action=kActionRunaway;
               break;
            
        
        }
    
        return outputAction;
    
    }
    
}