/* 
 * File:   DialogMessage.h
 * Author: User
 *
 * Created on June 26, 2012, 9:56 PM
 */

#ifndef DIALOGMESSAGE_H
#define	DIALOGMESSAGE_H

#include "Const.h"

#ifdef	__cplusplus
extern "C" {
#endif

    struct DialogMessage {
        
        char message[kShortStringLength];
        char icon[kShortStringLength];
        
    };


#ifdef	__cplusplus
}
#endif

#endif	/* DIALOGMESSAGE_H */

