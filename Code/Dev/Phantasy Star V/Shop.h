/* 
 * File:   Shop.h
 * Author: User
 *
 * Created on August 18, 2012, 11:39 PM
 */

#ifndef SHOP_H
#define	SHOP_H

#include <map>
#include <vector>
#include "PlayerTeam.h"
#include <SDL/SDL.h>
#include "InputController.h"
#include <string>
#include "GameItem.h"
#include "GameItemsDatabase.h"
#include "GameplayMenuMaster.h"

class Shop {
public:
    Shop(vector<ShopItem>newGoods,SDL_Surface *newDrawSurface, PlayerTeam *newPlayerTeam, InputController *newInputController, GameItemsDatabase *newItemsDatabase, string newNotEnoughMoneyMessage, string newDontBuyThatMessage, string newWillBuyItForMessage);
    Shop(const Shop& orig);
    virtual ~Shop();
    
    static void loadShopWithName(char *shopName, char *gameModule, PlayerTeam *playerTeam, SDL_Surface *drawSurface, InputController *inputController, GameItemsDatabase *newItemsDatabase);
    static void startShop(char *shopConfigPath, PlayerTeam *playerTeam, SDL_Surface *drawSurface, InputController *inputController, GameItemsDatabase *newItemsDatabase);
    
    
private:

  
    
    void shopLoop();
    void buyMenu();
    void sellMenu();
    void buySellMenu();
    
    string notEnoughMoneyMessage;
    string dontBuyThatMessage;
    string willBuyItForMessage;
    
    PlayerTeam *playerTeam;
    SDL_Surface *drawSurface;
    InputController *inputController;
    vector<ShopItem>goods;
    GameItemsDatabase *itemsDatabase;
    
    
    
};

#endif	/* SHOP_H */

