/* 
 * File:   Shop.cpp
 * Author: User
 * 
 * Created on August 18, 2012, 11:39 PM
 */

#include "Shop.h"
#include "GameItemsDatabase.h"

Shop::Shop(vector<ShopItem>newGoods, SDL_Surface *newDrawSurface, PlayerTeam *newPlayerTeam, InputController *newInputController, GameItemsDatabase *newItemsDatabase, string newNotEnoughMoneyMessage, string newDontBuyThatMessage, string newWillBuyItForMessage) {
    playerTeam=newPlayerTeam;
    inputController=newInputController;
    drawSurface=newDrawSurface;
    itemsDatabase=newItemsDatabase;
    notEnoughMoneyMessage=newNotEnoughMoneyMessage;
    dontBuyThatMessage=newDontBuyThatMessage;
    willBuyItForMessage=newWillBuyItForMessage;
    goods=newGoods;
}

Shop::Shop(const Shop& orig) {
}

Shop::~Shop() {
}

void Shop::loadShopWithName(char *shopName, char *gameModule, PlayerTeam *playerTeam, SDL_Surface *drawSurface, InputController *inputController, GameItemsDatabase *newItemsDatabase) {
    
}

void Shop::buyMenu() {
    
    while (true) {
    
        GameplayMenuMaster::showMoney(drawSurface,100,100,playerTeam);
        GameplayMenuMaster::showShopGoodsMenu(goods,drawSurface,10,10,inputController);
        int choice=MenuController::returnChoice(goods.size(),10,10,drawSurface,inputController);
        
        if (choice==kMenuChoiceExit) break;
        
        ShopItem nowItem=goods[choice];
        
        if (playerTeam->getMoney()<nowItem.price) {
            DialogRender::renderDialogBox(10,200,(char*)notEnoughMoneyMessage.c_str(),drawSurface);
        }
        else {
            playerTeam->takeMoney(nowItem.price);
            playerTeam->giveItem((GameItem*)nowItem.item);
        }
        
        
        
    }
        
}

void Shop::sellMenu() {

    while (true) {
    
        GameplayMenuMaster::showMoney(drawSurface,100,100,playerTeam);
        GameplayMenuMaster::showInventory(drawSurface,10,10,playerTeam,inputController);
        int choice=MenuController::returnChoice(playerTeam->getItemsCount(),10,10,drawSurface,inputController);
        
        if (choice==kMenuChoiceExit) break;
        
        GameItem *nowItem=playerTeam->getItemAtIndex(choice);
        
        //TODO dont buy some items
        //todo price multiplier
        
        char willBuyIt[kShortStringLength];
        snprintf(willBuyIt,kShortStringLength,willBuyItForMessage.c_str(),(char*)nowItem->getHumanReadableName().c_str(),nowItem->getPrice());
        int confirm=GameplayMenuMaster::youSureMenu(drawSurface,100,100,inputController);
        
        if (confirm==kMenuItemYes) {
            playerTeam->removeItemAtIndex(choice);
            playerTeam->giveMoney(nowItem->getPrice());
        }
        
        
        
        
    }    
    
}

void Shop::buySellMenu() {
    
    while (true) {
    
    MenuRender::makeMenu((char**)BuySellMenuString,10,10,drawSurface);
    int choice=MenuController::returnChoice(kShopBuySellMenuLength,10,10,drawSurface,inputController);
    
    switch (choice) {
        
        case kGameplayBuy:
            this->buyMenu();
            break;
            
        case kGameplaySell:
            this->sellMenu();
            break;
        
        case kMenuChoiceExit:
            return;
            
    }
    
    }
    
}

void Shop::shopLoop() {
    
    this->buySellMenu();
    
}

void Shop::startShop(char *shopConfigPath, PlayerTeam *newPlayerTeam, SDL_Surface *newDrawSurface, InputController *newInputController, GameItemsDatabase *newItemsDatabase) {
    
    map<string,string>parsedShopConfig=ConfigParser::parseConfigFile(shopConfigPath);
    string rawGoods=parsedShopConfig["goods"];
    
    string newNotEnoughMoneyMessage=parsedShopConfig["notEnoughMoneyMessage"];
    string newDontBuyThatMessage=parsedShopConfig["dontBuyThatMessage"];
    string newWillBuyItForMessage=parsedShopConfig["willBuyItForMessage"];
    
    vector<string>parsedGoods=StringParser::splitTextToVectorBySeparator((char*)rawGoods.c_str(),(char*)",");
    int parsedGoodsCount=parsedGoods.size()/2;
    
    vector<ShopItem>newShopGoods;
    
    for (int a=0;a<parsedGoodsCount;a++) {
        
        string nowItemName=parsedGoods[a];
        string nowItemPrice=parsedGoods[a+1];
        
        GameItem *gameItem=newItemsDatabase->getGameItemWithName(nowItemName);
        
        if (gameItem==NULL) continue;
        
        ShopItem newItem;
        newItem.item=gameItem;
        newItem.price=atoi(nowItemPrice.c_str());
        
        newShopGoods.push_back(newItem);
        
    }
    
    Shop *shop=new Shop(newShopGoods,newDrawSurface,newPlayerTeam,newInputController,newItemsDatabase,newNotEnoughMoneyMessage,newDontBuyThatMessage,newWillBuyItForMessage);
    shop->shopLoop();
    delete shop;
    
    
}