/* 
 * File:   GraphicsSettings.cpp
 * Author: demensdeum
 * 
 * Created on December 17, 2011, 8:13 PM
 */

#include "GraphicsSettings.h"

const int kDefaultScreenWidth=640;
const int kDefaultScreenHeight=480;
const int kDefaultColorDepth=32; 

GraphicsSettings::GraphicsSettings() {
}

GraphicsSettings::GraphicsSettings(const GraphicsSettings& orig) {
}

GraphicsSettings::~GraphicsSettings() {
}

int  GraphicsSettings::getScreenWidth() {
    
    //TODO: own screen width and height
    return kDefaultScreenWidth;
    
}

int GraphicsSettings::getScreenHeight() {
 
    return kDefaultScreenHeight;
    
}

int GraphicsSettings::getColorDepth() {
    
    return kDefaultColorDepth;
    
}