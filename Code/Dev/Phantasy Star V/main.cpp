/* 
 * File:   main.cpp
 * Author: demensdeum
 *
 * Created on December 3, 2011, 12:50 AM
 */

#include <cstdlib>
#include "GameEngine.h"

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {


    
    //test render
    GameEngine *gameMaster=new GameEngine();
    //gameMaster->testMap();
    //gameMaster->simpleRenderTest();
    gameMaster->startGameEngine();
    
    return 0;
}

