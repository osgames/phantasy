/* 
 * File:   GameManager.h
 * Author: demensdeum
 *
 * Created on December 3, 2011, 1:01 AM
 * 
 * Game Manager controls game engine, save and load game
 * 
 */

#ifndef GAMEMANAGER_H
#define	GAMEMANAGER_H

class GameManager {
public:
    GameManager();
    GameManager(const GameManager& orig);
    virtual ~GameManager();

private:

};

#endif	/* GAMEMANAGER_H */

