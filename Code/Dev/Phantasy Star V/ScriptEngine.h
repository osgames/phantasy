/* 
 * File:   GameObjectsMaster.h
 * Author: User
 *
 * Created on May 26, 2012, 3:44 PM
 */

#ifndef GAMEOBJECTSMASTER_H
#define	GAMEOBJECTSMASTER_H

#include "GameObject.h"
#include <vector>
#include "Const.h"
#include "ScriptEngineAPI.h"

#include "GameEngine.h"

using namespace std;

class GameEngine;

class ScriptEngine {
public:
    ScriptEngine();
    ScriptEngine(const ScriptEngine& orig);
    virtual ~ScriptEngine();
    
    void manipulateObjects();
    
    static void compileString(char *inputString, char *outputScriptCode);
    static void decompileScript(char *inputScriptCode, char *outputHumanString);
    static vector<string> splitTextToVectorByWhitespace(char *inputMultilineText);
    static void runScriptForObject(const char *script, GameObject *nowObject, GameEngine *gameEngine);
    static void getFirstOperator(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor);
    static bool checkIf(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor);
    static bool ifTouchedBy(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor);
    static bool ifCalledBy(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor);
    static void performCommand(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor);
    
private:
    
};

#endif	/* GAMEOBJECTSMASTER_H */

