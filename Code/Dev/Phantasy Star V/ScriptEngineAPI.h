/* 
 * File:   ScriptEngineAPI.h
 * Author: User
 *
 * Created on May 26, 2012, 4:21 PM
 */

#ifndef SCRIPTENGINEAPI_H
#define	SCRIPTENGINEAPI_H

#ifdef	__cplusplus
extern "C" {
#endif

    enum {
        kSCRIPT_ENGINE_NULL,
        kSCRIPT_ENGINE_OPERATION_END,
        kSCRIPT_ENGINE_IF,
        kSCRIPT_ENGINE_TOUCHED_BY,
        kSCRIPT_ENGINE_THEN,
        kSCRIPT_CHANGE_MAP,
        kSCRIPT_SHOW_DIALOG,
        kSCRIPT_ENGINE_CALLED_BY,
        kSCRIPT_ENGINE_CHANGE_SCRIPT_INDEX,
        kSCRIPT_API_LENGTH
    };
    //everything seems ok if you use only alphabet and numbers ascii after number code 44 (,)
    extern const char *scriptEngineAPIhumanLanguage[];
    
#ifdef	__cplusplus
}
#endif

#endif	/* SCRIPTENGINEAPI_H */

