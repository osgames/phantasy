/* 
 * File:   ResourcesManager.h
 * Author: User
 *
 * Created on May 6, 2012, 1:01 AM
 */

#ifndef RESOURCESMANAGER_H
#define	RESOURCESMANAGER_H

#include "Const.h"
#include "GameObject.h"
#include "Tile.h"
#include "FileSystemManager.h"
#include "MenuRender.h"
#include <vector>
#include "MenuController.h"
#include "MenuRender.h"
#include "InputController.h"
#include "GameModuleSettings.h"
#include "MenuController.h"
#include "GameModulesManager.h"
#include "SDL/SDL_image.h"

class ResourcesManager {
public:
    ResourcesManager();
    ResourcesManager(const ResourcesManager& orig);
    virtual ~ResourcesManager();
    
    static int addTiles(SDL_Surface *drawSurface, InputController *inputController, vector<Tile*>&tiles);
    static int addObjects(SDL_Surface *drawSurface, InputController *inputController, vector<GameObject*>&gameObjects);    
    static string selectGameModule(SDL_Surface *drawSurface,InputController *inputController);
    static void selectTileForGameModulePath(string gameModuleChoicePath,SDL_Surface *drawSurface,InputController *inputController, vector<Tile*>&tiles);
    static void selectObjectForGameModulePath(string gameModuleChoicePath,SDL_Surface *drawSurface,InputController *inputController, vector<GameObject*>&gameObjects);
    static void loadTile(const char *tileDirectory, vector<Tile*>&tiles);
    static void loadTile(char *tileDirectory, vector<Tile*>&tiles);
    static void loadObject(char* objectDirectory, vector<GameObject*>&gameObjects);
    static SDL_Surface* loadBattleBackgroundWithName(string battleBackgroundName, string gameModuleName);
    static SDL_Surface* loadNoneBattleBackground();
    
private:


};

#endif	/* RESOURCESMANAGER_H */

