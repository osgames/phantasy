/* 
 * File:   MapEditor.cpp
 * Author: User
 * 
 * Created on April 16, 2012, 1:48 AM
 */

#include "MapEditor.h"
#include "debuglog.h"
#include "version.h"
#include "SDL/SDL.h"
#include "Const.h"
#include "SDL/SDL_image.h"
#include <vector>
#include "SDL/SDL_gfxPrimitives.h"
#include "SDLSurfaceSaverToPNG.h"

const char* hexToBin[]={
    
        "0000",
        "0001",
        "0010",
        "0011",
        "0100",
        "0101",
        "0110",
        "0111",
        "1000",
        "1001",
        "1010",
        "1011",
        "1100",
        "1101",
        "1110",
        "1111"
    
};

const char* wallToSolid[]= {
 
    
        "0000000001100000",
        "0110011001100110",
        "0000000011110000",
        "0000000000110010",
        "0110011001100000",
        "0110011001100110",
        "0110011001110000",
        "0110011001110110",
        "0110011011100000",
        "0000000011000110",
        "0000000011110000",
        "0110011011110110",
        "0110011011100000",
        "0110011011100110",
        "0000000011110000",
        "0110011011110110"

};

MapEditor::MapEditor() {
    gameModule="rebirth";
    currentLanguage="en";
    oldTime=0;
    renderMode=kSoftwareSingleThreadRender;
    isShowAdvancedInformation=true;
 
}

MapEditor::MapEditor(const MapEditor& orig) {
}

MapEditor::~MapEditor() {
    
    //free memory
    
    //TODO free memory for tiles vector
    
    selectType=kSelectTypeNone;
    selectLayer=kTileTypeUnknown;
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        if (renderLayers[a]!=NULL)
            delete renderLayers[a];
        
        if (mapLayers[a]!=NULL)
            delete mapLayers[a];
    }
    
    if (screenSurface!=NULL)
        SDL_FreeSurface(screenSurface);
    
    if (mainMenuPanelImage!=NULL)
        SDL_FreeSurface(mainMenuPanelImage);
    
    if (addRemoveExitPanelImage!=NULL)
        SDL_FreeSurface(addRemoveExitPanelImage);
    
    if (inputController!=NULL)
        delete inputController;
    

}

void MapEditor::checkRefreshMap() {
    
    if (!inputController->getRefreshMapButton()) return;
    if (currentMapPath.length()<1) return;
    this->openMapSystem((char*)currentMapPath.c_str());
    inputController->allButtonsToUnpress();
    debugLog("Map was refreshed\n");
    
}

SDL_Surface *MapEditor::getTileByMouse(int mouseX, int mouseY) {
    
    int tileX=mouseX/kTileSize;
    int tileY=(mouseY-kTileSize)/kTileSize;
    
    int tilePosition=tileX+tileY*(screenSurface->w/kTileSize);
    
    if (tilePosition<tiles.size())
        if (tiles[tilePosition]!=NULL) {
            
            debugLog("Selected tile:"<<tilePosition<<"\n");
            selectType=kSelectTypeTile;
            selectNumber=tilePosition;
            
            return tiles[tilePosition]->getImage();
        }
#ifdef DEBUG
        else {
            debugLog("Can't get tile:"<<tilePosition<<"\n");
        }
#endif
    
}

void MapEditor::removeTileset() {
    
    if (selectType!=kSelectTypeTile)
        return;
    
    if (selectNumber>=tiles.size())
        return;
    
    //remove from arrangement map
    for (int a=0;a<kArrangementMapCount;a++)
        for (int b=0;b<kDefaultMapSize*2;b++)
            for(int c=0;c<kDefaultMapSize*2;c++)
                if (arrangementMap[a][b][c]==selectNumber)
                    arrangementMap[a][b][c]=-1;
            
     //down index for tilesets higher that removed tileset
    for (int a=0;a<kArrangementMapCount;a++)
        for (int b=0;b<kDefaultMapSize*2;b++)
            for(int c=0;c<kDefaultMapSize*2;c++)
                if (arrangementMap[a][b][c]>selectNumber)
                    arrangementMap[a][b][c]--;
            
            
        for (int a=0;a<kDefaultMapSize;a++)
            for (int b=0;b<kDefaultMapSize;b++)
                if (wallsArrangementMap[a][b]==selectNumber)
                        wallsArrangementMap[a][b]=kArrangementMapEmptyCell;            
 
        for (int a=0;a<kDefaultMapSize;a++)
            for (int b=0;b<kDefaultMapSize;b++)
                if (wallsArrangementMap[a][b]>selectNumber)
                        wallsArrangementMap[a][b]--;               
            

            
    tiles.erase(tiles.begin()+selectNumber);
            
    this->fixTilesAllLayers();

    
}

void MapEditor::addRemoveTilesView() {
    
        SDL_FillRect(screenSurface, NULL, 0x00000000);  
        
        //render interface
        SDL_BlitSurface(addRemoveExitPanelImage,NULL,screenSurface,NULL);    
        
        //render available tiles
        //debugLog("draw tiles\n");
        
        int drawX=0;
        int drawY=kTileSize;
        
        for (int a=0;a<tiles.size();a++) {

            SDL_Surface *nowTile=tiles[a]->getImage();


            SDL_Rect source;
            source.x=kTileSize;
            source.y=kTileSize;
            source.w=kTileSize;
            source.h=kTileSize;            
            
            SDL_Rect destination;
            destination.x=drawX;
            destination.y=drawY;
            destination.w=kTileSize;
            destination.h=kTileSize;               
        
            SDL_BlitSurface(nowTile,&source,screenSurface,&destination);
            
            if (nowTile!=NULL)
                if (drawX>screenSurface->w) {
                        drawX=0;
                        drawY+=kTileSize;
                }
                else drawX+=kTileSize;            
            
        }
    
        
        SDL_Flip(screenSurface);
        
        inputController->pollEvents();    
    
        if (inputController->getLeftMouseClick()) {
            if (inputController->getMouseY()/kTileSize==0) {
                switch(inputController->getMouseX()/kTileSize){
                    
                    case kExitButton:
                        debugLog("switch to kMapEditorMapEditView\n");
                        viewMode=kMapEditorMapEditView;
                        break;
                        
                    case kAddButton:
                        debugLog("add tile view\n");
                        viewMode=kMapEditorAddTilesView;
                        break;
                    
                    case kRemoveButton:
                        removeTileset();
                        break;                        

                        
                }
            //TODO add all buttons
            }
            else {
                    
                getTileByMouse(inputController->getMouseX(),inputController->getMouseY());
                    
            }
            
            inputController->unpressMouse();
   
        }        
        
}

void MapEditor::fixTilesAllLayers() {

    //fix lower tiles
    //clear all layers tile
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++)
                for (int z=0;z<kMaxGraphicsLayers;z++)
                        mapLayers[z]->setTile(a,b,-1);

    
    for (int layerIterator=0;layerIterator<kArrangementMapCount;layerIterator++)
        for (int a=0;a<kDefaultMapSize;a++)
            for (int b=0;b<kDefaultMapSize;b++) {

            
                int upLeft=arrangementMap[layerIterator][b*2][a*2];
                int upRight=arrangementMap[layerIterator][b*2][a*2+1];
                int downLeft=arrangementMap[layerIterator][b*2+1][a*2];
                int downRight=arrangementMap[layerIterator][b*2+1][a*2+1];
           
                if (upLeft==-1 && upRight==-1 && downLeft==-1 && downRight==-1)
                    continue;
                
                int layers[4]={-1,-1,-1,-1};
                int layersCount=0;
                
                if (upLeft!=-1) { 
                    layers[layersCount]=upLeft;
                    layersCount++;
                }
                if (upRight!=-1) { 
                    layers[layersCount]=upRight;
                    layersCount++;
                }
                if (downLeft!=-1) { 
                    layers[layersCount]=downLeft;
                    layersCount++;
                }
                if (downRight!=-1) { 
                    layers[layersCount]=downRight;
                    layersCount++;
                }
                
                int tilesetsToArrange[4]={kArrangementMapEmptyCell,kArrangementMapEmptyCell,kArrangementMapEmptyCell,kArrangementMapEmptyCell};
                
                for (int a=0;a<4;a++) {
                    
                    int nowUniqueTileset=layers[a];
                    
                    for (int b=0;b<4;b++) {
                        
                        if (nowUniqueTileset==tilesetsToArrange[b])
                            break;
                        else if (b==3)
                            tilesetsToArrange[a]=nowUniqueTileset;
                        
                    }
                    
                }
                
                debugLog("Tilesets to arrange: "<<tilesetsToArrange[0]<<","<<tilesetsToArrange[1]<<","<<tilesetsToArrange[2]<<","<<tilesetsToArrange[3]<<"\n");
                
                //TODO sort by index
                
                for (int z=0;z<4;z++) {
                    
                    int renderLayer=layerIterator*4+z;
                    int nowTilesetIndex=tilesetsToArrange[z];
                    if (nowTilesetIndex==-1)
                        continue;
                    
                    int outputUpLeft=upLeft;
                    int outputUpRight=upRight;
                    int outputDownLeft=downLeft;
                    int outputDownRight=downRight;
                    
                    if (outputUpLeft==nowTilesetIndex) outputUpLeft=1; else outputUpLeft=0;
                    if (outputUpRight==nowTilesetIndex) outputUpRight=1; else outputUpRight=0;
                    if (outputDownLeft==nowTilesetIndex) outputDownLeft=1; else outputDownLeft=0;
                    if (outputDownRight==nowTilesetIndex) outputDownRight=1; else outputDownRight=0;   
                    
                    char binTile[5];
                    snprintf(binTile,5,"%d%d%d%d",outputUpLeft,outputUpRight,outputDownLeft,outputDownRight);
                    int outputTile=strtol(binTile,NULL,2);
                    outputTile=nowTilesetIndex*kDefaultTilesInTileset+outputTile;
                    mapLayers[renderLayer]->setTile(a,b,outputTile);
                    debugLog("outputTile set to: "<<outputTile<<"\n at layer:"<<renderLayer<<"\n");
                    
                }
            
        }

    //fix walls
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++) {
            
            int nowTilesetIndex=wallsArrangementMap[a][b];
            
            int leftTile=wallsArrangementMap[a][b-1];
            int rightTile=wallsArrangementMap[a][b+1];
            
            int downTile=wallsArrangementMap[a+1][b];
            int upTile=wallsArrangementMap[a-1][b];
            
            if (nowTilesetIndex==-1)
                continue;
            
            if (leftTile==nowTilesetIndex) leftTile=1; else leftTile=0;
            if (rightTile==nowTilesetIndex) rightTile=1; else rightTile=0;
            if (downTile==nowTilesetIndex) downTile=1; else downTile=0;
            if (upTile==nowTilesetIndex) upTile=1; else upTile=0;             
  
            //if (arrangementMap[kTileTypeRoad][a*2][b*2]==kInvisibleTile) addAdvancedWallsFlag=true;
            //if (arrangementMap[kTileTypeRoad][a*2+1][b*2]==kInvisibleTile) addAdvancedWallsFlag=true;
            
            char binTile[5];
            snprintf(binTile,5,"%d%d%d%d",leftTile,upTile,rightTile,downTile);
            int outputTile=strtol(binTile,NULL,2);

            //check if floor empty
            bool addAdvancedWallsFlag=false;
            
            
            
             if (arrangementMap[kTileTypeRoad][a*2+2][b*2]==kInvisibleTile && arrangementMap[kTileTypeRoad][a*2+2][b*2+1]==kInvisibleTile) {
                addAdvancedWallsFlag=true;
            }

            if (outputTile==3 || outputTile==7) {
                if (arrangementMap[kTileTypeRoad][a*2+2][b*2+2]==kInvisibleTile && arrangementMap[kTileTypeRoad][a*2+2][b*2+3]==kInvisibleTile) addAdvancedWallsFlag=true;
            }
            
            if (outputTile==9 || outputTile==13) {
                if (arrangementMap[kTileTypeRoad][a*2+2][b*2-2]==kInvisibleTile && arrangementMap[kTileTypeRoad][a*2+2][b*2-3]==kInvisibleTile) addAdvancedWallsFlag=true;
            }
            
            outputTile=nowTilesetIndex*kDefaultTilesInTileset+outputTile;
            if (addAdvancedWallsFlag) {
                outputTile|=268435456;
            }
            mapLayers[kWallsRenderLayer]->setTile(b,a,outputTile);            
            
            
        }
 
}

void MapEditor::putTileOnMapByMouse(int mouseX, int mouseY) {
    
    int tileX=(cameraX+mouseX)/kTileSize;
    int tileY=(cameraY+mouseY)/kTileSize;
    
    int miniTileX=(cameraX+mouseX)/(kTileSize/2);
    int miniTileY=(cameraY+mouseY)/(kTileSize/2); 
    
    int posTileX=(mouseX%kTileSize)/(kTileSize/2);
    int posTileY=(mouseY%kTileSize)/(kTileSize/2);
    
    debugLog("tileX:"<<tileX<<"\ntileY:"<<tileY<<"\nminiTileX:"<<miniTileX<<"\nminiTileY"<<miniTileY<<"\nposTileX"<<posTileX<<"\nposTileY:"<<posTileY<<"\n");

    int tileType=tiles[selectNumber]->getTileType();
    
    if (tileType==kTileTypeUnknown)
        return;
    
    selectLayer=tileType;
    
    if (tileType!=kTileTypeWall) {
    
    if (tileX>0 && tileY>0 && tileX<kDefaultMapSize && tileY<kDefaultMapSize) {

        arrangementMap[selectLayer][miniTileY][miniTileX]=selectNumber;
        
        if (posTileX==0 && posTileY==0) {

                arrangementMap[selectLayer][miniTileY-1][miniTileX]=selectNumber;    
                arrangementMap[selectLayer][miniTileY][miniTileX-1]=selectNumber;
                arrangementMap[selectLayer][miniTileY-1][miniTileX-1]=selectNumber;
                
        }

        if (posTileX==1 && posTileY==0) {

                arrangementMap[selectLayer][miniTileY][miniTileX+1]=selectNumber;    
                arrangementMap[selectLayer][miniTileY-1][miniTileX]=selectNumber;
                arrangementMap[selectLayer][miniTileY-1][miniTileX+1]=selectNumber;
                
        }
        
        if (posTileX==0 && posTileY==1) {

                arrangementMap[selectLayer][miniTileY][miniTileX-1]=selectNumber;    
                arrangementMap[selectLayer][miniTileY+1][miniTileX]=selectNumber;
                arrangementMap[selectLayer][miniTileY+1][miniTileX-1]=selectNumber;
                
        }        
        
        if (posTileX==1 && posTileY==1) {

                arrangementMap[selectLayer][miniTileY][miniTileX+1]=selectNumber;    
                arrangementMap[selectLayer][miniTileY+1][miniTileX]=selectNumber;
                arrangementMap[selectLayer][miniTileY+1][miniTileX+1]=selectNumber;
                
        }            
        
    }
    
    }
    else {
    
        wallsArrangementMap[tileY][tileX]=selectNumber;
        
    }
    
    
    
    fixTilesAllLayers();
    

    
}

void MapEditor::putGameObjectOnMapByMouse(int mouseX, int mouseY) {
    
    int newGameObjectPositionX=cameraX+mouseX;
    int newGameObjectPositionY=cameraY+mouseY;
    
    //TODO make copy constructor for game objects
    GameObject *newGameObject=gameObjects[selectNumber]->getCopy();
    
    //tile size based position by ctrl key
    if (inputController->getControlButton()) {
        newGameObjectPositionX=(newGameObjectPositionX/kTileSize)*kTileSize;
        newGameObjectPositionY=(newGameObjectPositionY/kTileSize)*kTileSize;
    }
        
    
    /*newGameObject->setImage(gameObjects[selectNumber]->getImageWithoutAnimation());
    newGameObject->setPosition(new GameObjectPosition(newGameObjectPositionX,newGameObjectPositionY));
    
    debugLog("put gameObject on X:"<<newGameObjectPositionX<<"\nput gameObject on Y:"<<newGameObjectPositionY<<"\n");
    
   //TODO get animation params by struct
    newGameObject->setAnimation(kTileSize,40,3,0);*/    
    newGameObject->setPosition(new GameObjectPosition(newGameObjectPositionX,newGameObjectPositionY));
    renderLayers[kObjectsRenderLayer]->loadObject(newGameObject);
    
}

void MapEditor::putSolidOnMapByMouse(int mouseX, int mouseY) {
    
    int tileX=(cameraX+mouseX)/(kTileSize/4);
    int tileY=(cameraY+mouseY)/(kTileSize/4);  
    
    solidMap[tileY][tileX]=selectNumber;
    //solidMap[tileY][tileX]=kSolid;
    //solidMap[tileY][tileX]++;//increment
    
    
    debugLog("put solid on X:"<<tileX<<"\nY:"<<tileY<<"\n");
    
}

void MapEditor::putSelectedTypeByMouse(int mouseX, int mouseY) {
    
    switch (selectType) {
        
        case kSelectTypeTile:
            
            putTileOnMapByMouse(mouseX,mouseY);
            break;
            
        case kSelectTypeSolid:
            
            debugLog("put solid on map\n");
            putSolidOnMapByMouse(mouseX,mouseY);
            break;
            
        case kSelectTypeParentGameObject:
            debugLog("put object on map\n");
            putGameObjectOnMapByMouse(mouseX,mouseY);
            inputController->unpressMouse();
            break;
        
    }
    
    if (viewMode==kMapEditorSelectionModeView) {
        
        selectObjects(cameraX+inputController->getMouseX(),cameraY+inputController->getMouseY());
        inputController->unpressMouse();
        
    }
    
}

GameObject* MapEditor::getChildObjectByParentName(char* parentName) {
    
    int gameObjectsCount=renderLayers[kObjectsRenderLayer]->getObjectsCount();
    for (int a=0;a<gameObjectsCount;a++) {
        
        GameObject *nowObject=renderLayers[kObjectsRenderLayer]->getGameObject(a);
        if (strncmp(nowObject->getParentName().c_str(),parentName,kMaxObjectNameLength)==1) {
            
            return nowObject;
            
        }
        
    }
    
    return NULL;
    
}

bool MapEditor::removeChildObjectByParentName(char* parentName) {
    
    int gameObjectsCount=renderLayers[kObjectsRenderLayer]->getObjectsCount();
    for (int a=0;a<gameObjectsCount;a++) {
        
        GameObject *nowObject=renderLayers[kObjectsRenderLayer]->getGameObject(a);
        if (strncmp(nowObject->getParentName().c_str(),parentName,kMaxObjectNameLength)==0) {
            
            renderLayers[kObjectsRenderLayer]->removeObjectAtIndex(a);
            return true;
            
        }
        
    }
    
    return false;
    
}

void MapEditor::removeParentObject() {
    
    if (selectType!=kSelectTypeParentGameObject)
        return;
    
    if (selectNumber>=gameObjects.size())
        return;
    
    GameObject *parentObjectToRemove=NULL;
    parentObjectToRemove=gameObjects[selectNumber];
    if (!parentObjectToRemove)
        return;
    
    const char *parentName=parentObjectToRemove->getParentName().c_str();
    
    //remove child objects from map
    while (this->removeChildObjectByParentName((char*)parentName));
    
    gameObjects.erase(gameObjects.begin()+selectNumber);
    
}

void MapEditor::eraseSolidMap() {
    
    debugLog("Erase solid map\n");
    
    for (int a=0;a<kDefaultMapSize*4;a++)
        for (int b=0;b<kDefaultMapSize*4;b++)
            solidMap[a][b]=kEmptySolid;
    
}

void MapEditor::autoSolidMap() {
    
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++) {
            
            //rewrite this
            
            if (wallsArrangementMap[a][b]!=kArrangementMapEmptyCell) {
                
            int nowTilesetIndex=wallsArrangementMap[a][b];
            
            int leftTile=wallsArrangementMap[a][b-1];
            int rightTile=wallsArrangementMap[a][b+1];
            
            int downTile=wallsArrangementMap[a+1][b];
            int upTile=wallsArrangementMap[a-1][b];
            
            if (nowTilesetIndex==-1)
                continue;
            
            if (leftTile==nowTilesetIndex) leftTile=1; else leftTile=0;
            if (rightTile==nowTilesetIndex) rightTile=1; else rightTile=0;
            if (downTile==nowTilesetIndex) downTile=1; else downTile=0;
            if (upTile==nowTilesetIndex) upTile=1; else upTile=0;             
  
            char binTile[5];
            snprintf(binTile,5,"%d%d%d%d",leftTile,upTile,rightTile,downTile);
            int outputTile=strtol(binTile,NULL,2);                
                
            int tileIndex=outputTile%kDefaultTilesInTileset;
                
                debugLog("tileIndex:"<<tileIndex<<"\n");
                char autoSolid[16];
                
                strncpy(autoSolid,wallToSolid[tileIndex],16);
                
                int solidX=b*4;
                int solidY=a*4;
                
                for (int z=0;z<16;z++) {
                    
                    if (autoSolid[z]=='1')
                        solidMap[solidY][solidX]=kSolid;    
                    
                    debugLog("solid:"<<autoSolid[z]<<"\n");
                    
                    solidX++;
                    
                    if (z%4==3) {
                        solidX-=4;
                        solidY++;
                    }
                    
                    //debugLog("solid:"<<z%4<<"\n");
                    
                }
                
                
                /*for (int a=0;a<16;a++) {
                    
                    if (autoSolid[a]=='1') {
                        solidMap[solidY][solidX]=kSolid;
                    }
                    
                    solidX++;
                    
                    if (a%4==0)
                        solidY+=1;
                    
                }*/
                
            }
            
        }
    
}

void MapEditor::saveDialogToFileFromRawBuffer(char* dialogInputBuffer, char* dialogName) {
    
    queue<DialogMessage>messagesToSave;
    vector<string>parsedDialogMessages;
    parsedDialogMessages=MapEditor::multilineTextToRawScripts(dialogInputBuffer);
    
    int parsedMessagesCount=0;
    
    FILE *outputFile=NULL;
    outputFile=fopen(dialogName,"w");
    
    if (!outputFile)
        return;
    
    for (int a=0;a<parsedDialogMessages.size();a++) {
        
        string nowMessage=parsedDialogMessages[a];
        
        size_t foundPosition=0;
        foundPosition=nowMessage.rfind(kMessageIconSeparator);
        
        if (foundPosition!=string::npos) {
                
            int messageEndBorder=foundPosition;
            int iconStartBorder=foundPosition+1;
            
            if (messageEndBorder>=nowMessage.length())
                continue;
            if (iconStartBorder>=nowMessage.length())
                continue;
            
            string messageToStruct=nowMessage.substr(0,messageEndBorder);
            string iconToStruct=nowMessage.substr(iconStartBorder,kShortStringLength);
            
            if (messageToStruct.length()==0)
                continue;
            if (iconToStruct.length()==0)
                continue;
            
            DialogMessage newDialogMessages;
            
            strncpy(newDialogMessages.message,messageToStruct.c_str(),kShortStringLength);
            strncpy(newDialogMessages.icon,iconToStruct.c_str(),kShortStringLength);
            
            messagesToSave.push(newDialogMessages);
            parsedMessagesCount++;
            
        }
        
        //save to file
        
        if (fwrite(&parsedMessagesCount,1,sizeof(int),outputFile)!=sizeof(int)) {
            
            debugLog("Error write dialog count\n");
            return;
            
        }
        
        for (int a=0;a<messagesToSave.size();a++) {
            
            DialogMessage nowMessage=messagesToSave.front();
            messagesToSave.pop();
            
            if (fwrite(&nowMessage,1,sizeof(nowMessage),outputFile)!=sizeof(nowMessage)) {
                
                debugLog("Error write dialog message\n");
                return;
                
            }
            
            
        }
        
        
    }
    
    fclose(outputFile);
}

void MapEditor::newDialogFile(char *inputBuffer) {
    
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);      
    
    //open editor
    char dialogInputBuffer[kMaxScriptLength];
    
    if (!inputBuffer) {
        dialogInputBuffer[0]=0;
    }
    else {
        memcpy(dialogInputBuffer,inputBuffer,kMaxScriptLength);
    }
    
    ugl::showTextEditorWithText(kDialogEditorLabel,dialogInputBuffer);
    
    char dialogName[kMaxStringLength];
    char directoryPath[kMaxStringLength];
    
    strncpy(dialogName,"Unnamed",kMaxStringLength);
    snprintf(directoryPath,kMaxStringLength,"%s/%s/%s/%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleDialogDirectory);
    
    
    if (ugl::showFileSaveDialog(kSaveDialogLabel,dialogName,directoryPath,kGameDialogExtension,kGameDialogExtensionDescription)) {

        
        debugLog("Set current directory to "<<currentDirectory<<"\n");
        chdir(currentDirectory);
        
        //set file extension
        ugl::setFileExtensionForPath((char*)&dialogName,kGameDialogExtension);    
        
        //remove if exist
        remove(dialogName);
        
        //save to file
        MapEditor::saveDialogToFileFromRawBuffer(dialogInputBuffer,dialogName);
        
    }
    
    
}

void MapEditor::parseDialogFileToMultiline(char* dialogFile, char* editorBuffer) {
    
    int messagesCount=0;
    
    string outputBufferString;
    
    FILE *file=NULL;
    
    file=fopen(dialogFile,"r");
    if (!file) return;
    
    //read count
    if (fread(&messagesCount,1,sizeof(int),file)!=sizeof(int)) return;
    
    for (int a=0;a<messagesCount;a++) {
        
        DialogMessage nowMessage;
        if (fread(&nowMessage,1,sizeof(DialogMessage),file)!=sizeof(DialogMessage)) return;
            
        outputBufferString.append(nowMessage.message);
        outputBufferString.append("|");
        outputBufferString.append(nowMessage.icon);
        outputBufferString.append("\n");
            
        
        
    }
    
    fclose(file);
    
    memcpy(editorBuffer,outputBufferString.c_str(),outputBufferString.length());
    
}

void MapEditor::openDialogFile() {
    
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);    
    
    char dialogName[kMaxStringLength];
    char directoryPath[kMaxStringLength];
    
    strncpy(dialogName,"Unnamed",kMaxStringLength);
    snprintf(directoryPath,kMaxStringLength,"%s/%s/%s/%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleDialogDirectory);    
    
    if (ugl::showFileOpenDialog(kOpenDialogFile,dialogName,directoryPath,kGameDialogExtension,kGameDialogExtensionDescription)) {
        
        debugLog("Set current directory to "<<currentDirectory<<"\n");
        chdir(currentDirectory);
        
        char editorBuffer[kMaxScriptLength];
        
        this->parseDialogFileToMultiline((char*)dialogName,(char*)&editorBuffer);
        this->newDialogFile((char*)&editorBuffer);
        
        
    }
    
}

void MapEditor::switchTile(int tileIndex, int replaceTo, int layerIndex) {
    
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++)
                    if (mapLayers[layerIndex]->getTile(a,b)==tileIndex)
                        mapLayers[layerIndex]->setTile(a,b,replaceTo);  
                
    
}

void MapEditor::itemsDatabaseEditor() {
    
    
    char itemsDatabasePath[kMaxStringLength];
    snprintf(itemsDatabasePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kItemsDatabaseFile);
    
    debugLog("Items database path:"<<itemsDatabasePath<<"\n");
    
    char itemsDatabaseStringBuffer[kMaxStringLength*10];
    
    //load db file
    FILE *itemsDBfile=NULL;
    
    itemsDBfile=fopen(itemsDatabasePath,"r");
    
    if (itemsDBfile) {
        
        string outputString;
        
        //structs parse to multiline
        uint gameItemsCount=0;
        
        fread(&gameItemsCount,sizeof(uint),1,itemsDBfile);
        
        for (int a=0;a<gameItemsCount;a++) {
            
            GameItemStruct nowGameItemStruct;
            fread(&nowGameItemStruct,sizeof(GameItemStruct),1,itemsDBfile);
            
            char newLine[kMaxStringLength];
            snprintf(newLine,kMaxStringLength,"%s,%s,%u,%u,%u,%u\n",nowGameItemStruct.name,nowGameItemStruct.humanReadableName,nowGameItemStruct.gameItemType,nowGameItemStruct.minimumEffect,nowGameItemStruct.maximumEffect,nowGameItemStruct.price);
            outputString.append(newLine);
            
        }
        
        strncpy(itemsDatabaseStringBuffer,outputString.c_str(),outputString.length());
        
        fclose(itemsDBfile);
        
    }
    
    itemsDBfile=fopen(itemsDatabasePath,"w");
    
    //open editor
    ugl::showTextEditorWithText(kItemsDatabaseEditorLabel,(char*)&itemsDatabaseStringBuffer);
    //parse to structs
    vector<GameItemStruct>gameItemsToSave=MapEditor::parseMultilineTextToGameItems((char*)&itemsDatabaseStringBuffer);
    
    {
        
        //save
        uint gameItemsCount=gameItemsToSave.size();
        fwrite(&gameItemsCount,sizeof(uint),1,itemsDBfile);
    
        for (int a=0;a<gameItemsCount;a++) {
                GameItemStruct nowGameItemStruct=gameItemsToSave[a];
                fwrite(&nowGameItemStruct,sizeof(GameItemStruct),1,itemsDBfile);
        }
    
    }
    
    fclose(itemsDBfile);
    
}

vector<GameItemStruct> MapEditor::parseMultilineTextToGameItems(char* multilineText) {
    
    vector<GameItemStruct>outputVector;
    
    vector<string>parsedMultilineText=StringParser::splitTextToVectorBySeparator(multilineText,"\n");
    
    for (int a=0;a<parsedMultilineText.size();a++) {
        
        //parse to game item
        string nowParsedString=parsedMultilineText[a];
        
        vector<string>nowParsedGameItemString=StringParser::splitTextToVectorBySeparator((char*)nowParsedString.c_str(),",");
        
        if (nowParsedGameItemString.size()!=6) {
            debugLog("Parsed game item parameters not equal 6:"<<nowParsedGameItemString.size()<<"\n");
            continue;
        }
        
        GameItemStruct outputGameItem;
        strncpy(outputGameItem.name,nowParsedGameItemString[0].c_str(),kGameItemNameLength);
        outputGameItem.name[kGameItemNameLength-1]=0;//terminate

        strncpy(outputGameItem.humanReadableName,nowParsedGameItemString[1].c_str(),kGameItemHumanReadableLength);
        outputGameItem.humanReadableName[kGameItemHumanReadableLength-1]=0;//terminate        
        
        GameItemType outputGameItemType=kGameItemTypeUnknown;
        
        //TODO parse game item type
        //if (nowParsedGameItemString[2]=="")
        
        outputGameItem.gameItemType=outputGameItemType;
        
        outputGameItem.maximumEffect=(uint)atoi(nowParsedGameItemString[3].c_str());
        outputGameItem.minimumEffect=(uint)atoi(nowParsedGameItemString[4].c_str());
        
        outputGameItem.price=(uint)atoi(nowParsedGameItemString[5].c_str());
        
        outputVector.push_back(outputGameItem);
        
    }
    
    return outputVector;
    
}

void MapEditor::masterConsole() {
    
    char consoleBuffer[kShortStringLength];
    memset(consoleBuffer,0,kShortStringLength);
    ugl::showTextEditorWithText((char*)kMasterConsoleLabel,(char*)&consoleBuffer);
    
#ifdef GEDIT_SUPPORT
    //remove \n symbol at end of line
    {
        int removeIndex=strlen(consoleBuffer);
        if (removeIndex<1) return;
        removeIndex--;
        consoleBuffer[removeIndex]=0;
    }
    
#endif
    
    //try to run method from console input
    if (strncmp(consoleBuffer,kEraseSolidMapCommand,kShortStringLength)==0) {
        this->eraseSolidMap();
    }
    else if (strncmp(consoleBuffer,kAutoSolidMap,kShortStringLength)==0) {
        this->autoSolidMap();
    }
    else if (strncmp(consoleBuffer,kNewDialogFile,kShortStringLength)==0) {
        this->newDialogFile(NULL);
    }
    else if (strncmp(consoleBuffer,kOpenDialogFile,kShortStringLength)==0) {
        this->openDialogFile();
    }
    else if (strncmp(consoleBuffer,kItemsDatabaseEditor,kShortStringLength)==0) {
        this->itemsDatabaseEditor();
    }
    else if (strlen(consoleBuffer)>strlen(kSwitchTileCommand)) {
        
        char commandBuffer[strlen(kSwitchTileCommand)];
        
        strncpy(commandBuffer,consoleBuffer,strlen(kSwitchTileCommand));
        
        if (strncmp(commandBuffer,kSwitchTileCommand,strlen(kSwitchTileCommand))==0) {
            
            //get tile index
            //get switch to index
            //switch
            
            //Warning not secure
           
            int tileIndex=0;
            int changeTo=0;
            int layerIndex=0;
            
            sscanf(consoleBuffer, "%*s %*s %d %d %d", &tileIndex, &changeTo, &layerIndex);
            
            debugLog("Tile index:"<<tileIndex<<"\nChangeTo:"<<changeTo<<"\nLayer index:"<<layerIndex<<"\n");

            this->switchTile(tileIndex,changeTo,layerIndex);
            
        }
        
    }
    
}

void MapEditor::getScreenshotCheck() {
    
    if (inputController->getScreenshotButton()) {
        SDLSurfaceSaverToPNG::saveSurface(screenSurface);
        inputController->allButtonsToUnpress();
    }
    
}

void MapEditor::renderSolidMap() {
    
    int drawSolidX=0;
    int drawSolidY=0;
    
    int solidSize=kTileSize/4;

    int solidX=cameraX/solidSize;
    int solidY=cameraY/solidSize;    
    
    while (true) {
        
        int nowSolid=kEmptySolid;
        
        if (solidX<0 || solidY<0 || solidX>kDefaultMapSize*4 || solidY>kDefaultMapSize*4)
            nowSolid=kEmptySolid;
        else
            nowSolid=solidMap[solidY][solidX];
        
        if (nowSolid!=kEmptySolid) {
            
            //cool alpha blended rect
            
            boxRGBA(screenSurface,
		drawSolidX,
		drawSolidY,
		drawSolidX+solidSize,
		drawSolidY+solidSize,
		nowSolid,
		0,
		120,
		128
                );             
            
               
        }
        

        
        drawSolidX+=solidSize;
        solidX++;
        if (drawSolidX>screenSurface->w) {
            
            drawSolidX=0;
            drawSolidY+=solidSize;
            
            solidX=cameraX/solidSize;
            solidY++;
            
        }
        if (drawSolidY>screenSurface->h)
            break;
        
        
        
    }
    
}

void MapEditor::openMapSystem(char *filePath) {
    
     MapMaster::loadMap(filePath,tiles,gameObjects,renderLayers,mapLayers,arrangementMap,wallsArrangementMap,solidMap,scripts,mapProperties);   
     currentMapPath=filePath;
     for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderLayers[a]->setLoopMap(false);
     }
    
    if (!mapProperties["loopMap"].empty()) {
        if (mapProperties["loopMap"]=="yes") {
                for (int a=0;a<kMaxGraphicsLayers;a++) {
                        renderLayers[a]->setLoopMap(true);
                }            
        }
    }     
     
}

void MapEditor::saveMapSystem(char *filePath) {

    MapMaster::saveMap(filePath,tiles,gameObjects,renderLayers,mapLayers,arrangementMap,wallsArrangementMap,solidMap,scripts);
    
}

void MapEditor::loadGameObject(GameObject::GameObjectStruct gameObjectStruct) {
    
    

    
}

GameObject* MapEditor::getParentObjectByName(char *parentName) {
    
    
    
}

void MapEditor::saveMap() {
 
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);    
    
    debugLog(currentDirectory<<"\n");
    
    char mapName[kMaxStringLength];
    char directoryPath[kMaxStringLength];
    
    strncpy(mapName,"Unnamed",kMaxStringLength);
    snprintf(directoryPath,kMaxStringLength,"%s/%s/%s/%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleMapsDirectory);
    
    
    if(ugl::showFileSaveDialog((char*)kSaveMapLabel,(char*)&mapName,(char*)directoryPath,kGameMapExtension,kGameMapExtensionDescription)) {
        
        debugLog("Set current directory to "<<currentDirectory<<"\n");
        chdir(currentDirectory);
        
        //set file extension
        ugl::setFileExtensionForPath((char*)&mapName,kGameMapExtension);    
        
        //remove if exist
        remove(mapName);
        //save map
        this->saveMapSystem(mapName);
        

        
    }
    
    debugLog("Set current directory to "<<currentDirectory<<"\n");    
    chdir(currentDirectory);
                
}

void MapEditor::openMap() {
    
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);    
    
    debugLog(currentDirectory<<"\n");
    
    char mapName[kMaxStringLength];
    char directoryPath[kMaxStringLength];
    
    strncpy(mapName,"Unnamed",kMaxStringLength);
    snprintf(directoryPath,kMaxStringLength,"%s/%s/%s/%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleMapsDirectory);
    
    debugLog("directory path:"<<directoryPath<<"\n");
    
    if(ugl::showFileOpenDialog("Open Map",(char*)&mapName,(char*)directoryPath,kGameMapExtension,kGameMapExtensionDescription)) {

        debugLog("Set current directory to "<<currentDirectory<<"\n");
        chdir(currentDirectory);
        
        
        //save map
        this->openMapSystem(mapName);
        
    }
    
    
}

void MapEditor::eraseSolidsUnderMouse(int mouseX, int mouseY) {
    
    int tileX=(cameraX+mouseX)/(kTileSize/4);
    int tileY=(cameraY+mouseY)/(kTileSize/4);  
    
    solidMap[tileY][tileX]=kEmptySolid;

    
}

void MapEditor::eraseTilesUnderMouse(int mouseX, int mouseY) {
    
    int tileX=(cameraX+mouseX)/kTileSize;
    int tileY=(cameraY+mouseY)/kTileSize;
    
    int miniTileX=(cameraX+mouseX)/(kTileSize/2);
    int miniTileY=(cameraY+mouseY)/(kTileSize/2); 

    int posTileX=(mouseX%kTileSize)/(kTileSize/2);
    int posTileY=(mouseY%kTileSize)/(kTileSize/2);    

        
        arrangementMap[selectLayer][miniTileY][miniTileX]=kArrangementMapEmptyCell;
        
        if (posTileX==0 && posTileY==0) {

                arrangementMap[selectLayer][miniTileY-1][miniTileX]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY][miniTileX-1]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY-1][miniTileX-1]=kArrangementMapEmptyCell;
                
        }

        if (posTileX==1 && posTileY==0) {

                arrangementMap[selectLayer][miniTileY][miniTileX+1]=kArrangementMapEmptyCell;    
                arrangementMap[selectLayer][miniTileY-1][miniTileX]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY-1][miniTileX+1]=kArrangementMapEmptyCell;
                
        }
        
        if (posTileX==0 && posTileY==1) {

                arrangementMap[selectLayer][miniTileY][miniTileX-1]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY+1][miniTileX]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY+1][miniTileX-1]=kArrangementMapEmptyCell;
                
        }        
        
        if (posTileX==1 && posTileY==1) {

                arrangementMap[selectLayer][miniTileY][miniTileX+1]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY+1][miniTileX]=kArrangementMapEmptyCell;
                arrangementMap[selectLayer][miniTileY+1][miniTileX+1]=kArrangementMapEmptyCell;
                
        }            
        
        {
            //wall arrangement map
            wallsArrangementMap[tileY][tileX]=kArrangementMapEmptyCell;
        }
        
    
    fixTilesAllLayers();
    
}

void MapEditor::selectObjects(int selectOnX, int selectOnY) {
    
    //TODO mass objects
    
    RenderLayer *objectsLayer=renderLayers[kObjectsRenderLayer];
    
        for (int a=0;a<objectsLayer->getObjectsCount();a++) {
                
                GameObject *nowObject=NULL;
                nowObject=objectsLayer->getGameObject(a);
                if (!nowObject)
                        continue;
                
                        if (nowObject->hitTest(selectOnX,selectOnY)) {
                    
                        debugLog("object selected:"<<a<<"\n");
                        
                        //TODO double click
                        if (selectType==kSelectTypeSceneGameObject && selectNumber==a)
                            editObjectProperties();
                            
                        
                        selectType=kSelectTypeSceneGameObject;
                        selectNumber=a;
                    
                        }
                    
                
                }
    
}

void MapEditor::renderObjectsSelection() {
    
        if (selectType==kSelectTypeSceneGameObject) {
            
            GameObject *selectedObject=NULL;
            RenderLayer *objectsLayer=renderLayers[kObjectsRenderLayer];
            
            if (selectNumber>objectsLayer->getObjectsCount())
                return;
            
            selectedObject=objectsLayer->getGameObject(selectNumber);
            
            if (!selectedObject)
                return;
            
            int selectionX=selectedObject->getX()-(selectedObject->getWidth()/2);
            int selectionY=selectedObject->getY()-selectedObject->getHeight();
            
            selectionX-=cameraX;
            selectionY-=cameraY;
            
            int selectionXend=selectionX+selectedObject->getWidth();
            int selectionYend=selectionY+selectedObject->getHeight();
            
            //draw selection rectangle
            //rectangleRGBA(screenSurface,selectionX,selectionY,selectionXend,selectionYend,0,255,0,100);*/
            
            int borderSize=1;
            
            SDL_Rect lineRect;
            lineRect.x=selectionX;
            lineRect.y=selectionY;
            
            //horizontal lines
            lineRect.w=selectedObject->getWidth();
            lineRect.h=borderSize;
            
            SDL_FillRect(screenSurface,&lineRect,0x0000FF00);
            
            lineRect.y=selectionYend;
            SDL_FillRect(screenSurface,&lineRect,0x0000FF00);
            
            //vertical lines
            lineRect.x=selectionX;
            lineRect.y=selectionY;

            lineRect.w=borderSize;
            lineRect.h=selectedObject->getHeight();
            
            SDL_FillRect(screenSurface,&lineRect,0x0000FF00);
            
            lineRect.x=selectionXend;
            SDL_FillRect(screenSurface,&lineRect,0x0000FF00);

            
        }
    
}

void MapEditor::deleteSelectedSceneObject() {
    
    RenderLayer *objectsLayer=renderLayers[kObjectsRenderLayer];
    
    if (selectType!=kSelectTypeSceneGameObject)
        return;
    
    if (selectNumber>objectsLayer->getObjectsCount())
        return;
    
    objectsLayer->removeObjectAtIndex(selectNumber);
    
    selectType=kSelectTypeNone;
    
    
}

vector<string> MapEditor::multilineTextToRawScripts(char *inputMultilineText) {

    string multilineString=inputMultilineText;
    
    int leftPosition=0;
    int rightPosition=0;
    
    vector<string>outputRawScripts;
    
    while (rightPosition!=string::npos) {
        
        rightPosition=multilineString.find("\n",leftPosition);
        string newScript=multilineString.substr(leftPosition,rightPosition-leftPosition);
        debugLog("newScript:"<<newScript<<"\n");
        leftPosition=rightPosition+1;
        
        if (newScript.length()>0)
                outputRawScripts.push_back(newScript);
        
    }
    
    return outputRawScripts;
    
}

void MapEditor::decompileScriptsToMultilineText(char *outputMultilineText) {
    
    string multilineString;
    
    for (int a=0;a<scripts.size();a++) {
        
        char decompiledScriptBuffer[kMaxScriptLength];
        ScriptEngine::decompileScript((char*)scripts[a].c_str(),decompiledScriptBuffer);
        string decompiledString=decompiledScriptBuffer;
        
        debugLog("scriptToText:"<<decompiledString<<"\n");
        multilineString.append(decompiledString);
        if (a!=scripts.size()-1)
                multilineString.append("\n");
    }
    
    strncpy(outputMultilineText,multilineString.c_str(),kMaxScriptsBuffer);
    
}

void MapEditor::scriptsEditor() {
    
    //TODO script editor with syntax check and compiler
    //and edit scripts by selecting from list
    char multilineText[kMaxScriptsBuffer];
    this->decompileScriptsToMultilineText((char*)&multilineText);
    ugl::showTextEditorWithText(kScriptEditorLabel,(char*)&multilineText);
    vector<string>rawScripts=this->multilineTextToRawScripts((char*)&multilineText);
    scripts.clear();
    for (int a=0;a<rawScripts.size();a++) {
        
        char outputScriptBuffer[kMaxScriptLength];
        memset(outputScriptBuffer,0,kMaxScriptLength);
        string nowRawScript=rawScripts[a];
        ScriptEngine::compileString((char*)nowRawScript.c_str(),outputScriptBuffer);
        string outputString=outputScriptBuffer;
        scripts.push_back(outputString);
    }
    
}

void MapEditor::editObjectProperties() {
    
    //TODO full properties
    //now only script index
    RenderLayer *objectsLayer=renderLayers[kObjectsRenderLayer];
    if (selectNumber>=objectsLayer->getObjectsCount())
        return;
    
    GameObject *nowObject=objectsLayer->getGameObject(selectNumber);
    
    char scriptIndexBuffer[kShortStringLength];
    snprintf(scriptIndexBuffer,kShortStringLength,"%d",nowObject->getScriptIndex());
    ugl::showTextEditorWithText((char*)kObjectPropertiesLabel,(char*)&scriptIndexBuffer);
    
    int newScriptIndex=atoi(scriptIndexBuffer);
    nowObject->setScriptIndex(newScriptIndex);
    
}

void MapEditor::changePropertiesForSolid(int mouseX, int mouseY) {
    
    int tileX=(cameraX+mouseX)/(kTileSize/4);
    int tileY=(cameraY+mouseY)/(kTileSize/4);  
    
    debugLog("put solid on X:"<<tileX<<"\nY:"<<tileY<<"\n");    
    
    char solidIndexBuffer[10];
    snprintf(solidIndexBuffer,10,"%d",solidMap[tileY][tileX]);
    ugl::showTextEditorWithText((char*)"Tile Script Index",(char*)&solidIndexBuffer);
    solidMap[tileY][tileX]=atoi(solidIndexBuffer);
    
}

void MapEditor::singleThreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    backgroundRender->render(screenSurface);
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
     
        SDL_Thread *nowThread=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h,0,0);
        SDL_WaitThread(nowThread, NULL);
    }
    
}

void MapEditor::multithreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    //experimental multithread render
    //but still slow    
    //sprites sometime don't shows
    
    backgroundRender->render(screenSurface);

    SDL_Thread *renderThreads[4];
    
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderThreads[0]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w/2, drawSurface->h/2,0,0);
        renderThreads[1]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h/2,drawSurface->w/2,0);
        renderThreads[2]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w/2, drawSurface->h,0,drawSurface->h/2);
        renderThreads[3]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h,drawSurface->w/2,drawSurface->h/2);

        
        for (int a=0;a<4;a++) {
            
            SDL_WaitThread(renderThreads[a], NULL);
            
        }       
    
    }
}


void MapEditor::renderScene(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    switch (renderMode) {
        
        case kSoftwareSingleThreadRender:
            this->singleThreadSoftwareRender(fromCameraX,fromCameraY,drawSurface);
            break;
            
        case kSoftwareMultithreadedRender:
            this->multithreadSoftwareRender(fromCameraX,fromCameraY,drawSurface);
            break;
    
    }
    
}

void MapEditor::showAdvancedInformation(int cameraX, int cameraY, int mouseX, int mouseY, SDL_Surface *screenSurface) {
    
        
        if (font==NULL)
        debugLog("Can't load font ./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf\nMaybe SDL_TTF not inited\n");
        
        SDL_Color fColor;
        fColor.r = fColor.g = fColor.b = 255;     
    
        {
                //camera X, camera Y
                char advancedInformation[256];
                snprintf(advancedInformation,256,"camera X: %d camera Y: %d",cameraX,cameraY);
        
                SDL_Surface *drawText=TTF_RenderUTF8_Blended(font, advancedInformation, fColor);
        
                SDL_Rect destinationRect;
                destinationRect.w=drawText->w;
                destinationRect.h=drawText->h;
                destinationRect.x=screenSurface->w-drawText->w;
                destinationRect.y=screenSurface->h-drawText->h;
        
                destinationRect.x-=20;
                destinationRect.y-=10;
        
                SDL_BlitSurface(drawText,NULL,screenSurface,&destinationRect);
    
                SDL_FreeSurface(drawText);
        }
        
        {
                //mouse X, mouse Y
                char advancedInformation[256];
                snprintf(advancedInformation,256,"mouse X: %d mouse Y: %d",cameraX+mouseX,cameraY+mouseY);
        
                SDL_Surface *drawText=TTF_RenderUTF8_Blended(font, advancedInformation, fColor);
        
                SDL_Rect destinationRect;
                destinationRect.w=drawText->w;
                destinationRect.h=drawText->h;
                destinationRect.x=screenSurface->w-drawText->w;
                destinationRect.y=screenSurface->h-drawText->h;
        
                destinationRect.x-=20;
                destinationRect.y-=30;
        
                SDL_BlitSurface(drawText,NULL,screenSurface,&destinationRect);
    
                SDL_FreeSurface(drawText);
        }        
        
        
}

void MapEditor::mapEditView() {
    
        //render background
        backgroundRender->render(screenSurface);
        //render layers
        renderScene(cameraX,cameraY,screenSurface);
        
        //if solid edit mode
        if (viewMode==kMapEditorSolidMapEditView || viewMode==kMapEditorSelectionModeView)
            renderSolidMap();
        
        //render selection
        if (viewMode==kMapEditorSelectionModeView)
            renderObjectsSelection();
        
        //show advanced information
        if (isShowAdvancedInformation)
            showAdvancedInformation(cameraX,cameraY,inputController->getMouseX(),inputController->getMouseY(),screenSurface);
        
        //render interface
        SDL_BlitSurface(mainMenuPanelImage,NULL,screenSurface,NULL);
        
        
        
        SDL_Flip(screenSurface);
        
        inputController->pollEvents();
        
        if (inputController->getLeftMouseClick()) {
            if (inputController->getMouseY()/kTileSize==0){
                switch (inputController->getMouseX()/kTileSize){
                    
                    case kAddRemoveTilesButton:
                    
                        debugLog("switch to addRemoveTiles\n");
                        viewMode=kMapEditorAddRemoveTilesView;
                        inputController->unpressMouse();
                        break;
                    
                    case kSolidMapButton:
                    
                        debugLog("switch to solidEditMode\n");
                        viewMode=kMapEditorSolidMapEditView;
                        selectType=kSelectTypeSolid;
                        inputController->unpressMouse();
                        break;

                    case kObjectsAddRemoveButton:
                    
                        inputController->unpressMouse();
                        viewMode=kMapEditorAddRemoveObjectsView;
                        break;
                    
                    case kSelectionModeButton:
                        
                        inputController->unpressMouse();
                        viewMode=kMapEditorSelectionModeView;
                        selectType=kSelectTypeNone;
                        break;
                    
                    case kScriptsEditor:
                        this->scriptsEditor();
                        break;
                        
                    case kOpenMapButton:
                    
                        inputController->unpressMouse();
                        this->openMap();
                        break;
                
                    case kSaveMapButton:
                    
                        inputController->unpressMouse();
                        debugLog("Save map\n");
                        this->saveMap();
                    
                } 
                
            }
            else {
                
                putSelectedTypeByMouse(inputController->getMouseX(),inputController->getMouseY());
                
            }
            
            


            
        }
        else if (inputController->getMasterConsoleButton()) {
            inputController->allButtonsToUnpress();
            this->masterConsole();
        }
        else if (inputController->getRightMouseClick()) {
            if (selectType==kSelectTypeTile)
                eraseTilesUnderMouse(inputController->getMouseX(),inputController->getMouseY());
            else if (selectType==kSelectTypeSolid) {
                eraseSolidsUnderMouse(inputController->getMouseX(),inputController->getMouseY());
            }
            else if (viewMode==kMapEditorSelectionModeView) {
                changePropertiesForSolid(inputController->getMouseX(),inputController->getMouseY());
            }
        }
        else if (inputController->getDeleteButton()) {
            deleteSelectedSceneObject();
        }
        else if (inputController->getMouseScrollUp()) {
            if (selectType==kSelectTypeSolid) {
                selectNumber++;
            }
            inputController->allButtonsToUnpress();
        }
        else if (inputController->getMouseScrollDown()) {
            if (selectType==kSelectTypeSolid) {
                selectNumber--;
            }
            inputController->allButtonsToUnpress();
        }
        else {
            
            
            //navigation
            switch (inputController->getLastCommand()) {
                    
                    case kCommandGoLeft:
                        cameraX-=kTileSize;
                        break;

                    case kCommandGoRight:
                        cameraX+=kTileSize;
                        break;
                        
                    case kCommandGoDown:
                        cameraY+=kTileSize;
                        break;
                        
                    case kCommandGoUp:
                        cameraY-=kTileSize;
                        break;
                        
            }
                        
                        
                        
                        
        }
    
    
}

void MapEditor::getGameObjectByMouse(int mouseX, int mouseY) {
    
    int gameObjectX=mouseX/kDefaultIconSize;
    int gameObjectY=(mouseY-kDefaultIconSize)/kDefaultIconSize;
    
    int gameObjectPosition=gameObjectX+gameObjectY*(screenSurface->w/kDefaultIconSize);
    
  
    
    if (gameObjectPosition<gameObjects.size())
        if (gameObjects[gameObjectPosition]!=NULL) {
            
            debugLog("Selected object:"<<gameObjectPosition<<"\n");
            selectType=kSelectTypeParentGameObject;
            selectNumber=gameObjectPosition;
           
        }
#ifdef DEBUG
        else {
            debugLog("Can't get object:"<<gameObjectPosition<<"\n");
        }
#endif    
    
}

void MapEditor::addRemoveObjectsView() {
    
        SDL_FillRect(screenSurface, NULL, 0x00000000);  
    
        //render interface
        SDL_BlitSurface(addRemoveExitPanelImage,NULL,screenSurface,NULL);    
        
        //render available objects
        
        int drawX=0;
        int drawY=kTileSize;
        
        for (int a=0;a<gameObjects.size();a++) {
            
            GameObject *nowGameObject=gameObjects[a];
            SDL_Surface *nowIcon=nowGameObject->getIcon();
            
            SDL_Rect destination;
            destination.x=drawX;
            destination.y=drawY;
            destination.w=NULL;
            destination.h=NULL;               
        
            SDL_BlitSurface(nowIcon,NULL,screenSurface,&destination);
            
            //TODO right NULL check with error or NULL-safe icon image
            
            if (nowIcon!=NULL)
                if (drawX>screenSurface->w) {
                        drawX=0;
                        drawY+=kTileSize;
                }
                else drawX+=kTileSize;            
            
        }
    
        
        SDL_Flip(screenSurface);
        
        inputController->pollEvents();    
    
        
        
        if (inputController->getLeftMouseClick()) {
            if (inputController->getMouseY()/kTileSize==0) {
                switch (inputController->getMouseX()/kTileSize) {
                    case kExitButton:
                        debugLog("switch to kMapEditorMapEditView\n");
                        viewMode=kMapEditorMapEditView;
                        break;
                              
                    case kAddButton:
                        debugLog("switch to kMapEditorMapEditView\n");
                        viewMode=kMapEditorAddObjectsView;
                        break;
                        
                    case kRemoveButton:
                        this->removeParentObject();
                        break;
                        
                }
            //TODO add all buttons
            }
            else {
                    
                getGameObjectByMouse(inputController->getMouseX(),inputController->getMouseY());
                    
            }
            
            inputController->unpressMouse();
   
        }            
    
}

void MapEditor::systemOperations() {
    
    this->windowResizeCheck();
    this->getScreenshotCheck();
    this->checkRefreshMap();
}

void MapEditor::windowResizeCheck() {
    
    if (inputController->getWindowResize())
        screenSurface=SDL_SetVideoMode(inputController->getWindowWidth(), inputController->getWindowHeight(), 32, SDL_SWSURFACE|SDL_RESIZABLE);
    
}

void MapEditor::applicationQuitCheck() {
    
    if (inputController->getApplicationQuit())
        run=false;
    
}

void MapEditor::addTilesView() {
    
    
    if (ResourcesManager::addTiles(screenSurface,inputController,tiles)==kResultExit)
        viewMode=kMapEditorAddRemoveTilesView;
    
    
}

void MapEditor::addObjectsView() {

    if (ResourcesManager::addObjects(screenSurface,inputController,gameObjects)==kResultExit)
        viewMode=kMapEditorAddRemoveObjectsView;
    
    
}

void MapEditor::mapEditorLoop() {
    
    
    while(run) {

    Uint32 currentTime=SDL_GetTicks();
    if (currentTime<oldTime+kFrameDelay) continue;
    oldTime=currentTime;        
        
        switch (viewMode) {
            
            case kMapEditorMapEditView:
                
                this->mapEditView();
                break;
                
            case kMapEditorSolidMapEditView:

                this->mapEditView();
                break;                
                
            case kMapEditorAddRemoveTilesView:
                
                this->addRemoveTilesView();
                break;
                
            case kMapEditorSelectionModeView:
                this->mapEditView();
                break;
                
            case kMapEditorAddRemoveObjectsView:
                
                this->addRemoveObjectsView();
                break;
            
            case kMapEditorAddTilesView:
                
                this->addTilesView();
                break;
                
            case kMapEditorAddObjectsView:
                
                this->addObjectsView();
                break;
                
        }
        
        this->systemOperations();
        this->applicationQuitCheck();
        
    }     

}

void MapEditor::startMapEditor() {
    
    debugLog("Map editor started\n");
    
    TTF_Init();
    
    inputController=new InputController();
    
    viewMode=kMapEditorMapEditView;
    
    run=true;
    
    cameraX=0;
    cameraY=0;
    
    char mapEditorTitle[100];
    sprintf(mapEditorTitle,"Phantasy Star Rebirth Map Editor v%s",kGameVersion);
    
    //initialize game classes
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderLayers[a]=new RenderLayer();
        mapLayers[a]=new MapMaster();
        
        mapLayers[a]->newBlankInvisibleMap();
        
        renderLayers[a]->setMap(mapLayers[a]->getMap());
        renderLayers[a]->setTiles(&tiles);
    }

    
    char mainMenuPanelImagePath[256];
    char addRemoveExitImagePath[256];
    
    sprintf(mainMenuPanelImagePath,"%s/%s/resources/graphics/gui/mainMenu.png",kGameModulesPath,kGameModuleSystemModulePath);
    sprintf(addRemoveExitImagePath,"%s/%s/resources/graphics/gui/exitAddRemove.png",kGameModulesPath,kGameModuleSystemModulePath);
    
 
    for (int layerIterator=0;layerIterator<kArrangementMapCount;layerIterator++)
        for (int a=0;a<kDefaultMapSize*2;a++)
                for (int b=0;b<kDefaultMapSize*2;b++)
                    arrangementMap[layerIterator][a][b]=kArrangementMapEmptyCell;
        

        
        //rewrite this
        for (int a=0;a<kDefaultMapSize*2;a++)
            for (int b=0;b<kDefaultMapSize;b++)
                solidMap[a][b]=kEmptySolid;
        
    
        for (int a=0;a<kDefaultMapSize;a++)
            for (int b=0;b<kDefaultMapSize;b++)
                wallsArrangementMap[a][b]=kArrangementMapEmptyCell;
        
    mainMenuPanelImage=IMG_Load(mainMenuPanelImagePath);
    if (mainMenuPanelImage==NULL)
        debugLog("Main menu image "<<mainMenuPanelImagePath<<" load error\n");
    
    addRemoveExitPanelImage=IMG_Load(addRemoveExitImagePath);
    if (addRemoveExitPanelImage==NULL)
        debugLog("Add remove image "<<addRemoveExitImagePath<<" load error\n");
    
    backgroundRender=new BackgroundRender();
    
    Uint32 flags=SDL_SWSURFACE|SDL_RESIZABLE;
    
    SDL_Init(SDL_INIT_VIDEO);
    font = TTF_OpenFont("./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf",18);       
    SDL_WM_SetCaption(mapEditorTitle,NULL);
    screenSurface=SDL_SetVideoMode(640, 480, 32, flags);
    
    /*ResourcesManager::loadTile((char*)"./gamemodules/phantasystarV/resources/graphics/tileset/grasspavement",tiles);
    ResourcesManager::loadObject("./gamemodules/phantasystarV/resources/graphics/objects/chaz",gameObjects);*/
    
    this->mapEditorLoop();  
    
    SDL_Quit();
    
}
