/* 
 * File:   GameSkillsDatabase.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 7:14 PM
 */

#ifndef GAMESKILLSDATABASE_H
#define	GAMESKILLSDATABASE_H

#include "GameDatabase.h"
#include "GameSkill.h"

class GameSkillsDatabase : public GameDatabase {
public:
    GameSkillsDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory);
    GameSkillsDatabase(const GameSkillsDatabase& orig);
    virtual ~GameSkillsDatabase();
    
    GameSkill* getGameSkillWithName(string nowItemName);
    vector<GameSkill*> returnGameSkillsFromRawString(string itemsListRawString);       
    
private:

    vector<GameSkill*>gameSkills;
    void addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary);
};

#endif	/* GAMESKILLSDATABASE_H */

