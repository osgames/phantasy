/* 
 * File:   GameMaster.h
 * Author: demensdeum
 *
 * Created on December 4, 2011, 7:18 PM
 */






#ifndef GAMEMASTER_H
#define	GAMEMASTER_H


#include "GameItemsDatabase.h"
#include "GameMagicDatabase.h"
#include "GameTechniqueDatabase.h"
#include "GameSkillsDatabase.h"

#include "TilesArray.h"
#include "TilesRender.h"
#include "InputController.h"
#include "ObjectsMaster.h"

#include <queue>
#include "SDL/SDL_ttf.h"
#include "GraphicsSettings.h"
#include "ObjectsRender.h"
#include "GraphicEffects.h"
#include "GameModuleSettings.h"
#include "RenderLayer.h"
#include "InputController.h"
#include "GameObject.h"
#include "MapMaster.h"
#include "MenuRender.h"
#include "DialogRender.h"
#include "MenuController.h"
#include "GameConst.h"
#include "version.h"
#include "ScriptEngine.h"
#include "ConfigParser.h"
#include "PlayerTeam.h"

#include "BackgroundRender.h"
#include "GameObjectCommands.h"
#include "Const.h"

#include "GameEngineCommand.h"
#include "savepng.h"


#include "SDLSurfaceSaverToPNG.h"

#include <SDL/SDL_image.h>

#ifdef OPENGL_SUPPORT
#include <SDL/SDL_opengl.h>
#endif

#include "GameMenu.h"
#include "Shop.h"

class GameEngine {
public:
    GameEngine();
    GameEngine(const GameEngine& orig);
    virtual ~GameEngine();

    void startGameEngine();
    void touchSolid(int solidIndex, GameObject *touchedBy);
    void callSolid(int solidIndex, GameObject *touchedBy);    
    void changeMapFromScriptLocation(const char *locationFromScript);
    vector<string> parseScriptArguments(char *rawArguments);
    void addGameEngineCommandToQueue(int command,string argument);
    void loadAndShowDialog(const char* path);
    
private:
    
    GameObject *playerObject;
    bool adaptKeyEvents(InputController *inputController);
    int getCameraXFromObject(GameObject *gameObject);
    int getCameraYFromObject(GameObject *gameObject);
    
    void changeMap(char *mapName);
    //void putPlayerObjectOnRenderLayer(int newPlayerPositionX, int newPlayerPositionY);
    void putPlayerTeamOnRenderLayer(int newPlayerTeamLeaderPositionX,int newPlayerTeamLeaderPositionY);
    void gameLoop();
    void gotoMapAtPosition(char *mapFile, int newPlayerPositionX, int newPlayerPositionY);
    void renderScene(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    void windowResizeCheck();
    void runCommandsFromCommandQueue();

    void makeMegaTexture();
    void loadMapForOpengl();
    
    void showGameplayMenu();
    void loadGameItemsDatabase();

    void multithreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    void singleThreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    void openGLRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    void getScreenshotCheck();
    void systemOperations();
    void loadGameItemWithName(string gameItemName);
    
    //TODO savegame loadgame
    
    string enemiesSet;
    int battleModeChance;
    
    char playerProfile[256];
    char gameModule[256];
  
    ObjectsMaster* objectsMaster;
    
    MapMaster *mapLayers[kMaxGraphicsLayers];
    RenderLayer *renderLayers[kMaxGraphicsLayers];
    BackgroundRender *backgroundRender;
    
    PlayerTeam *playerTeam;
    
    RenderMode renderMode;
    
    int arrangementMap[kArrangementMapCount][kDefaultMapSize*2][kDefaultMapSize*2];
    int wallsArrangementMap[kDefaultMapSize][kDefaultMapSize];
    int solidMap[kDefaultMapSize*4][kDefaultMapSize*4];
    
    SDL_Surface *screenSurface;
    
    vector<Tile*> tiles;
    vector<GameObject*> gameObjects;
    vector<string> scripts;    
    map<string,string>mapProperties;
    
    string battleModeBackgroundString;
    
    //int cameraX;
    //int cameraY;
    
    InputController *inputController;
    
    queue<GameEngineCommand*>gameEngineCommandQueue;
    
    Uint32 oldTime;
    
    string currentLanguage;
    
    GameItemsDatabase *gameItemsDatabase;
    GameMagicDatabase *gameMagicDatabase;
    GameTechniqueDatabase *gameTechniqueDatabase;
    GameSkillsDatabase *gameSkillsDatabase;
    
#ifdef OPENGL_SUPPORT
    GLuint megaTexture;//mega because holds tiles and objects
    GLfloat *vertices;//vertex array
    GLfloat *tex;//tex coords array
    GLsizei tilesCount;//count
    
    GLfloat GLcameraXspeed;
    GLfloat GLcameraYspeed;
#endif
};

#endif	/* GAMEMASTER_H */

