#include "ugl.h"

void ugl::setFileExtensionForPath(char *filePath, char *fileExtension) {
    
    string rawFilePathString=filePath;
    
    char extensionWithDot[kMaxStringLength];
    snprintf(extensionWithDot,kMaxStringLength,".%s",fileExtension);    
    
    size_t foundPos=0;
    foundPos=rawFilePathString.rfind(extensionWithDot);
    
    if (foundPos!=string::npos) {
        //TODO check string length
        if (foundPos==rawFilePathString.length()-strlen(extensionWithDot))
            return;
    }
    
    snprintf(filePath,kMaxStringLength,"%s%s",rawFilePathString.c_str(),extensionWithDot);
    
    debugLog("File path:"<<filePath<<"\n");
    
    
}
