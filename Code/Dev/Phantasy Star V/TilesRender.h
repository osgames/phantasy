/* 
 * File:   TilesRender.h
 * Author: demensdeum
 *
 * Created on December 3, 2011, 1:03 AM
 */

#include "Tile.h"
#include "GameObjectPosition.h"
#include "TilesArray.h"

#ifndef TILESRENDER_H
#define	TILESRENDER_H

class TilesRender {
public:
    TilesRender();
    TilesRender(const TilesRender& orig);
    virtual ~TilesRender();
    void renderTilesFromPositionToSurface(GameObjectPosition *position,TilesArray *tilesArray,SDL_Surface *drawSurface);
    void drawTile(Tile *nowTile,int drawX,int drawY,SDL_Surface *drawSurface);
private:

};

#endif	/* TILESRENDER_H */

