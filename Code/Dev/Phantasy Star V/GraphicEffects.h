/* 
 * File:   GraphicEffects.h
 * Author: demensdeum
 *
 * Created on December 17, 2011, 10:30 PM
 */

#include <SDL/SDL.h>
#include <SDL/SDL_rotozoom.h>
#include <SDL/SDL_gfxPrimitives.h>

#ifndef GRAPHICEFFECTS_H
#define	GRAPHICEFFECTS_H

class GraphicEffects {
public:
    GraphicEffects();
    GraphicEffects(const GraphicEffects& orig);
    virtual ~GraphicEffects();
    
    static void fadeIn(SDL_Surface *drawSurface, void *callback);
    void fadeOut(SDL_Surface *drawSurface, void (*callback));
    
    void rotateZoomAndFadeOut(SDL_Surface *drawSurface, void *callback);
    
private:

};

#endif	/* GRAPHICEFFECTS_H */

