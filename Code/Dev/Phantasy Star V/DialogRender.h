/* 
 * File:   DialogRender.h
 * Author: User
 *
 * Created on March 26, 2012, 10:51 PM
 */

#include "SDL/SDL.h"

#ifndef DIALOGRENDER_H
#define	DIALOGRENDER_H

class DialogRender {
public:
    DialogRender();
    DialogRender(const DialogRender& orig);
    virtual ~DialogRender();
    
    static char* renderDialogBox(int drawX, int drawY, char *dialogText, SDL_Surface *drawSurface);  
    void renderDialogBox(int drawX, int drawY, char *dialogText);
    void setDrawSurface(SDL_Surface *newDrawSurface);
    
private:

    SDL_Surface *drawSurface;
    
};

#endif	/* DIALOGRENDER_H */

