/* 
 * File:   BattleMode.cpp
 * Author: User
 * 
 * Created on July 6, 2012, 11:31 PM
 */

#include "BattleMode.h"
#include "ResourcesManager.h"

BattleMode::BattleMode(vector<Fighter>allFighters, SDL_Surface *newDrawSurface, InputController *newInputController,string newGameModule,GameItemsDatabase *newGameItemsDatabase, GameMagicDatabase *newGameMagicDatabase, GameTechniqueDatabase *newGameTechniqueDatabase, SDL_Surface *newBattleModeBackgroundImage) {
    
    fighters=allFighters;
    gameModule=newGameModule;
    drawSurface=newDrawSurface;
    inputController=newInputController;
    gameItemsDatabase=newGameItemsDatabase;
    gameMagicDatabase=newGameMagicDatabase;
    gameTechniqueDatabase=newGameTechniqueDatabase;
    battleModeBackgroundImage=newBattleModeBackgroundImage;

}

BattleMode::BattleMode(const BattleMode& orig) {
}

BattleMode::~BattleMode() {
}

BattleModeAction BattleMode::getPlayerActionForFighter(Fighter *nowFighter, int nowFighterIndex) {
    
    //TODO implement
    BattleModeAction outputAction=GameplayMenuMaster::playerBattleModeMenuForFighter(drawSurface,inputController,fighters,nowFighterIndex);
    return outputAction;
}

BattleModeAction BattleMode::getEnemyActionForFighter(Fighter *nowFighter, int nowFighterIndex) {
    
    //TODO AI for enemy anyway
    vector<Fighter*>playerTeam;
    for (int a=0;a<fighters.size();a++) {
        
        Fighter *nowFighter=&fighters[a];
        if (nowFighter->getBattleModeSide()==kPlayerTeam && nowFighter->isAlive()) playerTeam.push_back(nowFighter);
        
    }
    
    //select random fighter
    //while (true) {
    
        int targetIndex=RandomGenerator::randomInteger(0,playerTeam.size());
        
        //if (targetIndex==nowFighterIndex) continue;
        
        Fighter *targetFighter=playerTeam[targetIndex];
        
        //check if player
        
        //if (targetFighter->getBattleModeSide()==kPlayerTeam) {
            
            //if alive
            
            //if (!targetFighter->isAlive()) continue;
            
            //attack him
            BattleModeAction outputAction;
            outputAction.master=nowFighter;
            outputAction.target=targetFighter;
            outputAction.action=kActionAttack;
            
            return outputAction;
            
        //}
    
    //}
    
    
}

BattleModeAction BattleMode::getNeutralActionForFighter(Fighter *nowFighter, int nowFighterIndex) {
    
    //TODO implement
    
}

BattleModeAction BattleMode::getActionForFighter(Fighter *nowFighter, int nowFighterIndex) {
    
    BattleModeSide whichSide=nowFighter->getBattleModeSide();
    
    BattleModeAction outputAction;
    
    if (whichSide==kPlayerTeam) {
        //implement player command get
        outputAction=this->getPlayerActionForFighter(nowFighter,nowFighterIndex);
    }
    else if (whichSide==kEnemyTeam) {
        //implement enemy command get
        outputAction=this->getEnemyActionForFighter(nowFighter,nowFighterIndex);
    }
    else if (whichSide==kNeutral) {
        //implement neutral command get
        outputAction=this->getNeutralActionForFighter(nowFighter,nowFighterIndex);
    }
    
    return outputAction;
    
}

void BattleMode::showBattleModeLose() {
    
    
    
}

void BattleMode::showBattleModeVictory() {
    
    
    
}

void BattleMode::playAttackAction(BattleModeAction nowAction) {
    
    debugLog(((Fighter*)nowAction.master)->getName()<<" attacks "<<((Fighter*)nowAction.target)->getName()<<"\n");
    
    Fighter *master=(Fighter*)nowAction.master;
    Fighter *target=(Fighter*)nowAction.target;
    
    int attackPower=RandomGenerator::randomInteger(master->getMinAttackPower(),master->getMaxAttackPower());
    target->damage(attackPower);
    
    
}

void BattleMode::playAction(BattleModeAction nowAction) {
    
    debugLog("Play Battle Mode action\n");
    
    switch (nowAction.action) {
        
        case kActionAttack:
            this->playAttackAction(nowAction);
            break;
        
    }
    
}

void BattleMode::drawBattleModeBackground() {
    
    SDL_BlitSurface(battleModeBackgroundImage,NULL,drawSurface,NULL);
    
}

void BattleMode::showFightersStatus() {
    
    for (int a=0;a<fighters.size();a++) {
        
        Fighter nowFighter=fighters[a];
        GameplayMenuMaster::showFighterStatusWindow(&nowFighter,drawSurface,10+a*200,200+100*(nowFighter.getBattleModeSide()==kPlayerTeam));
        
    }
    

    
}

BattleModeResult BattleMode::battleModeLoop() {
    
    BattleModeResult result=kRunaway;
    
    while (true) {
    
        this->drawBattleModeBackground();
        this->showFightersStatus();
        SDL_Flip(drawSurface);
        
        vector<BattleModeAction>actions;
        
        //check all player team alive

        int playerTeamAlive=0;            
        
        for (int a=0;a<fighters.size();a++) {
            
            Fighter nowFighter=fighters[a];
            
            debugLog("Now fighter side:"<<nowFighter.getBattleModeSide()<<"\n");
            
            if (nowFighter.getBattleModeSide()==kPlayerTeam)
                if (nowFighter.isAlive())
                    playerTeamAlive++;
            
        }

        if (playerTeamAlive==0) {
            
            this->showBattleModeLose();
            result=kLose;
            return result;
            
        }
        
        //check all enemies alive
        
        int enemiesAlive=0;
        
         for (int a=0;a<fighters.size();a++) {
            
            Fighter nowFighter=fighters[a];
            if (nowFighter.getBattleModeSide()==kEnemyTeam)
                if (nowFighter.isAlive())
                    enemiesAlive++;
            
        }

        if (enemiesAlive==0) {
            
            this->showBattleModeVictory();
            result=kVictory;
            return result;
            
        }       
        
            
        //set commands
        for (int a=0;a<fighters.size();a++) {
        
                Fighter nowFighter=fighters[a];
                BattleModeAction newAction=this->getActionForFighter(&nowFighter,a);
                if (newAction.action==kActionRunaway) {
                    //TODO
                    //implement for enemies
                    //implement runaway chance
                    
                    if (RandomGenerator::randomInteger(0,4)==2) return result;
                    
                }
                actions.push_back(newAction);
        
        }
    
        //play actions
        for (int a=0;a<actions.size();a++) {
            
            BattleModeAction nowAction=actions[a];
            this->playAction(nowAction);
            
        }
        
        
        debugLog("Battle mode iteration\n");
    }
}

vector<Fighter> BattleMode::loadFightersFromEnemiesSet(string enemiesSet, string gameModulePath, GameItemsDatabase *gameItemsDatabase,GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase) {
    
    vector<Fighter>outputFighters;
    
    char enemiesSetFilePath[kMaxStringLength];
    snprintf(enemiesSetFilePath,kShortStringLength,"%s/%s/%s/%s.%s",kGameModulesPath,gameModulePath.c_str(),kEnemiesSetsDirectory,enemiesSet.c_str(),kEnemiesSetFileExtension);
    
    map<string,string>parsedEnemiesSetConfig=ConfigParser::parseConfigFile(enemiesSetFilePath);
    string set=parsedEnemiesSetConfig["set"];
    vector<string>parsedEnemiesSet=StringParser::splitTextToVectorBySeparator((char*)set.c_str(),",");
    
    vector<int>selectedEnemies;
    
    //enemies count
    int enemiesCount=RandomGenerator::randomInteger(1,kMaximumEnemies);
    
    for (int a=0;a<enemiesCount;a++) {
        
        int selectedEnemy=RandomGenerator::randomInteger(0,parsedEnemiesSet.size()-1);
        selectedEnemies.push_back(selectedEnemy);
        
    }
    
    for (int a=0;a<selectedEnemies.size();a++) {
        
        string nowEnemy=parsedEnemiesSet[selectedEnemies[a]];
        char enemyConfigPath[kMaxStringLength];
        snprintf(enemyConfigPath,kMaxStringLength,"%s/%s/%s/%s.%s",kGameModulesPath,gameModulePath.c_str(),kEnemiesDirectory,nowEnemy.c_str(),kEnemyConfigExtension);
        
        debugLog(enemyConfigPath<<"\n");
           
        Hero *newEnemyHero=Hero::loadHeroFromFilepath(enemyConfigPath,(char*)gameModulePath.c_str(),gameItemsDatabase,gameMagicDatabase,gameTechniqueDatabase,gameSkillsDatabase);
        if (!newEnemyHero) continue;
        Fighter newFighter=Fighter::fighterFromHero(newEnemyHero);
        delete newEnemyHero;
        
        newFighter.setBattleModeSide(kEnemyTeam);
        
        outputFighters.push_back(newFighter);
        
    }
    
    return outputFighters;
    
}

BattleModeResult BattleMode::startBattleMode(PlayerTeam *playerTeam, string enemiesSet, SDL_Surface *newDrawSurface, InputController *newInputController, string newGameModule, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase, string newBattleModeBackgroundString) {
    
    
    vector<Fighter>playerTeamFighters=playerTeam->getFighters();
    vector<Fighter>enemiesTeamFighters=BattleMode::loadFightersFromEnemiesSet(enemiesSet,newGameModule, gameItemsDatabase, gameMagicDatabase, gameTechniqueDatabase, gameSkillsDatabase);
    SDL_Surface *battleModeBackgroundImage=ResourcesManager::loadBattleBackgroundWithName(newBattleModeBackgroundString,newGameModule);
    
    debugLog("playerTeam team:"<<playerTeamFighters[0].getBattleModeSide()<<"\n");
    
    vector<Fighter>allFighters;
    
    for (int a=0;a<playerTeamFighters.size();a++) allFighters.push_back(playerTeamFighters[a]);
    for (int a=0;a<enemiesTeamFighters.size();a++) allFighters.push_back(enemiesTeamFighters[a]);
    
    
    BattleMode *newBattleMode=new BattleMode(allFighters,newDrawSurface,newInputController,newGameModule, gameItemsDatabase, gameMagicDatabase, gameTechniqueDatabase, battleModeBackgroundImage);
    BattleModeResult result=newBattleMode->battleModeLoop();
    delete newBattleMode;
    return result;
    
}
    