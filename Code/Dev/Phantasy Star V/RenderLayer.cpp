/* 
 * File:   SimpleRender.cpp
 * Author: User
 * 
 * Created on March 8, 2012, 10:54 PM
 * 
 * One layer simple tile and objects render for Phantasy Star V prototype
 * 
 */

#include "RenderLayer.h"
#include "debuglog.h"

RenderLayer::RenderLayer() {

    renderThread=NULL;
    tiles=NULL;
    loopMap=false;
    
}

void RenderLayer::setLoopMap(bool newLoopMap) {
    loopMap=newLoopMap;
}

GameObject *RenderLayer::getGameObject(int objectIndex) {
    
    return objects[objectIndex];
    
}

int RenderLayer::getObjectsCount() {
    
    return objects.size();
    
}

void RenderLayer::removeAllObjects() {
    
    for (int a=0;a<objects.size();a++) {
        if (!objects[a]->isPlayerTeamMember()) {
                debugLog("Remove object by name:"<<objects[a]->getName()<<"\n");            
                delete objects[a];
        }
    }
    
    while(!objects.empty())
        objects.pop_back();
    
}

RenderLayer::RenderLayer(const RenderLayer& orig) {
}

RenderLayer::~RenderLayer() {
    
    if (renderThread) {
        SDL_WaitThread(renderThread, NULL);
        SDL_KillThread(renderThread);
    }
    
}

void RenderLayer::loadObject(GameObject *newObject) {
    
    /*objects[objectsCount]=newObject;
    objectsCount++;*/
    
    objects.push_back(newObject);
    
}

void RenderLayer::removeObjectAtIndex(int objectIndex) {
    
    objects.erase(objects.begin()+objectIndex);
    
}

void RenderLayer::setTiles(vector <Tile*> *newTiles) {
    
    tiles=newTiles;
    
}

void RenderLayer::setMap(int **newMap) {
    
    map=newMap;
    
}

void RenderLayer::removeTilesetFromRenderLayer(int tilesetIndex) {
    
    for (int a=0;a<kDefaultMapSize;a++)
        for (int b=0;b<kDefaultMapSize;b++)
            if (map[a][b]/kDefaultTilesInTileset==tilesetIndex)
                map[a][b]=-1;
    
}

void RenderLayer::loadTile(Tile *newTile) {
    
    tiles->push_back(newTile);
    
}

void RenderLayer::drawObject(GameObject *nowObject,SDL_Surface *drawSurface,int cameraX,int cameraY,vector<GameObject*>&objects) {

    int surfaceWidth=drawSurface->w;
    int surfaceHeight=drawSurface->h;        
    
    SDL_Surface *objectImage=nowObject->getImage();
    
    GameObjectPosition *objectPosition=nowObject->getPosition();
    
    int objectX=objectPosition->getX();
    int objectY=objectPosition->getY();
    
    int drawX=objectX-cameraX;
    int drawY=objectY-cameraY;
    
    //check if out of screen
    if (drawX<1)
        return;
    if (drawY<1)
        return;
    if (drawX>surfaceWidth)
        return;
    if (drawY>surfaceHeight)
        return;
    
    if (objectImage==NULL)
        debugLog("Object image is NULL\n");
    
    drawX=drawX-objectImage->w/2;
    drawY=drawY-objectImage->h;
    
    SDL_Rect source;
    source.x=0;
    source.y=0;
    source.w=objectImage->w;
    source.h=objectImage->h;
    
    SDL_Rect destination;
    destination.x=drawX;
    destination.y=drawY;
    destination.w=source.w;
    destination.h=source.h;    
    
    SDL_BlitSurface(objectImage,&source,drawSurface,&destination);
    
}

void RenderLayer::drawTile(int index,int drawX,int drawY,SDL_Surface *drawSurface, vector<Tile*>*tiles) {
    
    //draw tile
    //debugLog("draw tile\n");

    bool drawAdvancedWalls=false;
    if ((index & 268435456)!=0) {
        drawAdvancedWalls=true;
        index &= ~268435456;
    }
    
    
    int tileIndex=index/kDefaultTilesInTileset;
    int tilePart=index%kDefaultTilesInTileset;
    
    
    //debugLog("Draw index"<<index<<"\nTile index:"<<tileIndex<<"\nTilePart:"<<tilePart<<"\n");
    
    int tileSourceX=tilePart%4;
    int tileSourceY=tilePart/4;
    

    if (tiles->size()<1) return;
    
    SDL_Surface *tileToDraw=tiles[0][tileIndex]->getImage();
    
    if (tileToDraw==NULL)
        debugLog("Wrong tile to draw\n");
    
    SDL_Rect source;
    source.x=tileSourceX*kTileSize;
    source.y=tileSourceY*kTileSize;
    if (drawAdvancedWalls) {
        if (tileToDraw->h==512)
            source.y+=256;
    }
    source.w=kTileSize;
    source.h=kTileSize;
    
    SDL_Rect destination;
    destination.x=drawX;
    destination.y=drawY;
    destination.w=source.w;
    destination.h=source.h;
    
    SDL_BlitSurface(tileToDraw,&source,drawSurface,&destination);
    
}

int RenderLayer::renderInThread(void *data) {
    
    RenderParameters *renderParameters=(RenderParameters*)data;
    
    int cameraX=renderParameters->cameraX;
    int cameraY=renderParameters->cameraY;
    SDL_Surface *drawSurface=(SDL_Surface*)renderParameters->drawSurface;
    int maximumX=renderParameters->maximumX;
    int maximumY=renderParameters->maximumY;
    
    int destinationX=renderParameters->destinationX;
    int destinationY=renderParameters->destinationY;
    
    vector<GameObject*>*objects=(vector<GameObject*>*)renderParameters->objects;
    vector<Tile*>*tiles =(vector<Tile*>*)renderParameters->tiles;
    int **map=renderParameters->map;
    bool loopMap=renderParameters->loopMap;
    
    /*debugLog("objects size:"<<objects.size()<<"\n");
    debugLog("tiles size:"<<tiles->size()<<"\n");
    
    
    debugLog("cameraX:"<<cameraX<<"\n");
    debugLog("cameraY:"<<cameraY<<"\n");
    
    debugLog("maximumX:"<<maximumX<<"\n");
    debugLog("maximumY:"<<maximumY<<"\n");*/
    
    
    
    RenderLayer::renderSystem(cameraX,cameraY,drawSurface,maximumX,maximumY,map,tiles,*objects,destinationX,destinationY,loopMap);
    
    free(data);
    
    return 0;
    
}

SDL_Thread* RenderLayer::render(int cameraX, int cameraY, SDL_Surface* drawSurface, int maximumX, int maximumY, int destinationX, int destinationY) {
    
    //create data for thread
    RenderParameters *renderParameters=(RenderParameters*)malloc(sizeof(RenderParameters));
    
    renderParameters->cameraX=cameraX;
    renderParameters->cameraY=cameraY;
    renderParameters->drawSurface=drawSurface;
    renderParameters->maximumX=maximumX;
    renderParameters->maximumY=maximumY;
    renderParameters->objects=&objects;
    renderParameters->tiles=tiles;
    renderParameters->map=map;
    renderParameters->destinationX=destinationX;
    renderParameters->destinationY=destinationY;
    renderParameters->loopMap=this->loopMap;
    
    //create thread
    return renderThread=SDL_CreateThread(RenderLayer::renderInThread, renderParameters);
    
    //RenderLayer::renderSystem(cameraX,cameraY,drawSurface,maximumX,maximumY,map,tiles,objects);
    //RenderLayer::renderSystem(cameraX,cameraY,drawSurface,maximumX,maximumY,map,tiles,objects);
    //RenderLayer::renderInThread(&renderParameters);
    
}

    int RenderLayer::getTileIndexAt(int mapX, int mapY) {
        
        return map[mapY][mapX];
        
    }

void RenderLayer::renderSystem(int cameraX, int cameraY, SDL_Surface *drawSurface, int maximumX, int maximumY, int **map, vector<Tile*> *tiles, vector<GameObject*>&objects, int destinationX, int destinationY, bool loopMap) {
    
    int surfaceWidth=drawSurface->w;
    int surfaceHeight=drawSurface->h;    
    
    int maximalMapSize=kDefaultMapSize-1;
    
    cameraX+=destinationX;
    cameraY+=destinationY;
    
    int firstTileX=cameraX/kTileSize;
    int firstDrawX=-cameraX%kTileSize;
    firstDrawX+=destinationX;

    int drawX=firstDrawX;
    int drawY=-cameraY%kTileSize;
    
    drawY+=destinationY;
    
    //drawX+=destinationX;
    //drawY+=destinationY;
    
    int tileX=firstTileX;
    int tileY=cameraY/kTileSize;
    
    //debugLog("Render start point: "<<tileX);
    
    //render map
    
    int drawedTiles=0;
    int nowTileID=kInvisibleTile;
    
    while (true) {
        
        if (drawX==firstDrawX) {
        
        for (int a=0;a<objects.size();a++) {
        
                GameObject *nowObject=objects[a];
        
                int nowObjectY=nowObject->getY();
                
                if (nowObjectY<cameraY+drawY) continue;
                if (nowObjectY>cameraY+drawY+kTileSize/2) continue;        
        
                RenderLayer::drawObject(nowObject,drawSurface,cameraX,cameraY,objects);
        
        }        
        
        }        
        
        //debugLog("drawX:"<<drawX<<"\ndrawY:"<<drawY<<"\n");
        
        if (loopMap) {
            
            int newTileX=tileX;
            int newTileY=tileY;

            
            if (newTileX<0) {
                newTileX=maximalMapSize-(abs(newTileX)%kDefaultMapSize);
            }
            else if (newTileX>maximalMapSize) {
                newTileX=newTileX%kDefaultMapSize;               
            }
            
            if (newTileY<0) {
                newTileY=maximalMapSize-(abs(newTileY)%kDefaultMapSize);
            }
            else if (newTileY>maximalMapSize) {
                newTileY=newTileY%kDefaultMapSize;             
            }            
            
            nowTileID=map[newTileY][newTileX];
            
        }
        else {
            if (tileX<0 || tileY<0 || tileX>=kDefaultMapSize || tileY>=kDefaultMapSize)
                nowTileID=kInvisibleTile;
            else
                nowTileID=map[tileY][tileX];
        }
        
        //invisible check
        if (nowTileID!=kInvisibleTile) {
            //debugLog("Tile draw\n");
            RenderLayer::drawTile(nowTileID,drawX,drawY,drawSurface,tiles);
        }
        

        
        drawedTiles++;
        
        if (drawX>surfaceWidth || drawX>maximumX) {
            
            if (drawY>surfaceHeight || drawY>maximumY) {
            
                //stop drawing
                //debugLog("stop draw\n");
                break;
                
            }
            else {
                
            for (int a=0;a<objects.size();a++) {

                    GameObject *nowObject=objects[a];

                    int nowObjectY=nowObject->getY();

                    if (nowObjectY<cameraY+drawY+kTileSize/2) continue;
                    if (nowObjectY>cameraY+drawY+kTileSize) continue;        

                    RenderLayer::drawObject(nowObject,drawSurface,cameraX,cameraY,objects);

            }                 
                
                drawX=firstDrawX;
                drawY+=kTileSize;
                
                tileY++;
                tileX=firstTileX;
                //debugLog("tileX:"<<tileX<<"\ntileY:"<<tileY<<"\n");
                
            }
            
        }
        else {
            
            drawX+=kTileSize;
            tileX++;
            
        }


        
    }
 

    

    

    
}
