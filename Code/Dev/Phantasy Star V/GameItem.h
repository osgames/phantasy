/* 
 * File:   GameItem.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:49 PM
 */

#ifndef GAMEITEM_H
#define	GAMEITEM_H

#include "GameDatabaseItem.h"

class GameItem : public GameDatabaseItem {
public:
    GameItem();
    GameItem(const GameItem& orig);
    virtual ~GameItem();
    
    //get
    int getPrice();
    int getItemType();
    int getEquipmentPosition();
    bool getEquipment();
    
    //set
    void setPrice(int newPrice);    
    void setItemType(int newItemType);
    void setEquipmentPosition(int newEquipPosition);
    void setEquipment(bool newEquipment);
    
private:

    int price;
    int itemType;
    int equipmentPosition;
    bool equipment;
    

    
};

#endif	/* GAMEITEM_H */

