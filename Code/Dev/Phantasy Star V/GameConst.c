#include "GameConst.h"
#include "stdio.h"

const char *YesNoMenuString[]={
    "Yes",
    "No",
    NULL
};

const char *BuySellMenuString[]={
    "Buy",
    "Sell",
    NULL
};

const char *kGameItemEquipPositionsStrings[]={
    "head",
    "right hand",
    "left hand",
    "body",
    "one hand",
    "two hands",
    NULL
};

const char *kGameItemTypesStrings[]={
    "heal living creatures",
    "heal anything",
    "small guns",
    "girl light gear",
    NULL
};

const char *GameplayMenuString[]={
    "Status",
    "Inventory",
    "Technique",
    "Equipment",
    "Magic",
    "Skills",
    NULL
};

const char *kItemMenu[]={
    "Use",
    "Description",
    "Drop",
    NULL
};

const char *kGameMenuMainMenuItems[] ={
    
    "New Game",
    "Load Game",
    "Save Game",
    "Options",
    "Credits",
    "Quit Game",
    NULL
    
};

const char *PlayerBattleModeMainMenuString[]={
    
    "Attack",
    "Runaway",
    NULL
    
};