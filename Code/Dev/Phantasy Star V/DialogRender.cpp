/* 
 * File:   DialogRender.cpp
 * Author: User
 * 
 * Created on March 26, 2012, 10:51 PM
 */

#include "DialogRender.h"
#include "debuglog.h"
#include "Const.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"


DialogRender::DialogRender() {
}

DialogRender::DialogRender(const DialogRender& orig) {
}

DialogRender::~DialogRender() {
}

void DialogRender::setDrawSurface(SDL_Surface *newDrawSurface) {
    
    drawSurface=newDrawSurface;
    
}

void DialogRender::renderDialogBox(int drawX, int drawY, char* dialogText) {
    
    DialogRender::renderDialogBox(drawX,drawY,dialogText,drawSurface);
    
}

char* DialogRender::renderDialogBox(int drawX, int drawY, char *dialogText, SDL_Surface *drawSurface) {
    
    TTF_Font *font = TTF_OpenFont("./gamemodules/system/resources/graphics/font/LiberationMono-Regular.ttf",18);
    
    char buffer[200];
    strcpy(buffer,dialogText);
    
    //TODO draw dialog box and text from surface size
    
    //draw dialog box border
    SDL_Color fColor;
    fColor.r = fColor.g = fColor.b = 255; 
    
    SDL_Rect dialogBoxBorderRect;
    
    dialogBoxBorderRect.x=drawX;
    dialogBoxBorderRect.y=drawY;
    dialogBoxBorderRect.w=kDialogBoxWidth;
    dialogBoxBorderRect.h=kDialogBoxHeight;

    SDL_Rect dialogBoxRect;
    
    dialogBoxRect.x=drawX+kDialogBoxBorder;
    dialogBoxRect.y=drawY+kDialogBoxBorder;
    dialogBoxRect.w=kDialogBoxWidth-kDialogBoxBorder*2;
    dialogBoxRect.h=kDialogBoxHeight-kDialogBoxBorder*2;
    
    
    SDL_FillRect(drawSurface, &dialogBoxBorderRect, 0xFFFFFFFF);    
    SDL_FillRect(drawSurface, &dialogBoxRect, 0x000000FF);    
    
    int textDrawX=0;
    int textDrawY=0;
    
    //split to words
    char *words;
    
    words = strtok (buffer," ");
    while (true)
    {
        
        int surfaceDrawX=drawX+textDrawX+kDialogBoxLeftOffset;
        int surfaceDrawY=drawY+textDrawY+kDialogBoxUpOffset;
        
        SDL_Surface *drawText=TTF_RenderUTF8_Blended(font, words, fColor);  
        
        SDL_Rect textRect;
        textRect.x=surfaceDrawX;
        textRect.y=surfaceDrawY;
        textRect.w=drawText->w;
        textRect.h=drawText->h;
        
        if (textRect.x+textRect.w>dialogBoxRect.x+dialogBoxRect.w) {
            textDrawX=0;
            textDrawY+=20;

            surfaceDrawX=drawX+textDrawX+kDialogBoxLeftOffset;
            surfaceDrawY=drawY+textDrawY+kDialogBoxUpOffset;            
            
            textRect.x=surfaceDrawX;
            textRect.y=surfaceDrawY;            
            
        }
        
        
        textDrawX+=kDialogBoxSpaceWidth+drawText->w;
        
        SDL_BlitSurface(drawText,NULL,drawSurface,&textRect);
        
        SDL_FreeSurface(drawText);
        
        words = strtok (NULL," ");

        if (words==NULL)
            break;
        


        
    }
    TTF_CloseFont(font); 
    
    //TODO return words if outside of render field
    
    
    
}
