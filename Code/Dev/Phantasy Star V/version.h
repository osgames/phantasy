/* 
 * File:   version.h
 * Author: User
 *
 * Created on April 21, 2012, 9:48 PM
 */

#ifndef VERSION_H
#define	VERSION_H

#ifdef	__cplusplus
extern "C" {
#endif

#define kGameVersion "0.2.7"


#ifdef	__cplusplus
}
#endif

#endif	/* VERSION_H */

