/* 
 * File:   ugl.cpp
 * Author: guyfromthespace
 * 
 * Created on May 26, 2012, 8:23 AM
 */

#include "ugl.h"

#define textBoxID 156

ugl::ugl() {
}

ugl::ugl(const ugl& orig) {
}

ugl::~ugl() {
}

void ugl::win32AskToSaveChanges(HWND mainWindow) {
    
    int result=MessageBox(NULL, "Save changes", "Do you want to save script changes?", MB_YESNOCANCEL);
    
    switch (result) {
        
        case IDCANCEL:
            return;
            
        case IDYES:
            //get text from textfield and save back
            char *textToChange=(char*)GetWindowLongPtr(mainWindow,DWL_USER);
            GetWindowText(GetDlgItem(mainWindow, textBoxID), textToChange, kMaxScriptLength);
            break;
            
    }
    
    DestroyWindow(mainWindow);
    PostQuitMessage(0);
    
    
}

BOOL WINAPI ugl::win32WindowCallback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam) 
{
	if(message == WM_CLOSE) {

            ugl::win32AskToSaveChanges(hwnd);
            
        }
		
	//check if text in textbox has been changed by user
	if(message==WM_COMMAND && HIWORD(wParam)==EN_CHANGE && LOWORD(wParam)==textBoxID) 
	{
		//text in the textbox has been modified
		//do your coding here
	}
	
	return false; 
}

void ugl::switchSlashesToWin32(char* path) {
    
        string replacedPath = path;
        replace( replacedPath.begin(), replacedPath.end(), '/', '\\');
        
        strncpy(path,replacedPath.c_str(),kMaxStringLength);

        debugLog("Directory path after switch:"<<path<<"\n");        
        
}

bool ugl::showFileOpenDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription) {
    
    debugLog("Open dialog\n");
    
    ugl::switchSlashesToWin32(directoryPath);
    

    
    OPENFILENAME saveFileStruct;
    
    char fullFilePath[kMaxStringLength];
    
    snprintf(fullFilePath,kMaxStringLength,"%s\\%s",directoryPath,filePath);

    debugLog(fullFilePath<<"\n");
    
    ZeroMemory(&saveFileStruct, sizeof(saveFileStruct));

    saveFileStruct.lStructSize = sizeof(saveFileStruct);
    saveFileStruct.hwndOwner = NULL;
    saveFileStruct.lpstrFilter = fileExtensionDescription;
    saveFileStruct.lpstrFile = fullFilePath;
    saveFileStruct.nMaxFile = MAX_PATH;
    saveFileStruct.Flags = OFN_EXPLORER;
    saveFileStruct.lpstrDefExt = fileExtension;  
    //saveFileStruct.lpstrInitialDir = directoryPath;
    
    bool result=GetOpenFileName(&saveFileStruct);
    
    if (result)
        strncpy(filePath,fullFilePath,kMaxStringLength);
    
    return result;
    
}

bool ugl::showFileSaveDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription) {
    
    debugLog("Save dialog\n");
    
    ugl::switchSlashesToWin32(directoryPath);    
    
    OPENFILENAME saveFileStruct;
    
    char fullFilePath[kMaxStringLength];
    
    snprintf(fullFilePath,kMaxStringLength,"%s\\%s",directoryPath,filePath);

    debugLog(fullFilePath<<"\n");
    
    ZeroMemory(&saveFileStruct, sizeof(saveFileStruct));

    saveFileStruct.lStructSize = sizeof(saveFileStruct);
    saveFileStruct.hwndOwner = NULL;
    saveFileStruct.lpstrFilter = fileExtensionDescription;
    saveFileStruct.lpstrFile = fullFilePath;
    saveFileStruct.nMaxFile = MAX_PATH;
    saveFileStruct.Flags = OFN_EXPLORER;
    saveFileStruct.lpstrDefExt = fileExtension; 
    //saveFileStruct.lpstrInitialDir = directoryPath;
    
    bool result=GetSaveFileName(&saveFileStruct);
    
    if (result)
        strncpy(filePath,fullFilePath,kMaxStringLength);
    
    return result;
    
}

void ugl::showTextEditorWithText(char *windowLabel, char *textToChange) {
    
	MSG msg;
	HWND myDialog = CreateWindowEx(
		0,WC_DIALOG,windowLabel,WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_THICKFRAME,
		400,100,640,480,NULL,NULL,NULL,NULL
	);
	//create the textbox
	CreateWindowEx(
		WS_EX_CLIENTEDGE,
		"EDIT",textToChange,WS_VISIBLE | WS_CHILD | WS_EX_RIGHTSCROLLBAR | ES_MULTILINE | ES_AUTOVSCROLL,
		0,0,640,450,myDialog,(HMENU)textBoxID,NULL,NULL
	);
	
	SetWindowLong(myDialog, DWL_DLGPROC, (long)ugl::win32WindowCallback);
        SetWindowLongPtr(myDialog, DWL_USER, (long)textToChange);

	while(GetMessage(&msg,NULL,0,0)) { 
		TranslateMessage(&msg); 
		DispatchMessage(&msg);
	}  
    
}