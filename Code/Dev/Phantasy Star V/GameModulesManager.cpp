/* 
 * File:   GameModulesManager.cpp
 * Author: User
 * 
 * Created on May 6, 2012, 11:40 AM
 */

#include "GameModulesManager.h"

GameModulesManager::GameModulesManager() {
    
    for (int a=0;a<kMaxGameModules;a++)
        gameModulesList[a]=NULL;
    
}

GameModulesManager::GameModulesManager(const GameModulesManager& orig) {
}

GameModulesManager::~GameModulesManager() {
    
    for (int a=0;a<kMaxGameModules;a++)
        if (gameModulesList[a]!=NULL) {
            debugLog("Old game module removed\n");
            delete(gameModulesList[a]);
        }
    
}

GameModuleSettings** GameModulesManager::getGameModulesList() {
    
    return gameModulesList;
    
}

void GameModulesManager::refreshModulesList() {
    
    map<string,string>parsedConfigFiles[kMaxGameModules];
    FileSystemManager::getParsedConfigFilesFromParentDirectory(kGameModulesPath, parsedConfigFiles);
    
    
    //parse configs to objects
    //remove old
    for (int a=0;a<kMaxGameModules;a++)
        if (gameModulesList[a]!=NULL) {
            debugLog("Old game module removed\n");
            delete(gameModulesList[a]);
        }
    
    modulesCount=0;
    for (int a=0;a<kMaxGameModules;a++) {
        
            if (!parsedConfigFiles[a]["name"].empty() && !parsedConfigFiles[a]["system_pathForConfigParser"].empty()) {
                gameModulesList[modulesCount]=new GameModuleSettings();
                gameModulesList[modulesCount]->setTitle(parsedConfigFiles[a]["name"].c_str());
                gameModulesList[modulesCount]->setPath(parsedConfigFiles[a]["system_pathForConfigParser"].c_str());
                debugLog("New game module on index "<<modulesCount<<"\n");
                if (gameModulesList[modulesCount]==NULL)
                    debugLog("Game module on index "<<modulesCount<<" are still NULL\n");
                else
                    debugLog("Game module on index "<<modulesCount<<" are okay\n");
                
                modulesCount++;
            }
            
    }
    

    
}
