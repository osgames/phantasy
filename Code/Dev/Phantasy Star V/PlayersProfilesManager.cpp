/* 
 * File:   PlayersProfilesManager.cpp
 * Author: demensdeum
 * 
 * Created on January 27, 2013, 12:21 AM
 */

#include "PlayersProfilesManager.h"

PlayersProfilesManager::PlayersProfilesManager() {
}

PlayersProfilesManager::PlayersProfilesManager(const PlayersProfilesManager& orig) {
}

PlayersProfilesManager::~PlayersProfilesManager() {
}

char **PlayersProfilesManager::getPlayersProfiles() {
    
    //get directories list
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);
    
    char *playersProfilesPath[256];
    snprintf(playersProfilesPath,256,"%s/%s",currentDirectory,kPlayersProfilesPath);
    
    char *playersProfilesListRaw[kMaxPlayerProfiles];
    for (int a=0;a<kMaxPlayerProfiles;a++)
        playersProfilesListRaw[a]=NULL;    
    
    FileSystemManager::getDirectoryList(playersProfilesPath, playersProfilesListRaw);
    
}
