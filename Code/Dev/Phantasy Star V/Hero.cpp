/* 
 * File:   Hero.cpp
 * Author: User
 * 
 * Created on June 29, 2012, 11:15 PM
 */

#include "RandomGenerator.h"


#include "Hero.h"
#include "GameMagicDatabase.h"
#include "GameSkillsDatabase.h"

Hero::Hero() {
    gameObject=NULL;
}

Hero::Hero(const Hero& orig) {
}

Hero::~Hero() {
}

void Hero::addEquipmentSpecialization(int newEquipmentSpecialization) {
    equipmentSpecialization.push_back(newEquipmentSpecialization);
}

Hero* Hero::loadHeroFromFilepath(char *filepath, char *gameModule, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase) {

    //load config
    map<string,string>heroConfig=ConfigParser::parseConfigFile(filepath,gameModule);    
    Hero *outputHero=NULL;
    
    //rewrite this mess into loop and static method
    if (heroConfig["name"].empty()) {
        debugLog("Hero config file name are empty\n");
        goto error;
    }
    
    if (heroConfig["job"].empty()) {
        debugLog("Hero config file job are empty\n");
        goto error;
    }    
    
    if (heroConfig["age"].empty()) {
        debugLog("Hero config file age are empty\n");
        goto error;
    }    
    
    if (heroConfig["nowHealth"].empty()) {
        debugLog("Hero config file nowHealth are empty\n");
        goto error;
    }

    if (heroConfig["maxHealth"].empty()) {
        debugLog("Hero config file maxHealth are empty\n");
        goto error;
    }    
    
    if (heroConfig["nowTechPoints"].empty()) {
        debugLog("Hero config file nowTechPoints are empty\n");
        goto error;
    }      
    
    if (heroConfig["maxTechPoints"].empty()) {
        debugLog("Hero config file maxTechPoints are empty\n");
        goto error;
    }  
    
    if (heroConfig["nowMagicPoints"].empty()) {
        debugLog("Hero config file nowMagicPoints are empty\n");
        goto error;
    }  
    
    if (heroConfig["maxMagicPoints"].empty()) {
        debugLog("Hero config file maxMagicPoints are empty\n");
        goto error;
    }      
    
    if (heroConfig["strength"].empty()) {
        debugLog("Hero config file strength are empty\n");
        goto error;
    }      

    if (heroConfig["mental"].empty()) {
        debugLog("Hero config file mental are empty\n");
        goto error;
    }      
    
    if (heroConfig["agility"].empty()) {
        debugLog("Hero config file agility are empty\n");
        goto error;
    }      

    if (heroConfig["dexterity"].empty()) {
        debugLog("Hero config file dexterity are empty\n");
        goto error;
    }      

    if (heroConfig["minAttackPower"].empty()) {
        debugLog("Hero config file minAttackPower are empty\n");
        goto error;
    }      

    if (heroConfig["maxAttackPower"].empty()) {
        debugLog("Hero config file maxAttackPower are empty\n");
        goto error;
    }      

    if (heroConfig["minDefencePower"].empty()) {
        debugLog("Hero config file minDefencePower are empty\n");
        goto error;
    }      

    if (heroConfig["maxDefencePower"].empty()) {
        debugLog("Hero config file maxDefencePower are empty\n");
        goto error;
    }      

    if (heroConfig["nowExperience"].empty()) {
        debugLog("Hero config file nowExperience are empty\n");
        goto error;
    }      

    if (heroConfig["nowExperience"].empty()) {
        debugLog("Hero config file nowExperience are empty\n");
        goto error;
    }      

    if (heroConfig["maxExperience"].empty()) {
        debugLog("Hero config file maxExperience are empty\n");
        goto error;
    }      
    
    if (heroConfig["nowLevel"].empty()) {
        debugLog("Hero config file nowLevel are empty\n");
        goto error;
    }      
    
    if (heroConfig["equipmentSpecialization"].empty()) {
        debugLog("Hero equipmentSpecialization are empty\n");
        goto error;
    }      
    
    if (heroConfig["gameObject"].empty()) {
        
        debugLog("Hero config gameObject are empty\n");
        goto error;
    }
    
    if (heroConfig["headEquipment"].empty()) {
        
        debugLog("Hero config headEquipment are empty\n");
        goto error;
    }
    
    if (heroConfig["rightHandEquipment"].empty()) {
        
        debugLog("Hero config rightHandEquipment are empty\n");
        goto error;
    }
    
    if (heroConfig["leftHandEquipment"].empty()) {
        
        debugLog("Hero config leftHandEquipment are empty\n");
        goto error;
    }
    
    if (heroConfig["bodyEquipment"].empty()) {
        
        debugLog("Hero config bodyEquipment are empty\n");
        goto error;
    }
    
    {
    //TODO all parameters
    outputHero=new Hero();
    
    outputHero->setName(heroConfig["name"]);
    outputHero->setJob(heroConfig["job"]);
    
    //health
    int nowHealth=atoi(heroConfig["nowHealth"].c_str());
    int maxHealth=atoi(heroConfig["maxHealth"].c_str());
    
    //attack
    int minAttackPower=atoi(heroConfig["minAttackPower"].c_str());
    int maxAttackPower=atoi(heroConfig["maxAttackPower"].c_str());
    
    //defence
    int minDefencePower=atoi(heroConfig["minDefencePower"].c_str());
    int maxDefencePower=atoi(heroConfig["maxDefencePower"].c_str());
    
    outputHero->setNowHealth(nowHealth);
    outputHero->setMaxHealth(maxHealth);
    
    outputHero->setMinAttackPower(minAttackPower);
    outputHero->setMaxAttackPower(maxAttackPower);
    
    outputHero->setMinDefencePower(minDefencePower);
    outputHero->setMaxDefencePower(maxDefencePower);
    
    //gameMagic
    if (!heroConfig["gameMagic"].empty()) {
        vector<GameMagic*>gameMagic=gameMagicDatabase->returnGameMagicFromRawString(heroConfig["gameMagic"]);
        outputHero->addGameMagicSystem(gameMagic);
    }
    
    //gameTechnique
    if (!heroConfig["gameTechnique"].empty()) {
        vector<GameTechnique*>gameTechnique=gameTechniqueDatabase->returnGameTechniqueFromRawString(heroConfig["gameTechnique"]);
        outputHero->addGameTechniqueSystem(gameTechnique);    
    }

    //game skills
    if (!heroConfig["gameSkills"].empty()) {
        vector<GameSkill*>gameSkills=gameSkillsDatabase->returnGameSkillsFromRawString(heroConfig["gameSkills"]);
        outputHero->addGameSkillsSystem(gameSkills);
    }
    
    //equipment
    outputHero->setHeadEquipment(gameItemsDatabase->getGameItemWithName(heroConfig["headEquipment"]));
    outputHero->setRightHandEquipment(gameItemsDatabase->getGameItemWithName(heroConfig["rightHandEquipment"]));
    outputHero->setLeftHandEquipment(gameItemsDatabase->getGameItemWithName(heroConfig["leftHandEquipment"]));
    outputHero->setBodyEquipment(gameItemsDatabase->getGameItemWithName(heroConfig["bodyEquipment"]));
    
    //equipment specialization
    {
        vector<string>rawEquipmentSpecialization=StringParser::splitTextToVectorBySeparator((char*)heroConfig["equipmentSpecialization"].c_str(),",");
        for (int a=0;a<rawEquipmentSpecialization.size();a++) {
            int gameItemTypeSpecialization=gameItemsDatabase->getGameItemTypeForString(rawEquipmentSpecialization[a]);
            if (gameItemTypeSpecialization==-1) {
                debugLog("Cannot find hero specialization:"<<rawEquipmentSpecialization[a]<<"\n");
                exit(5);
            }
            outputHero->addEquipmentSpecialization(gameItemTypeSpecialization);
        }
        
    }
    
    if (heroConfig["gameObject"]!=kNoneGameObject) {
    
        char gameObjectPath[kMaxStringLength];
        snprintf(gameObjectPath,kMaxStringLength,"%s/%s/%s/%s",kGameModulesPath,gameModule,kGameModuleObjectsDirectory,heroConfig["gameObject"].c_str());
    
        GameObject *heroGameObject=GameObject::loadObjectFromFile(gameObjectPath);
        heroGameObject->setPlayerTeamMember();
    
        outputHero->setGameObject(heroGameObject);
    }
    
    }
        
error:
    
    return outputHero;
    
    
}

//get equipment
GameItem* Hero::getHeadEquipment() {
    return headEquipment;
}

GameItem* Hero::getRightHandEquipment() {
    return rightHandEquipment;
}

GameItem* Hero::getLeftHandEquipment() {
    return leftHandEquipment;
}

GameItem* Hero::getBodyEquipment() {
    return bodyEquipment;
}
    
//set equipment
void Hero::setHeadEquipment(GameItem *newHeadEquipment) {
    headEquipment=newHeadEquipment;
}

void Hero::setRightHandEquipment(GameItem *newRightHandEquipment) {
    rightHandEquipment=newRightHandEquipment;
}

void Hero::setLeftHandEquipment(GameItem *newLeftHandEquipment) {
    leftHandEquipment=newLeftHandEquipment;
}

void Hero::setBodyEquipment(GameItem *newBodyEquipment) {
    bodyEquipment=newBodyEquipment;
}

void Hero::addGameSkillsSystem(vector<GameSkill*>newGameSkill) {
    for (int a=0;a<newGameSkill.size();a++) {
        gameSkills.push_back(newGameSkill[a]);
    }
}


void Hero::addGameMagicSystem(vector<GameMagic*>newGameMagic) {
    for (int a=0;a<newGameMagic.size();a++) {
        this->addGameMagic(newGameMagic[a]);
    }
}

void Hero::addGameTechniqueSystem(vector<GameTechnique*>newGameTechnique) {
    for (int a=0;a<newGameTechnique.size();a++) {
        gameTechnique.push_back(newGameTechnique[a]);
    }    
}

int Hero::getGameSkillsCount() {
    return gameSkills.size();
}

GameSkill* Hero::getGameSkillAtIndex(int index) {
    return gameSkills[index];
}

int Hero::getGameMagicCount() {
    
    debugLog("getGameMagicCount\n");
    
    return gameMagic.size();
}
GameMagic* Hero::getGameMagicAtIndex(int index) {
    return gameMagic[index];
}

void Hero::addGameMagic(GameMagic *newGameMagic) {
    debugLog("add game magic "<<newGameMagic->getHumanReadableName()<<"\n");
    gameMagic.push_back(newGameMagic);
    debugLog("gameMagic size:"<<gameMagic.size()<<"\n");
}
    
int Hero::getGameTechniqueCount() {
    return gameTechnique.size();
}
GameTechnique* Hero::getGameTechniqueAtIndex(int index) {
    return gameTechnique[index];
}

void Hero::addGameTechnique(GameTechnique *newGameTechnique) {
    gameTechnique.push_back(newGameTechnique);
}

void Hero::setPriorityAtTeam(int newPriority) {
    priorityAtTeam=newPriority;
}

int Hero::getPriorityAtTeam() {
    return priorityAtTeam;
}

void Hero::damage(int damagePower) {
    
    //TODO damage type
    //damage type gear defence etc
    
    int defencePower=RandomGenerator::randomInteger(minDefencePower,maxDefencePower);
    if (defencePower<=damagePower)damagePower-=defencePower;
    else damagePower=0;
    
    debugLog("Damage power:"<<damagePower<<"\n");
    
    nowHealth-=damagePower;
    
}

Hero* Hero::loadHeroFileFromName(char *gameModule, string heroName, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase) {
    
    //parse hero config file
    //new Hero
    //return hero
    
    char heroConfigPath[kMaxStringLength];
    snprintf(heroConfigPath,kMaxStringLength,"%s/%s/%s/%s.%s",kGameModulesPath,gameModule,kHeroesConfigsDirectory,heroName.c_str(),kHeroConfigExtension);

    return Hero::loadHeroFromFilepath(heroConfigPath,gameModule, gameItemsDatabase, gameMagicDatabase, gameTechniqueDatabase, gameSkillsDatabase);
    
}

void Hero::setGameObject(GameObject* newGameObject) {
    gameObject=newGameObject;
}

int Hero::getNowMagicPoints() {
    return nowMagicPoints;
}

int Hero::getMaxMagicPoints() {
    return maxMagicPoints;
}


bool Hero::isAlive() {
    
    return (nowHealth>0);
    
}

    //get
string Hero::getName() {
    return name;
}

string Hero::getJob() {
    return job;
}
    
int Hero::getAge() {
    return age;
}
    
int Hero::getNowHealth() {
    return nowHealth;
}
int Hero::getMaxHealth() {
    return maxHealth;
}
    
int Hero::getNowTechPoints() {
    return nowTechPoints;
}
int Hero::getMaxTechPoints() {
    return maxTechPoints;
}
    
int Hero::getStrength() {
    return strength;
}
int Hero::getMental() {
    return mental;
}
int Hero::getAgility() {
    return agility;
}
int Hero::getDexterity() {
    return dexterity;
}
    
int Hero::getMinAttackPower() {
    return minAttackPower;
}
int Hero::getMinDefencePower() {
    return minDefencePower;
}
    
int Hero::getMaxAttackPower() {
    return maxAttackPower;
}
int Hero::getMaxDefencePower() {
    return maxDefencePower;
}

int Hero::getNowExperience() {
    return nowExperience;
}
int Hero::getMaxExperience() {
    return maxExperience;
}
int Hero::getNowLevel() {
    return nowLevel;
}

    //set    
void Hero::setName(string newName) {
    name=newName;
}
void Hero::setJob(string newJob) {
    job=newJob;
}
    
void Hero::setAge(int newAge) {
    age=newAge;
}
    
void Hero::setNowHealth(int newNowHealth) {
    nowHealth=newNowHealth;
}
void Hero::setMaxHealth(int newMaxHealth) {
    maxHealth=newMaxHealth;
}
    
void Hero::setNowTechPoints(int newNowTechPoints) {
    nowTechPoints=newNowTechPoints;
}
void Hero::setMaxTechPoints(int newMaxTechPoints) {
    maxTechPoints=newMaxTechPoints;
}
    
void Hero::setMaxAttackPower(int newMaxAttackPower) {
    maxAttackPower=newMaxAttackPower;
}
void Hero::setMaxDefencePower(int newMaxDefencePower) {
    maxDefencePower=newMaxDefencePower;
}

void Hero::setStrength(int newStrength) {
    strength=newStrength;
}

void Hero::setMental(int newMental) {
    mental=newMental;
}

void Hero::setAgility(int newAgility) {
    agility=newAgility;
}

void Hero::setDexterity(int newDexterity) {
    dexterity=newDexterity;
}
    
void Hero::setMinAttackPower(int newAttackPower) {
    minAttackPower=newAttackPower;
}

void Hero::setMinDefencePower(int newDefencePower) {
    minDefencePower=newDefencePower;
}
    
void Hero::setNowExperience(int newNowExperience) {
    nowExperience=newNowExperience;
}

void Hero::setNowMagicPoints(int newNowMagicPoints) {
    nowMagicPoints=newNowMagicPoints;
}

void Hero::setMaxMagicPoints(int newMaxMagicPoints) {
    maxMagicPoints=newMaxMagicPoints;
}

void Hero::setMaxExperience(int newMaxExperience) {
    maxExperience=newMaxExperience;
}
void Hero::setNowLevel(int newNowLevel) {
    nowLevel=newNowLevel;
}
    
void Hero::addExperience(int newExperience) {
    //implement also level up
}

void Hero::freeHeadEquipment() {
    headEquipment=NULL;
}
void Hero::freeRightHandEquipment() {
    rightHandEquipment=NULL;
}

void Hero::freeLeftHandEquipment() {
    leftHandEquipment=NULL;
}

void Hero::freeBodyEquipment() {
    bodyEquipment=NULL;
}

bool Hero::canEquipItemWithSpecialization(int itemEquipSpecialization) {
    
    for (int a=0;a<equipmentSpecialization.size();a++) {
        int nowEquipmentSpecialization=equipmentSpecialization[a];
        if (itemEquipSpecialization==nowEquipmentSpecialization) return true;
    }
    return false;
    
}
    
GameObject* Hero::getGameObject() {
    return gameObject;
}