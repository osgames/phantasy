/* 
 * File:   GameObject.h
 * Author: demensdeum
 *
 * Created on December 7, 2011, 9:36 PM
 */

#include <SDL/SDL.h>
#include <string>
#include "GameObjectPosition.h"
#include "GameObjectCommands.h"
#include "Const.h"
#include <map>
#include "ConfigParser.h"
#include <list>

using namespace std;

#ifndef GAMEOBJECT_H
#define	GAMEOBJECT_H

class GameObject {
public:
    GameObject();
    GameObject(const GameObject& orig);
    virtual ~GameObject();
    void setImage(char *imagePath);
    SDL_Surface* getImage();
    SDL_Surface* getIcon();
    
    int getWidth();
    int getHeight();
    
    void setName(string newName);
    string getName();
    
    //parent name uses for save/load map
    void setParentName(string newParentName);
    string getParentName();
    
    int getX();
    int getY();
    
    void setX(int newX);
    void setY(int newY);
    
    bool hitTest(int hitOnX, int hitOnY);
    
    void setPosition(GameObjectPosition* newGamePosition);
    GameObjectPosition* getPosition();
    
    void setImage(SDL_Surface* newImage);
    SDL_Surface *getImageWithoutAnimation();
    
    int getNowCommand();
    void setNowCommand(int newCommand);
    
    void live();
    
    void goRight();
    void goLeft();
    void goUp();
    void goDown();
    
    void setAnimation(int newAnimationFrameSize, int newAnimationFramerate, int newMaximumAnimationFrames,int newAnimationDirection);
    
    bool isAnimated();
    bool parent;
    
    void setAnimationDirection(int newAnimationDirection);
    
    int getAnimationFrameSize();
    int getAnimationFramerate();
    int getMaximumAnimationFrames();
    int getAnimationDirection();
    
    bool isCalled();
    
    void setParent();
    void setChild();
    
    void touch();
    void untouch();
    void setTouchedBy(GameObject *touchedBy);
    GameObject* getTouchedBy();    
    bool isTouched();
    
    int getScriptIndex();
    GameObject* getCalledBy();
    
    GameObject* getCopy();
    
    void setScriptIndex(int newScriptIndex);
    void call();
    void uncall();
    
    void setCalledBy(GameObject *calledByObject);
    
    string getFullPath();
    void setFullPath(string newFullPath);    
    
    void setPlayerTeamMember();
    void setNotPlayerTeamMember();
    bool isPlayerTeamMember();
    
    static GameObject* loadObjectFromFile(char *objectDirectory);
    
    struct GameObjectParentStruct {
        
        char name[kMaxObjectNameLength];
        char fullPath[kShortStringLength];
        bool isAnimated;
        int animationFrameSize;
        int animationFramerate;
        int maximumAnimationFrames;
        
    }; //for serialize/deserialize as parent (without scene specific parameters)
    
    struct GameObjectStruct {
        
        char parentName[kMaxObjectNameLength];
        int animationDirection;
        int scriptIndex;
        int positionX;
        int positionY;
        
    }; //for serialize/deserialize as scene object with script
    
    int getMovementTraceCount();
    GameObjectStruct getLastGameObjectStructFromMovementTrace();
    void clearMovementTrace();
    
    GameObjectParentStruct exportAsParent();
    GameObjectStruct exportAsGameObject(); 
    
private:
    
    GameObjectPosition *gamePosition;
    SDL_Surface *image;
    SDL_Surface *animationFrame;
    string name;
    string parentName;
    int nowCommand;
    
    //script engine data
    bool called;
    bool calledByGiveItem;
    bool touched;
    
    GameObject *calledByObject;
    GameObject *touchedByObject;
    int calledByItemID;
    
    string fullPath;
    
    //Animation
    bool animated;
    
    //number of frame
    int animationFrameNow;
    int maximumAnimationFrames;
    
    //animation frame size
    int animationFrameSize;
    
    int animationFramerate;
    int animationFrameTimeNow;
    
    //object direction for animation
    int animationDirection;
    
    
    int scriptIndex;
    
    SDL_Surface* returnAnimationFrame();
    
    bool playerTeamMember;//don't release memory for playerTeamMember

    list<GameObjectStruct>movementTrace;

    
};

#endif	/* GAMEOBJECT_H */

