/* 
 * File:   Fighter.cpp
 * Author: User
 * 
 * Created on July 7, 2012, 11:26 AM
 * 
 * Monster or player in battle mode
 * 
 */

#include "Fighter.h"

Fighter::Fighter() {
}

Fighter::Fighter(const Fighter& orig) {
    name=orig.name;
    job=orig.job;
    age=orig.age;
    nowHealth=orig.nowHealth;
    maxHealth=orig.maxHealth;
    nowTechPoints=orig.nowTechPoints;
    maxTechPoints=orig.maxTechPoints;
    nowMagicPoints=orig.nowMagicPoints;
    maxMagicPoints=orig.maxMagicPoints;
    strength=orig.strength;
    mental=orig.mental;
    agility=orig.agility;
    dexterity=orig.dexterity;
    
    minAttackPower=orig.minAttackPower;
    minDefencePower=orig.minDefencePower;
    
    maxAttackPower=orig.maxAttackPower;
    maxDefencePower=orig.maxDefencePower;
    
    nowExperience=orig.nowExperience;
    maxExperience=orig.maxExperience;
    nowLevel=orig.nowLevel;
    
    whichSide=orig.whichSide;
    
}

Fighter::~Fighter() {
}

Fighter Fighter::fighterFromHero(Hero *fromHero) {
    
    Fighter outputFighter;
    
    outputFighter.setName(fromHero->getName());
    outputFighter.setJob(fromHero->getJob());
    outputFighter.setAge(fromHero->getAge());
    outputFighter.setNowHealth(fromHero->getNowHealth());
    outputFighter.setMaxHealth(fromHero->getMaxHealth());
    outputFighter.setNowTechPoints(fromHero->getNowTechPoints());
    outputFighter.setMaxTechPoints(fromHero->getMaxTechPoints());
    outputFighter.setNowMagicPoints(fromHero->getNowMagicPoints());
    outputFighter.setMaxMagicPoints(fromHero->getMaxMagicPoints());
    outputFighter.setStrength(fromHero->getStrength());
    outputFighter.setMental(fromHero->getMental());
    outputFighter.setAgility(fromHero->getAgility());
    outputFighter.setDexterity(fromHero->getDexterity());
    outputFighter.setMinAttackPower(fromHero->getMinAttackPower());
    outputFighter.setMaxAttackPower(fromHero->getMaxAttackPower());
    outputFighter.setMinDefencePower(fromHero->getMinDefencePower());
    outputFighter.setMaxDefencePower(fromHero->getMaxDefencePower());
    outputFighter.setNowExperience(fromHero->getNowExperience());
    outputFighter.setMaxExperience(fromHero->getMaxExperience());
    outputFighter.setNowLevel(fromHero->getNowLevel());
    
    
    return outputFighter;
    
}

BattleModeSide Fighter::getBattleModeSide() {
    
    return whichSide;
    
}

void Fighter::setBattleModeSide(BattleModeSide newBattleModeSide) {
    
    whichSide=newBattleModeSide;
    
}