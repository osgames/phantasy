/* 
 * File:   ModulesManager.h
 * Author: demensdeum
 *
 * Created on December 3, 2011, 12:58 AM
 * 
 * Modules Manager controls loading and unloading of game modules
 * 
 */


#include "GameModuleSettings.h"
#include <vector>
#include "Const.h"
#ifdef WINDOWS32_BUILD
#include "win32-dirent.h"
#else
#include "dirent.h"
#endif
#include "debuglog.h"
#include <string.h>

#ifndef MODULESMANAGER_H
#define	MODULESMANAGER_H

using namespace std;

class ModulesManager {
public:
    ModulesManager();
    ModulesManager(const ModulesManager& orig);
    virtual ~ModulesManager();
    
    void refreshModulesList();
    vector<GameModuleSettings*> getGameModules();
    
    
private:

    char** getGameModulesDirectoryList();
    
    vector<GameModuleSettings*>gameModules;
    
};

#endif	/* MODULESMANAGER_H */

