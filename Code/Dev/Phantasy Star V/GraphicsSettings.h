/* 
 * File:   GraphicsSettings.h
 * Author: demensdeum
 *
 * Created on December 17, 2011, 8:13 PM
 */

#ifndef GRAPHICSSETTINGS_H
#define	GRAPHICSSETTINGS_H

class GraphicsSettings {
public:
    GraphicsSettings();
    GraphicsSettings(const GraphicsSettings& orig);
    virtual ~GraphicsSettings();
    static int getScreenWidth();
    static int getScreenHeight();
    static int getColorDepth();
private:

};

#endif	/* GRAPHICSSETTINGS_H */

