/*
 * File:   Const.h
 * Author: User
 *
 * Created on March 10, 2012, 3:47 PM
 */

#include "stdint.h"

#include <vector>

using namespace std;

#ifndef CONST_H
#define	CONST_H

//other solids are triggers with number to script
#define kNoneGameObject "<None>"
#define kMaximumEnemies 4
#define kMaxDialogMessages 128
#define kFrameDelay 6
#define kSolid -1//block solid
#define kEmptySolid -2//pass free
#define kScriptIndexNone -1
#define kMaxMapObjects 128
#define kObjectIDNone -1
#define kLengthTilePath 256
#define kShortStringLength 256
#define kMaxStringLength 1024
#define kInvisibleTile -1
#define kMaxGameModules 256
#define kEnemiesSetFileExtension "ini"
#define kGameModulesPath "gamemodules"
#define kGameModuleSystemModulePath "system"
#define kDefaultIconSize 64
#define kGameModuleConfigFileName "config.ini"
#define kGameMapExtensionDescription "Phantasy Star V Map File"


#define kItemsDatabaseFile "resources/database/items/ItemsList.ini"
#define kItemsDatabaseDirectory "resources/database/items"

#define kTechniqueDatabaseFile "resources/database/technique/TechniqueList.ini"
#define kTechniqueDatabaseDirectory "resources/database/technique"

#define kMagicDatabaseFile "resources/database/magic/MagicList.ini"
#define kMagicDatabaseDirectory "resources/database/magic"
#define kBackgroundMenuImagePath "resources/graphics/menu/background.png"
#define kMenuMusicPath "resources/music/menu/menu.ogg"

#define kSkillsDatabaseFile "resources/database/skills/SkillsList.ini"
#define kSkillsDatabaseDirectory "resources/database/skills"

#define kGameItemConfigExtension "ini"
#define kHeroConfigExtension "ini"
#define kEnemyConfigExtension "ini"
#define kHeroesConfigsDirectory "resources/heroes"
#define kEnemiesSetsDirectory "resources/enemiesSets"
#define kEnemiesDirectory "resources/enemies"
#define kBattleBackgroundsDirectory "resources/graphics/battlemode/backgrounds"
#define kBattleBackgroundImageExtension "png"
#define kBattleBackgroundNonePath "resources/graphics/battlemode/backgrounds/None.png"

#define kGameMapExtension "algomap"
#define kGameMapExtensionDescription "Phantasy Star V Map (*.algomap)\0"

#define kGameDialogExtension "algodialog"
#define kGameDialogExtensionDescription "Phantasy Star V Dialog File (*.algodialog)\0"

#define kMessageIconSeparator "|"

#define kPlayersProfilesPath "playersprofiles"

#define kGameModuleDialogDirectory "resources/text/dialogs"
#define kGameModuleMapsDirectory "resources/maps"
#define kGameModuleTilesDirectory "resources/graphics/tileset"
#define kGameModuleObjectsDirectory "resources/graphics/objects"
#define kTilesetFileDefaultName "tileset.png"
#define kObjectImageFileDefaultName "object.png"

#define kObjectParserKeyName "name"
#define kObjectParserKeyAnimated "animated"
#define kObjectParserKeyAnimationFrameSize "animationFrameSize"
#define kObjectParserKeyAnimationFramerate "animationFramerate"
#define kObjectParserKeyMaximumAnimationFrames "maximumAnimationFrames"
#define kObjectParserKeyAnimationDirection "animationDirection"
#define kConfigParserTrueValue "true"
#define kObjectParserKeyDirectionDown "down"
#define kObjectParserKeyDirectionLeft "left"
#define kObjectParserKeyDirectionRight "right"
#define kObjectParserKeyDirectionUp "up"

#define kTileParserKeyTileType "tileType"
#define kTileParserTypeValueGround "ground"
#define kTileParserTypeValueRoad "road"
#define kTileParserTypeValueWall "wall"

#define kMaxScriptsCount 40
#define kMaxScriptLength 1024
#define kMaxObjectNameLength 6

#define kSaveMapLabel "Save Map File As"
#define kSaveDialogLabel "Save Dialog File As"
#define kOpenDialogLabel "Open Dialog File"

#define kMaxScriptsBuffer kMaxScriptsCount*kMaxScriptLength
#define kScriptEditorLabel "Scripts Editor"
#define kMasterConsoleLabel "Master Console"
#define kObjectPropertiesLabel "Object Properties"
#define kDialogEditorLabel "Dialog Editor"
#define kItemsDatabaseEditorLabel "Items Database Editor"

#define kEraseSolidMapCommand "erase solid map"
#define kAutoSolidMap         "auto solid map"
#define kOpenDialogFile       "open dialog file"
#define kNewDialogFile        "new dialog file"
#define kSwitchTileCommand    "switch tile"
#define kItemsDatabaseEditor  "items database editor"
#define kTechDatabaseEditor   "tech database editor"
#define kMagicDatabaseEditor  "magic database editor"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
#define kRmask 0xff000000
#define kGmask 0x00ff0000
#define kBmask 0x0000ff00
#define kAmask 0x000000ff
#else
#define kRmask 0x000000ff
#define kGmask 0x0000ff00
#define kBmask 0x00ff0000
#define kAmask 0xff000000
#endif


//menu controller constants


    enum {
        
        kAddRemoveTilesButton,
        kSolidMapButton,
        kObjectsAddRemoveButton,
        kSelectionModeButton,
        kScriptsEditor,
        kOpenMapButton,
        kSaveMapButton,
        kPreferencesButton
        
    };
    
    enum {
        
        kExitButton,
        kAddButton,
        kRemoveButton
        
    };
    
enum {

    kResultExit,
    kResultContinue,
    kResultErrorUnknown
    
};
   
enum {

    kMapEditorMapEditView,
    kMapEditorSolidMapEditView,
    kMapEditorAddRemoveTilesView,
    kMapEditorAddRemoveObjectsView,
    kMapEditorSelectionModeView,
    kMapEditorSaveMapView,
    kMapEditorLoadMapView,
    kMapEditorAddTilesView,
    kMapEditorAddObjectsView
        
};
    
enum RenderMode {
    
    kSoftwareSingleThreadRender,
    kSoftwareMultithreadedRender,
    kOpenGLRender
    
};

struct RenderParameters {
    
    int destinationX;
    int destinationY;
    
    int cameraX;
    int cameraY;
    void *drawSurface;
    int maximumX;
    int maximumY;
    void *objects;
    void *tiles;
    int **map;
    bool loopMap;
    
};

enum {

    kMenuChoiceExit = -1,
    kMenuChoiceOutOfRangeUp = -2,
    kMenuChoiceOutOfRangeDown = -3

};

#define kMenuBorder 2
#define kLeftMenuOffset 30
#define kRightMenuOffset 20
#define kUpMenuOffset 10
#define kDownMenuOffset 10
#define kMenuButtonSize 12
#define kLeftMenuButtonOffset 14
#define kUpMenuButtonOffset 13

//dialog box
#define kDialogBoxLeftOffset 10
#define kDialogBoxUpOffset 10
#define kDialogBoxWidth 620
#define kDialogBoxHeight 80
#define kDialogBoxBorder 2
#define kDialogBoxSpaceWidth 8

//BattleMOde menu
//static int kDialogBoxLeftOffset=10;
//static int kDialogBoxUpOffset=10;
//static int kDialogBoxWidth=620;
//static int kDialogBoxHeight=80;
//static int kDialogBoxBorder=2;
//static int kDialogBoxSpaceWidth=8;

#define kTileSize 64
#define kSolidSize (kTileSize/4)
#define kDefaultMapSize 100
#define kMenuCanvasMaxSize 640
#define kMaxGraphicsLayers 9
#define kDefaultTile 0
#define kDefaultTilesInTileset 16
#define kMaxTiles 256
#define kMaxObjects 256
#define kArrangementMapEmptyCell -1
#define kWallsRenderLayer 8
#define kObjectsRenderLayer 8

enum {
    
    kTileTypeGround,    //for ground layer tiles
    kTileTypeRoad,      //for road layer tiles
    kArrangementMapCount, //uses for arrangement map layers
    kTileTypeWall,       //for wall layer tiles (same level as objects)
    kTileTypeUnknown,   //for invalid tiles
    
};

enum {
    
    kSelectTypeNone,
    kSelectTypeTile,
    kSelectTypeSolid,
    kSelectTypeParentGameObject,
    kSelectTypeSceneGameObject
    
};

enum {

    kObjectDirectionDown,
    kObjectDirectionUp,
    kObjectDirectionRight,
    kObjectDirectionLeft

};

#endif	/* CONST_H */

