/*
 * File:   GameMaster.cpp
 * Author: demensdeum
 *
 * Created on December 4, 2011, 7:18 PM
 */

#include "GameEngine.h"
#include "GameplayMenuMaster.h"
#include "BattleMode.h"
#include <SDL/SDL_gfxBlitFunc.h>
#include "unistd.h"



static int kStaticBattleModeRunNumber=42;

GameEngine::GameEngine() {
    gameModule[0]=0;
    playerProfile[0]=0;
    playerObject=NULL;
    playerTeam=NULL;
    oldTime=0;
    renderMode=kSoftwareSingleThreadRender;
#ifdef OPENGL_SUPPORT
    vertices=NULL;
    tex=NULL;
    tilesCount=0;
    
    GLcameraXspeed=0;
    GLcameraYspeed=0;
#endif
    
    battleModeBackgroundString="None";
    enemiesSet="";
    battleModeChance=-1;
}

GameEngine::GameEngine(const GameEngine& orig) {
}

GameEngine::~GameEngine() {
    
    if (playerObject) delete playerObject;
    if (playerTeam) delete playerTeam;
    
}


void GameEngine::loadAndShowDialog(const char* dialogFileName) {
    
    //TODO implement
    
    DialogMessage messages[kMaxDialogMessages];
    int messagesCount=0;
        
    
    debugLog("Load and show dialog:"<<dialogFileName<<"\n");
    
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);    
    
    char dialogFilePath[kMaxStringLength];
    
    snprintf(dialogFilePath,kMaxStringLength,"%s/%s/%s/%s/%s.%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleDialogDirectory,dialogFileName,kGameDialogExtension);
        
    
    FILE *file=NULL;
    
    file=fopen(dialogFilePath,"r");
    if (!file) return;
    
    if (fread(&messagesCount,1,sizeof(int),file)!=sizeof(int)) return;
    
    for (int a=0;a<messagesCount;a++) {
        
        if (fread(&messages[a],1,sizeof(DialogMessage),file)!=sizeof(DialogMessage)) return;
        
    }
    
    //start show messages
    for (int a=0;a<messagesCount;a++) {
    
        DialogMessage nowMessage=messages[a];
        debugLog("Message:"<<nowMessage.message<<"\nIcon:"<<nowMessage.icon<<"\n");
        
        //todo resizable dialog box
drawDialogBox:
        int cameraX=getCameraXFromObject(playerObject);
        int cameraY=getCameraYFromObject(playerObject);
        this->renderScene(cameraX,cameraY,screenSurface);
        
        DialogRender::renderDialogBox((screenSurface->w/2)-kDialogBoxWidth/2,screenSurface->h-(kDialogBoxHeight+10),nowMessage.message,screenSurface);        
        SDL_Flip(screenSurface);
        
        //await user input
        
        while(true) {
            inputController->pollEvents();
            if (inputController->getUseButton()) {
                
                inputController->allButtonsToUnpress();
                break;
                
            }
            else if (inputController->getEscapeButton()) {
                
                inputController->allButtonsToUnpress();
                return;//skip dialog
                
            }
            else if (inputController->getWindowResize()) {
                
                //resized
                //rerender dialog box
                screenSurface=SDL_SetVideoMode(inputController->getWindowWidth(), inputController->getWindowHeight(), 32, SDL_SWSURFACE|SDL_RESIZABLE);
                goto drawDialogBox;
                
            }
            else if (inputController->getApplicationQuit()) {
                
                return;//close game
                
            }

        }
        
    }
    
}

vector<string> GameEngine::parseScriptArguments(char *rawArguments) {
    
    vector<string>outputVector;
    
    string rawArgumentsString=rawArguments;
    
    int leftPosition=0;
    int rightPosition=0;
    
    while (rightPosition!=string::npos) {
        
        rightPosition=rawArgumentsString.find(",",leftPosition);
        string newScript=rawArgumentsString.substr(leftPosition,rightPosition-leftPosition);
        debugLog("newArgument:"<<newScript<<"\n");
        leftPosition=rightPosition+1;
        
        if (newScript.length()>0)
                outputVector.push_back(newScript);
        
    }
    
    return outputVector;
    
}

void GameEngine::runCommandsFromCommandQueue() {
    
    while (!gameEngineCommandQueue.empty()) {
        
        GameEngineCommand *nowCommand=gameEngineCommandQueue.front();
        int commandType=nowCommand->getCommandType();
        
        
        switch (commandType) {
            
            case kSCRIPT_CHANGE_MAP:
                this->changeMapFromScriptLocation(nowCommand->getArgument().c_str());
                break;
            
        }
        
        delete gameEngineCommandQueue.front();
        gameEngineCommandQueue.pop();
        
    }
    
}

void GameEngine::addGameEngineCommandToQueue(int command,string argument) {
    
    GameEngineCommand *newGameEngineCommand=new GameEngineCommand();
    newGameEngineCommand->setArgument(argument);
    newGameEngineCommand->setCommandType(command);
    gameEngineCommandQueue.push(newGameEngineCommand);
    
    debugLog("Command added to gameEngine argument:"<<argument<<"\n");
    
}

void GameEngine::changeMapFromScriptLocation(const char *locationFromScript) {
    
    //TODO make load map etc
    debugLog("Load map from script location:"<<locationFromScript<<"\n");
    
    vector<string>parsedArguments=GameEngine::parseScriptArguments((char*)locationFromScript);

    if (parsedArguments.size()<3)
        return;
    
    string mapName=parsedArguments[0];
    mapName.append(".");
    mapName.append(kGameMapExtension);
    int coordinateX=atoi(parsedArguments[1].c_str());
    int coordinateY=atoi(parsedArguments[2].c_str());
    
    //this->changeMap((char*)mapName.c_str());
    //this->putPlayerObjectOnRenderLayer(coordinateX,coordinateY);
    this->gotoMapAtPosition((char*)mapName.c_str(),coordinateX,coordinateY);
    
}

void GameEngine::callSolid(int solidIndex, GameObject *touchedBy) {

    if (solidIndex<0)
        return;
    
    if (solidIndex>=scripts.size())
        return;

    string nowScriptString=scripts[solidIndex];
    
    GameObject *solidObject=new GameObject();
    solidObject->call();
    solidObject->setCalledBy(touchedBy);
    
    ScriptEngine::runScriptForObject(nowScriptString.c_str(),solidObject,this);
    
    delete solidObject;
  
    
}

void GameEngine::touchSolid(int solidIndex, GameObject *touchedBy) {

    if (solidIndex<0)
        return;
    
    if (solidIndex>=scripts.size())
        return;
    
    string nowScriptString=scripts[solidIndex];
        
    //temporary object represent solid
    GameObject *solidObject=new GameObject();
    solidObject->touch();
    solidObject->setTouchedBy(touchedBy);
    
    
    ScriptEngine::runScriptForObject(nowScriptString.c_str(),solidObject,this);
    
    delete solidObject;
    
    
}

void GameEngine::loadGameItemWithName(string gameItemName) {
    
    /*char gameItemPath[kMaxStringLength];
    snprintf(gameItemPath,kMaxStringLength,"%s/%s/%s/%s.%s",kGameModulesPath,gameModule,kItemsDatabaseDirectory,gameItemName.c_str(),kGameItemConfigExtension);
    debugLog("Items database path:"<<gameItemPath<<"\n");    
    map<string,string>parsedGameItem=ConfigParser::parseConfigFile(gameItemPath, NULL);

    gameItemsDatabase->addGameItemFromDictionary(parsedGameItem);*/
    
}

void GameEngine::loadGameItemsDatabase() {
    
    /*char itemsDatabasePath[kMaxStringLength];
    snprintf(itemsDatabasePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kItemsDatabaseFile);
    debugLog("Items database path:"<<itemsDatabasePath<<"\n");
    //load db file
    FILE *itemsDBfile=NULL;
    itemsDBfile=fopen(itemsDatabasePath,"r");    
    
    uint gameItemsCount=0;
    
    fread(&gameItemsCount,sizeof(uint),1,itemsDBfile);
        
    for (int a=0;a<gameItemsCount;a++) {
            
        GameItemStruct nowGameItemStruct;
        fread(&nowGameItemStruct,sizeof(GameItemStruct),1,itemsDBfile);    
        gameItemsDatabase->addGameItemFromStruct(nowGameItemStruct);
    }
    
    //open items database directory
    
    char itemsDatabasePath[kMaxStringLength];
    snprintf(itemsDatabasePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kItemsDatabaseFile);
    debugLog("Items database path:"<<itemsDatabasePath<<"\n");    
    map<string,string>parsedItemsDatabase=ConfigParser::parseConfigFile(itemsDatabasePath, NULL);
    
    string itemsList=parsedItemsDatabase["ItemsList"];
    if (itemsList.empty()) {
        debugLog("Error loading items database. ItemsList are empty.");
        exit(1);
    }
    
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)itemsList.c_str(),",");
    if (parsedItemsList.size()<1) {
        debugLog("Error loading items database. Parsed items list are empty.");
        exit(2);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItem=parsedItemsList[a];
        this->loadGameItemWithName(nowItem);
    }*/
    
}

void GameEngine::startGameEngine() {

    //SDL init
    //implement video configuration load
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);

  int audio_rate = 22050;
  Uint16 audio_format = AUDIO_S16; /* 16-bit stereo */
  int audio_channels = 2;
  int audio_buffers = 4096;    
    
  if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
    printf("Unable to open audio!\n");
    exit(1);
  }
    
    
    
    if (renderMode==kOpenGLRender) SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    Uint32 flags=0;
    
    if (renderMode!=kOpenGLRender) {
        flags=SDL_SWSURFACE|SDL_RESIZABLE;
    }
    else {
        flags=SDL_OPENGL;
    }
    
    screenSurface=SDL_SetVideoMode(1280, 800, 32, flags);    
    
#ifdef OPENGL_SUPPORT
    if (renderMode==kOpenGLRender) {
        

        //TODO move opengl initialisation to separate method
        glEnable( GL_TEXTURE_2D );

        glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

        glViewport( 0, 0, 1280, 800 );

        glClear( GL_COLOR_BUFFER_BIT );

        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();     
        
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
        
        glOrtho(0.0f, 1280, 800, 0.0f, -1.0f, 1.0f);

        glMatrixMode( GL_MODELVIEW );
        glLoadIdentity();    
    
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        
        
        
    }
#endif

    if(TTF_Init()==-1) {
        printf("TTF_Init: %s\n", TTF_GetError());
        return;
    }
    
    
    
    inputController=new InputController();
    
    //initialize game classes
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderLayers[a]=new RenderLayer();
        mapLayers[a]=new MapMaster();
        mapLayers[a]->newBlankInvisibleMap();
        renderLayers[a]->setMap(mapLayers[a]->getMap());
        renderLayers[a]->setTiles(&tiles);        
    }
    
    //main game menu
    //TODO implement game menu
    GameMenu::getPlayerProfileNameFromPlayerByMenu(playerProfile);
    if (playerProfile[0]==0) {
        debugLog("playerProfileName are empty");
        return;
    }
    
    GameMenu::getGameModuleNameFromPlayerByMenu(playerProfile,gameModule);
    if (gameModule[0]==0) {
        debugLog("gameModule are empty");
        return;
    }
    
    if (GameMenu::getGameModuleMainMenuChoiceFromPlayer(playerProfile,gameModule,screenSurface,inputController)!=kGameModuleMainMenuChoiceNewGame) {
        debugLog("Only new game choice implemented currently");
        return;
    }
    
    
    int newPlayerPositionX=0;
    int newPlayerPositionY=0;
    
    //load game module config
    char gameModuleConfigPath[kMaxStringLength];
    snprintf(gameModuleConfigPath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kGameModuleConfigFileName);
    
    map<string,string>gameModuleConfig=ConfigParser::parseConfigFile(gameModuleConfigPath,gameModule);
    
    if (gameModuleConfig["firstMap"].empty()) {
        
        debugLog("First map are empty\n");
        return;
        
    }
    
    if (gameModuleConfig["startPlayerTeam"].empty()) {
        
        debugLog("startPlayerTeam are empty\n");
        return;
    }
        
    if (gameModuleConfig["startPositionX"].empty()) {
        
        debugLog("startPositionX are empty\n");
        return;
        
    }
    
    if (gameModuleConfig["startPositionY"].empty()) {
        
        debugLog("startPositionY are empty\n");
        return;
    }    

    if (gameModuleConfig["name"].empty()) {
        
        debugLog("Game module name are empty\n");
        return;
    }    

        
    char gameModuleTitle[kShortStringLength];
    sprintf(gameModuleTitle,"%s v%s",gameModuleConfig["name"].c_str(),kGameVersion);    
    
    SDL_WM_SetCaption(gameModuleTitle,NULL);
    
    newPlayerPositionX=atoi(gameModuleConfig["startPositionX"].c_str());
    newPlayerPositionY=atoi(gameModuleConfig["startPositionY"].c_str());
    
    //game items database
    char itemsDatabaseFilePath[kMaxStringLength];
    char itemsDatabaseDirectory[kMaxStringLength];
    
    snprintf(itemsDatabaseFilePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kItemsDatabaseFile);
    snprintf(itemsDatabaseDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kItemsDatabaseDirectory);
    
    char techniqueDatabaseFilePath[kMaxStringLength];
    char techniqueDatabaseDirectory[kMaxStringLength];
    
    snprintf(techniqueDatabaseFilePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kTechniqueDatabaseFile);
    snprintf(techniqueDatabaseDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kTechniqueDatabaseDirectory);    
    
    char magicDatabaseFilePath[kMaxStringLength];
    char magicDatabaseDirectory[kMaxStringLength];
    
    snprintf(magicDatabaseFilePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kMagicDatabaseFile);
    snprintf(magicDatabaseDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kMagicDatabaseDirectory);    
    
    char skillsDatabaseFilePath[kMaxStringLength];
    char skillsDatabaseDirectory[kMaxStringLength];

    snprintf(skillsDatabaseFilePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kSkillsDatabaseFile);
    snprintf(skillsDatabaseDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModule,kSkillsDatabaseDirectory);    
    
    gameTechniqueDatabase=new GameTechniqueDatabase(techniqueDatabaseFilePath,techniqueDatabaseDirectory);
    gameTechniqueDatabase->loadDatabaseFromDatabaseFile();
    
    gameMagicDatabase=new GameMagicDatabase(magicDatabaseFilePath,magicDatabaseDirectory);
    gameMagicDatabase->loadDatabaseFromDatabaseFile();
    
    gameItemsDatabase=new GameItemsDatabase(itemsDatabaseFilePath,itemsDatabaseDirectory);
    gameItemsDatabase->loadDatabaseFromDatabaseFile();
    
    gameSkillsDatabase=new GameSkillsDatabase(skillsDatabaseFilePath,skillsDatabaseDirectory);
    gameSkillsDatabase->loadDatabaseFromDatabaseFile();
    
    
    objectsMaster=new ObjectsMaster(this);
    objectsMaster->setSolidMap(&solidMap);

  
    //gameplay classes
    //playerTeam=new PlayerTeam();
    playerTeam=PlayerTeam::playerTeamFromHeroesNames(gameModule,gameModuleConfig["startPlayerTeam"],gameItemsDatabase,gameMagicDatabase,gameTechniqueDatabase,gameSkillsDatabase);
    vector<GameItem*>startInventory=gameItemsDatabase->returnGameItemsFromRawString(gameModuleConfig["startInventory"]);
    playerTeam->addItemsSystem(startInventory);
    
    //load first map
    this->gotoMapAtPosition((char*)gameModuleConfig["firstMap"].c_str(),newPlayerPositionX,newPlayerPositionY);
    this->gameLoop();
    

 
    
}

void GameEngine::gotoMapAtPosition(char *mapFile, int newPlayerPositionX, int newPlayerPositionY) {
    
    
    this->changeMap(mapFile);
    this->putPlayerTeamOnRenderLayer(newPlayerPositionX,newPlayerPositionY); 
    
}

void GameEngine::putPlayerTeamOnRenderLayer(int newPlayerTeamLeaderPositionX,int newPlayerTeamLeaderPositionY) {
    
    if (!playerTeam) {
        debugLog("player team are NULL");
        return;
    }
    int heroesCount=playerTeam->getHeroesCount();
    
    for (int a=0;a<heroesCount;a++) {
        
        Hero *nowHero=playerTeam->getHeroAtIndex(a);
        GameObject *nowHeroGameObject=nowHero->getGameObject();
        
        nowHeroGameObject->clearMovementTrace();
        
        nowHeroGameObject->setX(newPlayerTeamLeaderPositionX);
        nowHeroGameObject->setY(newPlayerTeamLeaderPositionY);  
        
        nowHeroGameObject->setParent();
        
        objectsMaster->loadObject(nowHeroGameObject);       
        renderLayers[kObjectsRenderLayer]->loadObject(nowHeroGameObject);        
        
    }
    //set leader
    playerObject=playerTeam->getLeaderGameObject();
    playerObject->setName("player");
    
}

//void GameEngine::putPlayerObjectOnRenderLayer(int newPlayerPositionX, int newPlayerPositionY) {
    
    /*playerObject=new GameObject();
    playerObject->setName("player");
    playerObject->setImage("./gamemodules/phantasystarV/resources/graphics/objects/chaz/object.png");
    playerObject->setPosition(new GameObjectPosition(320,240));
    //TODO const for direction
    playerObject->setAnimation(kTileSize,40,3,0); 
    
    renderLayers[kObjectsRenderLayer]->loadObject(playerObject);
    objectsMaster->loadObject(playerObject);       

    playerObject->setX(newPlayerPositionX);
    playerObject->setY(newPlayerPositionY);
    
    playerObject->setParent();*/
    
    //todo all team
    
    /*GameObject* leaderGameObject=playerTeam->getLeaderGameObject();
    if (!leaderGameObject) {
        debugLog("leader don't exist at player team\n");
        return;
    }
    
    playerObject=leaderGameObject;
    
    playerObject->setX(newPlayerPositionX);
    playerObject->setY(newPlayerPositionY);    
    
    playerObject->setParent();
    
    playerObject->setName("player");
    objectsMaster->loadObject(playerObject);       
    renderLayers[kObjectsRenderLayer]->loadObject(playerObject);*/
    
//}

void GameEngine::systemOperations() {
    
    //not gameplay related operations
    this->windowResizeCheck();
    this->getScreenshotCheck();
    
}

void GameEngine::getScreenshotCheck() {
    
    if (inputController->getScreenshotButton()) {
        SDLSurfaceSaverToPNG::saveSurface(screenSurface);
        inputController->allButtonsToUnpress();
    }
    
}

void GameEngine::windowResizeCheck() {
    
    if (inputController->getWindowResize() && renderMode!=kOpenGLRender)
        screenSurface=SDL_SetVideoMode(inputController->getWindowWidth(), inputController->getWindowHeight(), 32, SDL_SWSURFACE|SDL_RESIZABLE);
    
}

void GameEngine::singleThreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    backgroundRender->render(screenSurface);
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
     
        SDL_Thread *nowThread=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h,0,0);
        SDL_WaitThread(nowThread, NULL);
    }
    
}

void GameEngine::multithreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    //experimental multithread render
    //but still slow    
    //sprites sometime don't shows
    
    backgroundRender->render(screenSurface);

    SDL_Thread *renderThreads[4];
    
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderThreads[0]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w/2, drawSurface->h/2,0,0);
        renderThreads[1]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h/2,drawSurface->w/2,0);
        renderThreads[2]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w/2, drawSurface->h,0,drawSurface->h/2);
        renderThreads[3]=renderLayers[a]->render(fromCameraX,fromCameraY,drawSurface,drawSurface->w, drawSurface->h,drawSurface->w/2,drawSurface->h/2);

        
        for (int a=0;a<4;a++) {
            
            SDL_WaitThread(renderThreads[a], NULL);
            
        }       
    
    }
}

#ifdef OPENGL_SUPPORT
void GameEngine::loadMapForOpengl() {

for (int layer=0;layer<kMaxGraphicsLayers;layer++) {
                
                for (int tileY=0;tileY<kDefaultMapSize;tileY++) {
                    
                    for (int tileX=0;tileX<kDefaultMapSize;tileX++) {
                        
                        int nowTile=renderLayers[layer]->getTileIndexAt(tileX,tileY);
                        if (nowTile==kInvisibleTile) continue;
                        
                        int newSizeVertices=sizeof(GLfloat)*12*(tilesCount+1);
                        int newSizeTexcoords=sizeof(GLfloat)*8*(tilesCount+1);
                        
                        //debugLog("newSize:"<<newSize<<"\n");
                        
                vertices=(GLfloat*)realloc(vertices,newSizeVertices);
                tex=(GLfloat*)realloc(tex,newSizeTexcoords);                        
                        
                        
                        
                        int drawX=tileX*kTileSize;
                        int drawY=tileY*kTileSize;
                        
                                int index=nowTile;
                                int nowTileIndex=tilesCount;
                
                                int tileIndex=index/kDefaultTilesInTileset;
                                int tilePart=index%kDefaultTilesInTileset;
                        
                GLfloat tileSourceX=tilePart%4;
                GLfloat tileSourceY=tilePart/4;
                
                tileSourceX=(tileIndex*0.5)+tileSourceX*0.125f;
                tileSourceY=tileSourceY*0.125f;                                
                                
                //1
                vertices[nowTileIndex*12]=drawX;
                vertices[nowTileIndex*12+1]=drawY;
                vertices[nowTileIndex*12+2]=layer/100;
                
                tex[nowTileIndex*8]=tileSourceX;
                tex[nowTileIndex*8+1]=tileSourceY;
                
                //2
                vertices[nowTileIndex*12+3]=drawX+kTileSize;
                vertices[nowTileIndex*12+4]=drawY;
                vertices[nowTileIndex*12+5]=layer/100;
                
                tex[nowTileIndex*8+2]=tileSourceX+0.125f;
                tex[nowTileIndex*8+3]=tileSourceY;
                
                //4
                vertices[nowTileIndex*12+6]=drawX+kTileSize;
                vertices[nowTileIndex*12+7]=drawY+kTileSize;
                vertices[nowTileIndex*12+8]=layer/100;
                
                tex[nowTileIndex*8+4]=tileSourceX+0.125f;
                tex[nowTileIndex*8+5]=tileSourceY+0.125f;
                
                //3
                vertices[nowTileIndex*12+9]=drawX;
                vertices[nowTileIndex*12+10]=drawY+kTileSize;
                vertices[nowTileIndex*12+11]=layer/100;
                
                tex[nowTileIndex*8+6]=tileSourceX;
                tex[nowTileIndex*8+7]=tileSourceY+0.125f;
                                
                                
                                
                                tilesCount++;
                                
                    }
                    
                }
                
            }  

            glVertexPointer(3, GL_FLOAT, 0, vertices);
            glTexCoordPointer(2, GL_FLOAT, 0, tex);

    
}


void GameEngine::openGLRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
            
            //move around for testing
    GLcameraXspeed=0;
    GLcameraYspeed=0;
    
    GLfloat speed=10;
    
    inputController->pollEvents();
    switch (inputController->getLastCommand()) {
        
        case kCommandGoLeft:
            GLcameraXspeed=speed;
            break;
            
        case kCommandGoRight:
            GLcameraXspeed=-speed;
            break;
            
        case kCommandGoUp:
            GLcameraYspeed=speed;
            break;
            
        case kCommandGoDown:
            GLcameraYspeed=-speed;
            break;
    }
    
            glTranslatef(GLcameraXspeed,GLcameraYspeed,0);
    
            glClear( GL_COLOR_BUFFER_BIT );
            glDrawArrays(GL_QUADS,0,(tilesCount*12)/3);
            SDL_GL_SwapBuffers();
                      
}
#endif

void GameEngine::renderScene(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface) {
    
    switch (renderMode) {
        
        case kSoftwareSingleThreadRender:
            this->singleThreadSoftwareRender(fromCameraX,fromCameraY,drawSurface);
            break;
            
        case kSoftwareMultithreadedRender:
            this->multithreadSoftwareRender(fromCameraX,fromCameraY,drawSurface);
            break;

#ifdef OPENGL_SUPPORT
        case kOpenGLRender:
            this->openGLRender(fromCameraX,fromCameraY,drawSurface);
#endif
    }
    
}

void GameEngine::showGameplayMenu() {
    
    GameplayMenuMaster::gameplayMenu(screenSurface,inputController,playerTeam);
    
}

void GameEngine::gameLoop() {
    
/*#ifdef DEBUG
    Shop::startShop("./gamemodules/phantasystarV/resources/shops/AgiraDungeonShop.ini",playerTeam,screenSurface,inputController,gameItemsDatabase);
#endif*/
    
#ifdef ShowFPSCounter
Uint32 startclock = 0;
Uint32 deltaclock = 0;
Uint32 currentFPS = 0;
#endif
    
    while (true) {
        
#ifdef ShowFPSCounter
        startclock = SDL_GetTicks();
#else
        
    Uint32 currentTime=SDL_GetTicks();
    if (currentTime<oldTime+kFrameDelay) continue;
    oldTime=currentTime;        
#endif        
        
        int cameraX=getCameraXFromObject(playerObject);
        int cameraY=getCameraYFromObject(playerObject);
        this->renderScene(cameraX,cameraY,screenSurface);
        SDL_Flip(screenSurface);
        
        inputController->pollEvents();    
        
        if (inputController->getLastCommand()==kCommandGoLeft || inputController->getLastCommand()==kCommandGoRight || inputController->getLastCommand()==kCommandGoUp || inputController->getLastCommand()==kCommandGoDown) {
                if (battleModeChance>0) {
                        if (RandomGenerator::randomInteger(0,battleModeChance)==0)BattleMode::startBattleMode(playerTeam,enemiesSet,screenSurface,inputController,gameModule,gameItemsDatabase,gameMagicDatabase,gameTechniqueDatabase,gameSkillsDatabase, battleModeBackgroundString);
                }
        }
        
        if (inputController->getApplicationQuit()) break;
        this->systemOperations();
        if (inputController->getEscapeButton()) {
            inputController->allButtonsToUnpress();
            this->showGameplayMenu();
        }
        objectsMaster->handleInput(inputController,playerObject);
        objectsMaster->playerTeamFollowLeader(playerTeam);
        objectsMaster->liveStep();
        this->runCommandsFromCommandQueue();
        
#ifdef ShowFPSCounter
deltaclock = SDL_GetTicks() - startclock;
startclock = SDL_GetTicks();
		
if ( deltaclock != 0 )
	currentFPS = 1000 / deltaclock; 

debugLog("FPS:"<<currentFPS<<"\n");

#endif
        
    }
    
    SDL_Quit();
    
}

#ifdef OPENGL_SUPPORT
void GameEngine::makeMegaTexture() {
    
    //calculate texture width and height
    
    int megaTextureWidth=0;
    int megaTextureHeight=0;
    
    for (int a=0;a<tiles.size();a++) {
        
        megaTextureWidth+=tiles[a]->getImage()->w;
        megaTextureHeight=tiles[a]->getImage()->h;
        
    }
    
    SDL_Surface *megaTextureSurface=SDL_CreateRGBSurface(SDL_SWSURFACE,512,512,32,tiles[0]->getImage()->format->Rmask,tiles[0]->getImage()->format->Gmask,tiles[0]->getImage()->format->Bmask,tiles[0]->getImage()->format->Amask);
    
    if (!megaTextureSurface) debugLog("Mega texture error\n");
    
    //Blit all tiles to megaTextuure
    
    SDL_Rect distRect;
    distRect.x=0;
    distRect.y=0;
    
    
    for (int a=0;a<tiles.size();a++) {
      
        Tile* nowTile=tiles[a];
        
        distRect.w=tiles[a]->getImage()->w;
        distRect.h=tiles[a]->getImage()->h;
        
        SDL_gfxBlitRGBA(nowTile->getImage(),NULL,megaTextureSurface,&distRect);
        distRect.x+=tiles[a]->getImage()->w;
        
    }

    //test
    //SDL_BlitSurface(megaTextureSurface,NULL,screenSurface,NULL);
    //SDL_Flip(screenSurface);
    
    {

SDL_Surface *surface=megaTextureSurface;	// This surface will tell us the details of the image
GLenum texture_format;
GLint  nOfColors;
 
 
	// Check that the image's width is a power of 2
	if ( (surface->w & (surface->w - 1)) != 0 ) {
		printf("warning: image.bmp's width is not a power of 2\n");
	}
 
	// Also check if the height is a power of 2
	if ( (surface->h & (surface->h - 1)) != 0 ) {
		printf("warning: image.bmp's height is not a power of 2\n");
	}
 
        // get the number of channels in the SDL surface
        nOfColors = surface->format->BytesPerPixel;
        if (nOfColors == 4)     // contains an alpha channel
        {
                if (surface->format->Rmask == 0x000000ff)
                        texture_format = GL_RGBA;
                else
                        texture_format = GL_BGRA;
        } else if (nOfColors == 3)     // no alpha channel
        {
                if (surface->format->Rmask == 0x000000ff)
                        texture_format = GL_RGB;
                else
                        texture_format = GL_BGR;
        } else {
                printf("warning: the image is not truecolor..  this will probably break\n");
                // this error should not go unhandled
        }
 
	// Have OpenGL generate a texture object handle for us
	glGenTextures( 1, &megaTexture );
 
	// Bind the texture object
	glBindTexture( GL_TEXTURE_2D, megaTexture );
 
	// Set the texture's stretching properties
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
 
	// Edit the texture object's image data using the information SDL_Surface gives us
	glTexImage2D( GL_TEXTURE_2D, 0, nOfColors, surface->w, surface->h, 0,
                      texture_format, GL_UNSIGNED_BYTE, surface->pixels );        
        
    }
    
    SDL_FreeSurface(megaTextureSurface);
    
                glBindTexture( GL_TEXTURE_2D, megaTexture );
    
}
#endif

void GameEngine::changeMap(char *mapName) {
    
    debugLog("Change map:"<<mapName<<"\n");
    
    char currentDirectory[kMaxStringLength];
    getcwd(currentDirectory,kMaxStringLength);    
    
    debugLog(currentDirectory<<"\n");
    
    char mapPath[kMaxStringLength];
    
    snprintf(mapPath,kMaxStringLength,"%s/%s/%s/%s/%s",currentDirectory,kGameModulesPath,gameModule,kGameModuleMapsDirectory,mapName);
    
    debugLog("Map file path:"<<mapPath<<"\n");
    
    objectsMaster->clearObjectsCount();
    
#ifdef OPENGL_SUPPORT
    if (renderMode==kOpenGLRender) {
        
            free(vertices);
            free(tex);        
        
    }    
#endif
    
    MapMaster::loadMap(mapPath,tiles,gameObjects,renderLayers,mapLayers,arrangementMap,wallsArrangementMap,solidMap,scripts,mapProperties);
    
    if (!mapProperties["battleModeChance"].empty()) {
        battleModeChance=atoi(mapProperties["battleModeChance"].c_str());
    }
    
    if (!mapProperties["enemiesSet"].empty()) {
        enemiesSet=mapProperties["enemiesSet"];
    }
    
    for (int a=0;a<kMaxGraphicsLayers;a++) {
        renderLayers[a]->setLoopMap(false);
    }
    
    if (!mapProperties["loopMap"].empty()) {
        if (mapProperties["loopMap"]=="yes") {
                for (int a=0;a<kMaxGraphicsLayers;a++) {
                        renderLayers[a]->setLoopMap(true);
                }            
        }
    }
    
#ifdef OPENGL_SUPPORT
    if (renderMode==kOpenGLRender) {
        
        this->makeMegaTexture();
        this->loadMapForOpengl();
        
    }
#endif
}

bool GameEngine::adaptKeyEvents(InputController *inputController) {

    switch (inputController->getLastCommand()) {

            case kCommandGoLeft:
                playerObject->setNowCommand(kCommandGoLeft);
                return true;

            case kCommandGoRight:
                playerObject->setNowCommand(kCommandGoRight);
                return true;

            case kCommandGoUp:
                playerObject->setNowCommand(kCommandGoUp);
                return true;

            case kCommandGoDown:
                playerObject->setNowCommand(kCommandGoDown);
                return true;

    }

    return false;

}

//TODO not fixed screen resolution
//TODO down and right map border
int GameEngine::getCameraXFromObject(GameObject *gameObject) {

    GameObjectPosition *objectPosition=gameObject->getPosition();

    int objectX=objectPosition->getX();
    int cameraX=objectX-320;

    if (cameraX>0)
        return cameraX;
    else
        return 0;

}

int GameEngine::getCameraYFromObject(GameObject *gameObject) {

    GameObjectPosition *objectPosition=gameObject->getPosition();

    int objectY=objectPosition->getY();
    int cameraY=objectY-240;

    if (cameraY>0)
        return cameraY;
    else
        return 0;

}
