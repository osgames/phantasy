/* 
 * File:   GameModulesManager.h
 * Author: User
 *
 * Created on May 6, 2012, 11:40 AM
 */

#ifndef GAMEMODULESMANAGER_H
#define	GAMEMODULESMANAGER_H

#include "GameModuleSettings.h"
#include "Const.h"
#include "FileSystemManager.h"
#include <map>
#include "ConfigParser.h"

using namespace std;

class GameModulesManager {
public:
    GameModulesManager();
    GameModulesManager(const GameModulesManager& orig);
    virtual ~GameModulesManager();
    
    void refreshModulesList();
    GameModuleSettings** getGameModulesList();
    
private:
    
    int modulesCount;
    GameModuleSettings *gameModulesList[kMaxGameModules];

};

#endif	/* GAMEMODULESMANAGER_H */

