/* 
 * File:   GameMagic.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:52 PM
 */

#ifndef GAMEMAGIC_H
#define	GAMEMAGIC_H

#include "GameDatabaseItem.h"

class GameMagic : public GameDatabaseItem {
public:
    GameMagic();
    GameMagic(const GameMagic& orig);
    virtual ~GameMagic();
private:

};

#endif	/* GAMEMAGIC_H */

