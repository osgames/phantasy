/* 
 * File:   GameMenu.h
 * Author: demensdeum
 *
 * Created on January 27, 2013, 12:35 AM
 */

#ifndef GAMEMENU_H
#define	GAMEMENU_H

#include "PlayersProfilesManager.h"
#include "Const.h"
#include "MenuRender.h"
#include "GameConst.h"
#include "InputController.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_mixer.h"
#include "GraphicEffects.h"
#include "version.h"
#include "SDL/SDL_ttf.h"

class GameMenu {
public:
    GameMenu();
    GameMenu(const GameMenu& orig);
    virtual ~GameMenu();
    
    static void getPlayerProfileNameFromPlayerByMenu(char *outputPlayerProfileName);
    static void getGameModuleNameFromPlayerByMenu(char *playerProfileName,char *outputGameModuleName);
    static int getGameModuleMainMenuChoiceFromPlayer(char *playerProfileName, char *gameModuleName, SDL_Surface *drawSurface, InputController *inputController);
    
private:

};

#endif	/* GAMEMENU_H */

