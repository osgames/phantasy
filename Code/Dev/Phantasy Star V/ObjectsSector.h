/* 
 * File:   ObjectsSector.h
 * Author: demensdeum
 *
 * Created on December 7, 2011, 9:37 PM
 */

#include <list>
#include "GameObject.h"

using namespace std;

#ifndef OBJECTSSECTOR_H
#define	OBJECTSSECTOR_H

class ObjectsSector {
public:
    ObjectsSector();
    ObjectsSector(const ObjectsSector& orig);
    virtual ~ObjectsSector();
    ObjectsSector(GameObject *newUnknownObject,int newSectorWidth, int newSectorHeight);
    
    int getWidth();
    int getHeight();
    
    void addObject(GameObject *newGameObject);
    int getSize();
    GameObject* getObjectAt(int index);
    
private:

    list<GameObject*> objects;
    
    int sectorHeight;
    int sectorWidth;
    
    GameObject *unknownObject;
    
};

#endif	/* OBJECTSSECTOR_H */

