/* 
 * File:   Fighter.h
 * Author: User
 *
 * Created on July 7, 2012, 11:26 AM
 */

#ifndef FIGHTER_H
#define	FIGHTER_H

#include "Hero.h"
#include "GameConst.h"

class Fighter : public Hero {
public:
    Fighter();
    Fighter(const Fighter& orig);
    virtual ~Fighter();
    
    BattleModeSide getBattleModeSide();
    void           setBattleModeSide(BattleModeSide newBattleModeSide);
    static Fighter fighterFromHero(Hero *fromHero);
    
private:

    BattleModeSide whichSide;
    
};

#endif	/* FIGHTER_H */

