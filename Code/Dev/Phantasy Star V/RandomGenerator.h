/* 
 * File:   RandomGenerator.h
 * Author: User
 *
 * Created on July 7, 2012, 5:09 PM
 */

#ifndef RANDOMGENERATOR_H
#define	RANDOMGENERATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

class RandomGenerator {
public:
    RandomGenerator();
    RandomGenerator(const RandomGenerator& orig);
    virtual ~RandomGenerator();
    
    static int randomInteger(int minimal, int maximal);
    
private:

};

#endif	/* RANDOMGENERATOR_H */

