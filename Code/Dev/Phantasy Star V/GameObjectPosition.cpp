/* 
 * File:   GameObjectPosition.cpp
 * Author: demensdeum
 * 
 * Created on December 3, 2011, 1:51 AM
 */

#include "GameObjectPosition.h"

GameObjectPosition::GameObjectPosition() {
}

GameObjectPosition::GameObjectPosition(const GameObjectPosition& orig) {
}

GameObjectPosition::~GameObjectPosition() {
}

GameObjectPosition::GameObjectPosition(int newPositionX, int newPositionY) {
    
    positionX=newPositionX;
    positionY=newPositionY;
    
}

int GameObjectPosition::getX() {
    
    return positionX;
    
}

int GameObjectPosition::getY() {
    
    return positionY;
    
}

void GameObjectPosition::setX(int newPositionX) {
    
    positionX=newPositionX;
    
}

void GameObjectPosition::setY(int newPositionY) {
    
    positionY=newPositionY;
    
}