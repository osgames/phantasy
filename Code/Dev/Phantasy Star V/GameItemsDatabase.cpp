/* 
 * File:   GameItemsDatabase.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:40 PM
 */

#include "GameItemsDatabase.h"

GameItemsDatabase::GameItemsDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory) : GameDatabase(newDatabaseFilePath,newDatabaseResourcesDirectory) {
}

GameItemsDatabase::GameItemsDatabase(const GameItemsDatabase& orig) {
}

GameItemsDatabase::~GameItemsDatabase() {
}

GameItem* GameItemsDatabase::getGameItemWithName(string nowItemName) {
    
    for (int a=0;a<gameItems.size();a++) {
        GameItem *nowItem=gameItems[a];
        if (nowItem->getName()==nowItemName) return nowItem;
    }
    
    return NULL;
    
}

int GameItemsDatabase::getGameItemTypeForString(string rawEquipmentSpecialization) {
    
    for (int a=0;a<kGameItemTypeLength;a++) {
        const char *nowGameItemType=kGameItemTypesStrings[a];
        if (rawEquipmentSpecialization.compare(nowGameItemType)==0) {
            return a;
        }
    }
    return -1;
}

vector<GameItem*> GameItemsDatabase::returnGameItemsFromRawString(string itemsListRawString) {
    
    vector<GameItem*>outputVector;
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)itemsListRawString.c_str(),",");
    
    if (parsedItemsList.size()<1) {
        debugLog("Error. Parsed items list size:"<<parsedItemsList.size()<<"\n");
        exit(4);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItemName=parsedItemsList[a];
        GameItem* outputItem=this->getGameItemWithName(nowItemName);
        if (!outputItem) {
            debugLog("Cannot find item with name:"<<nowItemName<<"\n");
            exit(3);
        }
        outputVector.push_back(outputItem);
        
    }
    
    return outputVector;
    
    
}

int GameItemsDatabase::getGameItemEquipmentPositionForString(string rawEquipmentPosition) {
    
    for (int a=0;a<kGameItemEquipPositionLength;a++) {
        const char *nowGameItemEquipmentPosition=kGameItemEquipPositionsStrings[a];
        if (rawEquipmentPosition.compare(nowGameItemEquipmentPosition)==0) {
            return a;
        }
    }
    return -1;    
    
}

void GameItemsDatabase::addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary) {
    
    
    if (gameDatabaseDictionary["name"].empty()) {
        debugLog("Error loading item. Name key is empty.");
        exit(3);
    }
    
    if (gameDatabaseDictionary["humanReadableName"].empty()) {
        debugLog("Error loading item. Human Readable Name key is empty.");
        exit(3);
    }    
    
    if (gameDatabaseDictionary["description"].empty()) {
        debugLog("Error loading item. Description key is empty.");
        exit(3);
    }  
    
    if (gameDatabaseDictionary["type"].empty()) {
        debugLog("Error loading item. Type key is empty.");
        exit(3);
    }      
    
    GameItem *gameItem=new GameItem();
    gameItem->setName(gameDatabaseDictionary["name"]);
    gameItem->setHumanReadableName(gameDatabaseDictionary["humanReadableName"]);
    gameItem->setDescription(gameDatabaseDictionary["description"]);
    
    int gameItemType=this->getGameItemTypeForString(gameDatabaseDictionary["type"]);
    if (gameItemType==-1) {
        debugLog("Cannot get game item type for raw item type:"<<gameDatabaseDictionary["type"]<<"\n");
        exit(3);
    }
    gameItem->setItemType(gameItemType);
    
    //equipment
    gameItem->setEquipment(false);
    if (gameDatabaseDictionary["equipment"]=="yes") {
        gameItem->setEquipment(true);
    }
    
    if (gameItem->getEquipment()) {
        
        if (gameDatabaseDictionary["equipmentPosition"].empty()) {
            debugLog("Cannot set equipment position for game item - equipmentPosition key are empty.");
        }
        
        int gameItemEquipmentPosition=this->getGameItemEquipmentPositionForString(gameDatabaseDictionary["equipmentPosition"]);
        if (gameItemEquipmentPosition==-1) {
                debugLog("Cannot get game item equipmentPosition:"<<gameDatabaseDictionary["equipmentPosition"]<<"\n");
                exit(3);
        }
        gameItem->setEquipmentPosition(gameItemEquipmentPosition);
        
    }
    
    gameItems.push_back(gameItem);
    
}