/* 
 * File:   GameObjectPosition.h
 * Author: demensdeum
 *
 * Created on December 3, 2011, 1:51 AM
 */

#ifndef GAMEOBJECTPOSITION_H
#define	GAMEOBJECTPOSITION_H

class GameObjectPosition {
public:
    GameObjectPosition();
    GameObjectPosition(const GameObjectPosition& orig);
    virtual ~GameObjectPosition();
    GameObjectPosition(int newPositionX, int newPositionY);
    int getX();
    int getY();
    void setX(int newPositionX);
    void setY(int newPositionY);
    
private:
    
    int positionX;
    int positionY;

};

#endif	/* GAMEOBJECTPOSITION_H */

