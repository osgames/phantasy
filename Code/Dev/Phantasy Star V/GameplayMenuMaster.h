/* 
 * File:   GameplayMenuMaster.h
 * Author: User
 *
 * Created on June 30, 2012, 9:04 AM
 */

#ifndef GAMEPLAYMENUMASTER_H
#define	GAMEPLAYMENUMASTER_H

#include "GameConst.h"
#include "MenuRender.h"
#include "MenuController.h"
#include <vector>
#include "PlayerTeam.h"
#include "GameItem.h"
#include "DialogRender.h"
#include "Fighter.h"
#include "GameConst.h"

class GameplayMenuMaster {
public:
    GameplayMenuMaster();
    GameplayMenuMaster(const GameplayMenuMaster& orig);
    virtual ~GameplayMenuMaster();
    
    static void showEquipmentMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static void gameplayMenu(SDL_Surface *drawSurface, InputController *inputController, PlayerTeam *playerTeam);
    static void showInventory(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController);
    static void showStatus(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController);
    static void showMoney(SDL_Surface *drawSurface, int menuX,int menuY, PlayerTeam *playerTeam);
    static void showFighterStatusWindow(Fighter *nowFighter,SDL_Surface *drawSurface, int menuX,int menuY);
    static void showShopGoodsMenu(vector<ShopItem>goods, SDL_Surface *drawSurface,int menuX,int menuY,InputController *inputController);
    static int youSureMenu(SDL_Surface *drawSurface,int menuX,int menuY, InputController *inputController);
    static void showItemMenu(SDL_Surface *drawSurface, PlayerTeam *playerTeam, InputController *inputController, int itemIndex);
    static void showDescriptionForGameItem(GameItem *gameItem, SDL_Surface *drawSurface, int drawX, int drawY, InputController *inputController);
    static void showItemUseResult(ShowItemResultStruct showItemResultStruct,SDL_Surface *drawSurface, PlayerTeam *playerTeam);
    static void showTechniqueMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static void showMagicMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static void showSkillsMainMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static void showTeamListMenu(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static int selectTeamMember(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController);
    static void drawInventory(SDL_Surface *drawSurface, int menuX, int menuY, PlayerTeam *playerTeam, InputController *inputController);
    static void drawEquipmentOfHeroAtIndex(SDL_Surface *drawSurface, int drawX, int drawY, PlayerTeam *playerTeam, InputController *inputController, int heroIndex);
    
    static BattleModeAction playerBattleModeMenuForFighter(SDL_Surface *drawSurface, InputController *inputController, vector<Fighter>&fighters, int masterFighterIndex);
    static BattleModeAction playerBattleModeEnemyChoice(SDL_Surface *drawSurface, InputController *inputController, vector<Fighter>&fighters, int masterFighterIndex);
    
private:

};

#endif	/* GAMEPLAYMENUMASTER_H */

