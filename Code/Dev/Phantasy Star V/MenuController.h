/* 
 * File:   MenuController.h
 * Author: User
 *
 * Created on March 30, 2012, 11:03 PM
 */

#ifndef MENUCONTROLLER_H
#define	MENUCONTROLLER_H

#include "Const.h"
#include "GameObjectCommands.h"
#include "InputController.h"

class MenuController {
public:
    MenuController();
    MenuController(const MenuController& orig);
    virtual ~MenuController();
    
    static int returnChoice(int menuRange, int menuX, int menuY, SDL_Surface *drawSurface, InputController *inputController);
    static int returnChoiceWithRangeCheck(int menuRange, int menuX, int menuY, SDL_Surface *drawSurface, bool returnOutOfRange, InputController *inputController);
    
private:
    
};

#endif	/* MENUCONTROLLER_H */

