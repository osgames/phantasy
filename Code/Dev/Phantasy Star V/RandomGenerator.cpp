/* 
 * File:   RandomGenerator.cpp
 * Author: User
 * 
 * Created on July 7, 2012, 5:09 PM
 */

#include "RandomGenerator.h"

RandomGenerator::RandomGenerator() {
}

RandomGenerator::RandomGenerator(const RandomGenerator& orig) {
}

RandomGenerator::~RandomGenerator() {
}

int RandomGenerator::randomInteger(int minimal, int maximal) {
    
    
    if (minimal>=maximal) return -1;
    
    srand ( time(NULL) );
    
    
    return rand() % (maximal-minimal) + minimal;
    
}

