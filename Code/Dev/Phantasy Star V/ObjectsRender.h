/* 
 * File:   ObjectsRender.h
 * Author: demensdeum
 *
 * Created on December 17, 2011, 8:52 PM
 */

#include "GameObjectPosition.h"
#include "ObjectsArray.h"

#ifndef OBJECTSRENDER_H
#define	OBJECTSRENDER_H

class ObjectsRender {
public:
    ObjectsRender();
    ObjectsRender(const ObjectsRender& orig);
    virtual ~ObjectsRender();
    void renderObjectsFromPositionToSurface(GameObjectPosition* cameraPosition, ObjectsArray* objectsArray,SDL_Surface *drawSurface);
    void drawSector(ObjectsSector *sectorToDraw,int drawX,int drawY,SDL_Surface *drawSurface);
    
private:

};

#endif	/* OBJECTSRENDER_H */

