/* 
 * File:   GameItemsDatabase.h
 * Author: demensdeum
 *
 * Created on February 2, 2013, 12:40 PM
 */

#ifndef GAMEITEMSDATABASE_H
#define	GAMEITEMSDATABASE_H

#include "GameDatabase.h"
#include "GameItem.h"
#include "GameConst.h"
#include "StringParser.h"

class GameItemsDatabase : public GameDatabase {
public:
    GameItemsDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory);
    GameItemsDatabase(const GameItemsDatabase& orig);
    virtual ~GameItemsDatabase();
    
    GameItem* getGameItemWithName(string nowItemName);
    vector<GameItem*> returnGameItemsFromRawString(string itemsListRawString);
    int getGameItemTypeForString(string rawEquipmentSpecialization);
    int getGameItemEquipmentPositionForString(string rawEquipmentPosition);
    
    
private:
    vector<GameItem*>gameItems;
    void addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary);

};

#endif	/* GAMEITEMSDATABASE_H */

