/* 
 * File:   ObjectsMaster.h
 * Author: demensdeum
 *
 * Created on December 18, 2011, 3:54 PM
 */

#include "ObjectsArray.h"
#include "GameObject.h"
#include "Const.h"
#include "ScriptEngine.h"
#include "InputController.h"
#include "debuglog.h"
#include "GameEngine.h"
#include "PlayerTeam.h"


#ifndef OBJECTSMASTER_H
#define	OBJECTSMASTER_H

class ScriptEngine;
class GameEngine;


class ObjectsMaster {
public:
    ObjectsMaster(GameEngine *newGameEngine);
    ObjectsMaster(const ObjectsMaster& orig);
    virtual ~ObjectsMaster();
    
    void liveStep();
    void handleInput(InputController *inputController, GameObject *playerObject);
    void loadObject(GameObject *newObject);
    void callObjectAtFront(GameObject *callerObject);
    GameObject **getObjects();    
    
    void setSolidMap(int (*newSolidMap)[kDefaultMapSize*4][kDefaultMapSize*4]);
    void clearObjectsCount();
    void playerTeamFollowLeader(PlayerTeam *playerTeam);
    
private:

    //ObjectsArray *objects;
    int objectsCount;
    GameObject *objects[100];
    void processObjectCommand(GameObject *nowObject);
    void goRightObject(GameObject *nowObject);
    void goLeftObject(GameObject *nowObject);
    void goUpObject(GameObject *nowObject);
    void goDownObject(GameObject *nowObject);  
    void runScriptForSolid(int solidIndex);

    
    int (*solidMap)[kDefaultMapSize*4][kDefaultMapSize*4];
    
    GameEngine *gameEngine;
    
};

#endif	/* OBJECTSMASTER_H */

