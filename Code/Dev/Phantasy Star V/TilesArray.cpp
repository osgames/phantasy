/* 
 * File:   TilesArray.cpp
 * Author: demensdeum
 * 
 * Created on December 4, 2011, 5:21 PM
 */

#include "TilesArray.h"

TilesArray::TilesArray() {
}

TilesArray::TilesArray(Tile *newUnknownTile) {
    
    unknownTile=newUnknownTile;
    
}

TilesArray::TilesArray(const TilesArray& orig) {
}

TilesArray::~TilesArray() {
}

void TilesArray::addLine() {
    
    vector<Tile *> newLine;
    tilesMatrix.push_back(newLine);
    
    debugLog("line added " << tilesMatrix.size());
    
}

void TilesArray::addTile(Tile *newTile) {
    
    vector<Tile *> endLine=tilesMatrix.back();
    endLine.push_back(newTile);
    
}

void TilesArray::newLayer()
{
    //delete old tiles
    if (!tilesMatrix.empty()) {
        
        for (int a=0;a<tilesMatrix.size();a++) {
            
            vector<Tile *> nowLine=tilesMatrix[a];
            
            for (int b=0;b<nowLine.size();b++) {
                
                delete nowLine[b];
                
            }
            
        }
        
    }
    
    //erase
    tilesMatrix.erase(tilesMatrix.begin(),tilesMatrix.end());
}

Tile* TilesArray::getTile(int positionX, int positionY) {
    
    //check range
    if (positionX>-1 && positionY>-1 && positionY<tilesMatrix.size()) {
        
        vector<Tile *> nowLine=tilesMatrix[positionY];
        
        //check width range
        if (positionX<nowLine.size()) {
            
            return nowLine[positionX];
            
        }
        else {
        

            return unknownTile;
            
        }
        
        
    }
    else {

        return unknownTile;
    }
    
}
