/* 
 * File:   GameMagicDatabase.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:50 PM
 */

#include "GameMagicDatabase.h"

GameMagicDatabase::GameMagicDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory) : GameDatabase(newDatabaseFilePath,newDatabaseResourcesDirectory) {
}

GameMagicDatabase::GameMagicDatabase(const GameMagicDatabase& orig) {
}

GameMagicDatabase::~GameMagicDatabase() {
}

GameMagic* GameMagicDatabase::getGameMagicWithName(string nowItemName) {
    
    for (int a=0;a<gameMagic.size();a++) {
        GameMagic *nowItem=gameMagic[a];
        if (nowItem->getName()==nowItemName) return nowItem;
    }
    
    debugLog("Cant find game magic with name"<<nowItemName<<"\n");
    exit(9);
    
    return NULL;
    
}

vector<GameMagic*> GameMagicDatabase::returnGameMagicFromRawString(string itemsListRawString) {
    
    vector<GameMagic*>outputVector;
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)itemsListRawString.c_str(),",");
    
    if (parsedItemsList.size()<1) {
        debugLog("Error. Parsed items list size:"<<parsedItemsList.size()<<"\n");
        exit(4);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItemName=parsedItemsList[a];
        GameMagic* outputItem=this->getGameMagicWithName(nowItemName);
        if (!outputItem) {
            debugLog("Cannot find item with name:"<<nowItemName<<"\n");
            exit(3);
        }
        outputVector.push_back(outputItem);
        
    }
    
    return outputVector;
    
    
}


void GameMagicDatabase::addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary) {
    
    if (gameDatabaseDictionary["name"].empty()) {
        debugLog("Error loading item. Name key is empty.");
        exit(3);
    }
    
    if (gameDatabaseDictionary["humanReadableName"].empty()) {
        debugLog("Error loading item. Human Readable Name key is empty.");
        exit(3);
    }    
    
    if (gameDatabaseDictionary["description"].empty()) {
        debugLog("Error loading item. Description key is empty.");
        exit(3);
    }    
    
    GameMagic *newGameMagic=new GameMagic();
    newGameMagic->setName(gameDatabaseDictionary["name"]);
    newGameMagic->setHumanReadableName(gameDatabaseDictionary["humanReadableName"]);
    newGameMagic->setDescription(gameDatabaseDictionary["description"]);
    
    gameMagic.push_back(newGameMagic);    
    
}