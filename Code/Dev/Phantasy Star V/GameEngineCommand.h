/* 
 * File:   GameEngineCommand.h
 * Author: demensdeum
 *
 * Created on June 24, 2012, 9:15 PM
 */

#ifndef GAMEENGINECOMMAND_H
#define	GAMEENGINECOMMAND_H

#include <string>

using namespace std;

class GameEngineCommand {
public:
    GameEngineCommand();
    GameEngineCommand(const GameEngineCommand& orig);
    virtual ~GameEngineCommand();
    
    void setCommandType(int newCommandType);
    void setArgument(string newArgument);
    
    int getCommandType();
    string getArgument();
    
private:
    int commandType;
    string argument;
};

#endif	/* GAMEENGINECOMMAND_H */

