/* 
 * File:   GameModuleSettings.h
 * Author: demensdeum
 *
 * Created on December 18, 2011, 3:20 PM
 */

#include <string>
#include <stdio.h>
#include "string.h"
#include "debuglog.h"
#include "Const.h"

#ifndef GAMEMODULESETTINGS_H
#define	GAMEMODULESETTINGS_H



class GameModuleSettings {
public:
    GameModuleSettings();
    GameModuleSettings(const GameModuleSettings& orig);
    virtual ~GameModuleSettings();
    
    char * getTitle();
    void setTitle(const char *newGameModuleTitle);
    void setTitle(char *newGameModuleTitle);
    char * getIcon();
    
    void setPath(const char *newGameModulePath);
    void setPath(char *newGameModulePath);
    char* getPath();

    
private:

    char gameModuleTitle[kMaxStringLength];
    char gameModulePath[kMaxStringLength];
    
};

#endif	/* GAMEMODULESETTINGS_H */

