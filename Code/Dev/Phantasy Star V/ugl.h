/* 
 * File:   ugl.h
 * Author: guyfromthespace
 *
 * Created on May 26, 2012, 8:23 AM
 * 
 * Universal GUI Library for Algo Editor
 * 
 */

#ifndef UGL_H
#define	UGL_H

#include "debuglog.h"
#include "Const.h"
#include <string>
#include "string.h"
#include <stdlib.h>
#include <stdio.h>

using namespace std;

#ifdef WINDOWS32_BUILD
#include "windows.h"
#endif

#ifdef GTK2_BUILD
#include "gtk-2.0/gtk/gtk.h"
#endif

//TODO for other gui libraries

class ugl {
public:
    ugl();
    ugl(const ugl& orig);
    virtual ~ugl();
    
    static void showTextEditorWithText(char *windowLabel ,char *textToChange);
    static bool showFileSaveDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription);
    static bool showFileOpenDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription);
    static void setFileExtensionForPath(char *filePath, char *fileExtension);
   
#ifdef WINDOWS32_BUILD
    static void switchSlashesToWin32(char *path);
#endif
    
private:

#ifdef WINDOWS32_BUILD
    static BOOL WINAPI win32WindowCallback(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
    static void win32AskToSaveChanges(HWND mainWindow);
#endif
    
    
};

#endif	/* UGL_H */

