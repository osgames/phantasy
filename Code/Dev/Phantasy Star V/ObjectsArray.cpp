/* 
 * File:   ObjectsArray.cpp
 * Author: demensdeum
 * 
 * Created on December 7, 2011, 1:30 AM
 */

#include "ObjectsArray.h"
#include "debuglog.h"




ObjectsArray::ObjectsArray() {
}

ObjectsArray::ObjectsArray(const ObjectsArray& orig) {
}

ObjectsArray::~ObjectsArray() {
}

ObjectsArray::ObjectsArray(ObjectsSector *newUnknownSector) {
    
    unknownSector=newUnknownSector;
    
}


void ObjectsArray::addLine() {
    
    vector<ObjectsSector *> newLine;
    objectsMatrix.push_back(newLine);
    
    debugLog("line added " << objectsMatrix.size());
    
}


void ObjectsArray::addObjectsSector(ObjectsSector *newObjectsSector) {
    
    objectsMatrix.back().push_back(newObjectsSector);   

    
    debugLog("sector added " << objectsMatrix.back().size());
    
}

void ObjectsArray::addObject(GameObject *newGameObject, int positionX, int positionY) {

    //todo add object
    debugLog("Add object: "<<newGameObject->getName()<<" positionX: "<<positionX<<" positionY: "<<positionY);
    
}
    
void ObjectsArray::makeMatrix(int matrixWidth, int matrixHeight,int tileWidth, int tileHeight) {
    
    //TODO erase old matrix
    
    for (int a=0;a<matrixHeight;a++) {
        
        this->addLine();
        
        for (int b=0;b<matrixWidth;b++) {
            
            
            this->addObjectsSector(unknownSector);
            
        }
        
    }
    debugLog("Make matrix");
    
}

ObjectsSector* ObjectsArray::getSector(int positionX,int positionY) {
    
    debugLog("Get sector X:"<<positionX<<" positionY:"<<positionY);
    
    if (positionX>-1 && positionY>-1 && positionY<objectsMatrix.size()) {
    
        return objectsMatrix[positionY][positionX];
        
    }
    else {
        
        return unknownSector;
        
    }
    
    
    
}
