/* 
 * File:   PlayerTeam.cpp
 * Author: User
 * 
 * Created on June 29, 2012, 11:14 PM
 */

#include "PlayerTeam.h"

PlayerTeam::PlayerTeam() {
}

PlayerTeam::PlayerTeam(const PlayerTeam& orig) {
}

PlayerTeam::~PlayerTeam() {
}

ShowItemResultStruct PlayerTeam::useItemFromInventory(int itemIndex) {
    debugLog("Using item is unimplemented");
}

void PlayerTeam::dropItemFromInventory(int itemIndex) {
    if (itemIndex>items.size()) {
        debugLog("Error. Trying to drop item out of range:"<<itemIndex<<"/"<<items.size()<<"\n");
        return;
    }
    items.erase(items.begin()+itemIndex);
}

Hero* PlayerTeam::getHeroAtIndex(int index) {
    return heroes[index];
}

void PlayerTeam::unequipItemAtEquipmentPositionForHeroAtIndex(int itemEquipmentPosition, int heroIndex) {
    
    Hero *targetHero=this->getHeroAtIndex(heroIndex);
    if (!targetHero) return;    
    
    if (itemEquipmentPosition==kGameItemEquipPositionOneHand) {
        
        GameItem *equipedItem=targetHero->getRightHandEquipment();
        if (!equipedItem) return;
        this->addItem(equipedItem);
        targetHero->freeRightHandEquipment();
        
    }    
    
}

void PlayerTeam::equipItemAtIndexForHeroAtIndexSystem(int equipItemIndex, int heroIndex) {

    GameItem *targetItem=this->getItemAtIndex(equipItemIndex);
    Hero *targetHero=this->getHeroAtIndex(heroIndex);
    if (!targetItem) return;
    if (!targetHero) return;
    
    int itemEquipmentPosition=targetItem->getEquipmentPosition();
    this->unequipItemAtEquipmentPositionForHeroAtIndex(itemEquipmentPosition,heroIndex);
    
    //TODO others
    if (itemEquipmentPosition==kGameItemEquipPositionOneHand) {
        
        //TODO ask hand
        targetHero->setRightHandEquipment(targetItem);
        
    }
    
    this->dropItemFromInventory(equipItemIndex);
    
}

void PlayerTeam::equipItemAtIndexForHeroAtIndex(int equipItemIndex, int heroIndex) {
    
    GameItem *targetItem=this->getItemAtIndex(equipItemIndex);
    Hero *targetHero=this->getHeroAtIndex(heroIndex);
    if (!targetItem) return;
    if (!targetHero) return;
    
    int itemType=targetItem->getItemType();
    if (!targetHero->canEquipItemWithSpecialization(itemType)) return;
    
    //TODO equip
    this->equipItemAtIndexForHeroAtIndexSystem(equipItemIndex,heroIndex);
    
    
}

void PlayerTeam::addHero(Hero *newHero) {

    heroes.push_back(newHero);
    
}

signed long long PlayerTeam::getMoney() {
    return money;
}

void PlayerTeam::addItemSystem(GameItem* newGameItem) {
    
    items.push_back(newGameItem);
    
}

void PlayerTeam::takeMoney(int amount) {
    money-=amount;
}

void PlayerTeam::giveMoney(int amount) {
    money+=amount;
}

void PlayerTeam::removeItemAtIndex(int index) {
    items.erase(items.begin()+index);
}

void PlayerTeam::giveItem(GameItem *nowItem) {
    //TODO add capacity check
    items.push_back(nowItem);
}

void PlayerTeam::addItem(GameItem *newGameItem) {
    this->addItemSystem(newGameItem);
}

void PlayerTeam::addItemsSystem(vector<GameItem*>newItems) {//without capacity check
    
    for (int a=0;a<newItems.size();a++) {
        
        GameItem *nowItem=newItems[a];
        this->addItem(nowItem);
        
    }
    
}

GameObject* PlayerTeam::getLeaderGameObject() {
    
    return heroes[0]->getGameObject();
    
}

PlayerTeam* PlayerTeam::playerTeamFromHeroesNames(char *gameModule, string heroesNames, GameItemsDatabase *gameItemsDatabase,GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase) {
    
    //TODO implement
    //parse
    //malloc player team
    //load every hero
    //add to team
    //return pointer
    
    PlayerTeam *outputPlayerTeam=new PlayerTeam();
    
    vector<string>parsedNames=StringParser::splitTextToVectorBySeparator((char*)heroesNames.c_str(),",");
    
    for (int a=0;a<parsedNames.size();a++) {
        string nowName=parsedNames[a];
        Hero *newHero=Hero::loadHeroFileFromName(gameModule,nowName,gameItemsDatabase,gameMagicDatabase,gameTechniqueDatabase,gameSkillsDatabase);
        newHero->setPriorityAtTeam(a);
        outputPlayerTeam->addHero(newHero);
    }
    
    return outputPlayerTeam;
    
}

int PlayerTeam::getItemsCount() {
    
    return items.size();
    
}
GameItem* PlayerTeam::getItemAtIndex(int index) {
    
    return items[index];
    
}

int PlayerTeam::getHeroesCount() {
    
    return heroes.size();
    
}

vector<Fighter> PlayerTeam::getFighters() {
    
    vector<Fighter>outputFighters;
    
    for (int a=0;a<heroes.size();a++) {
        
        Fighter newFighter=Fighter::fighterFromHero(heroes[a]);
        newFighter.setBattleModeSide(kPlayerTeam);
        
        debugLog("Battle mode on adding:"<<newFighter.getBattleModeSide()<<"\n");
        
        outputFighters.push_back(newFighter);
        
        debugLog("Battle mode after adding:"<<outputFighters[a].getBattleModeSide()<<"\n");
        
    }
    
    return outputFighters;
    
}