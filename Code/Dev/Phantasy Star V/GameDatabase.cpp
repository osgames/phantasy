/* 
 * File:   GameDatabase.cpp
 * Author: demensdeum
 * 
 * Created on February 2, 2013, 12:06 PM
 */

#include "GameDatabase.h"

GameDatabase::GameDatabase() {
    
}

GameDatabase::GameDatabase(char *newDatabaseFilePath, char *newDatabaseResourcesDirectory) {
    databaseFilePath=newDatabaseFilePath;
    databaseResourcesDirectory=newDatabaseResourcesDirectory;
}

GameDatabase::GameDatabase(const GameDatabase& orig) {
}

GameDatabase::~GameDatabase() {
}

void GameDatabase::loadItemFromResourcesDirectoryWithName(string itemName) {
    
    char itemFilePath[kMaxStringLength];
    snprintf(itemFilePath,kMaxStringLength,"%s/%s.%s",databaseResourcesDirectory.c_str(),itemName.c_str(),kGameItemConfigExtension);
    debugLog("itemFilePath:"<<itemFilePath<<"\n");
    map<string,string>parsedItemFile=ConfigParser::parseConfigFile(itemFilePath,NULL);
    this->addGameDatabaseItemFromDictionary(parsedItemFile);
}

void GameDatabase::loadItemsFromListString(string rawListString) {
 
    vector<string>parsedItemsList=StringParser::splitTextToVectorBySeparator((char*)rawListString.c_str(),",");
    if (parsedItemsList.size()<1) {
        debugLog("Error loading items database. Parsed items list are empty.");
        exit(2);
    }
    
    for (int a=0;a<parsedItemsList.size();a++) {
        
        string nowItem=parsedItemsList[a];
        this->loadItemFromResourcesDirectoryWithName(nowItem);
    }    
    
}

void GameDatabase::loadDatabaseFromDatabaseFile() {
    

    debugLog("Database path:"<<databaseFilePath<<"\n");    
    map<string,string>parsedItemsDatabase=ConfigParser::parseConfigFile((char*)databaseFilePath.c_str(), NULL);
    
    string itemsList=parsedItemsDatabase["list"];
    if (itemsList.empty()) {
        debugLog("Error loading items database. Items list are empty.");
        exit(1);
    }
    
    this->loadItemsFromListString(itemsList);
    
}

void GameDatabase::addGameDatabaseItemFromDictionary(map<string,string>gameDatabaseDictionary) {
    
}