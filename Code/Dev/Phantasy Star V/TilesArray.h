/* 
 * File:   TilesArray.h
 * Author: demensdeum
 *
 * Created on December 4, 2011, 5:21 PM
 */

#include <vector>
#include "Tile.h"
#include "debuglog.h"

using namespace std;

#ifndef TILESARRAY_H
#define	TILESARRAY_H

class TilesArray {
public:
    TilesArray();
    TilesArray(const TilesArray& orig);
    virtual ~TilesArray();
    TilesArray(Tile *newUnknownTile);
    
    Tile* getTile(int positionX,int positionY);
    void newLayer();
    void addTile(Tile *newTile);
    void addLine();
    
private:

    vector<vector<Tile*> > tilesMatrix;
    Tile* unknownTile;
    
};

#endif	/* TILESARRAY_H */

