/* 
 * File:   MapEditor.h
 * Author: User
 *
 * Created on April 16, 2012, 1:48 AM
 */

#ifndef MAPEDITOR_H
#define	MAPEDITOR_H

#include "InputController.h"
#include "Const.h"
#include "MapMaster.h"
#include "RenderLayer.h"
#include "ResourcesManager.h"
#include "SDL/SDL_ttf.h"
#include "BackgroundRender.h"
#include "ugl.h"
#include "ScriptEngine.h"
#include "DialogMessage.h"


class MapEditor {
public:
    MapEditor();
    MapEditor(const MapEditor& orig);
    virtual ~MapEditor();
    void startMapEditor();
private:
    
    MapMaster *mapLayers[kMaxGraphicsLayers];
    RenderLayer *renderLayers[kMaxGraphicsLayers];
    BackgroundRender *backgroundRender;
    
    InputController *inputController;
    
    int cameraX;
    int cameraY;
    
    TTF_Font *font;
    bool isShowAdvancedInformation;
    
    int arrangementMap[kArrangementMapCount][kDefaultMapSize*2][kDefaultMapSize*2];
    int wallsArrangementMap[kDefaultMapSize][kDefaultMapSize];
    int solidMap[kDefaultMapSize*4][kDefaultMapSize*4];
    
    SDL_Surface *screenSurface;
    
    vector<Tile*> tiles;
    vector<GameObject*> gameObjects;
    vector<string> scripts;
    
    void removeTileset();
    void scriptsEditor();
    
    void openDialogFile();
    void newDialogFile(char *inputBuffer);
    void saveDialogToFileFromRawBuffer(char *dialogInputBuffer, char *dialogName);
    
    void autoSolidMap();
    void editObjectProperties();
    void eraseSolidMap();
    void decompileScriptsToMultilineText(char *outputMultilineText);
    vector<string> multilineTextToRawScripts(char *inputMultilineText);
    
    void parseDialogFileToMultiline(char* dialogFile, char* editorBuffer);
    
    void addRemoveTilesView();
    
    void changePropertiesForSolid(int mouseX, int mouseY);
    void addTilesView();
    void addObjectsView();
    
    void fixTilesAllLayers();
    void addRemoveObjectsView();
    
    void mapEditorLoop();
    void mapEditView();
    
    void renderSolidMap();
    
    void saveMap();
    void openMap();
    
    void itemsDatabaseEditor();
    
    void masterConsole();
    void getScreenshotCheck();
    void systemOperations();
    
    void eraseSolidsUnderMouse(int mouseX, int mouseY);
    void objectsSelectonView();
    void selectObjects(int selectOnX, int selectOnY);
    void renderObjectsSelection();
    void deleteSelectedSceneObject();
    void showAdvancedInformation(int cameraX, int cameraY, int mouseX, int mouseY, SDL_Surface *screenSurface);
    
    void putSelectedTypeByMouse(int mouseX, int mouseY);
    void putTileOnMapByMouse(int mouseX, int mouseY);
    void putSolidOnMapByMouse(int mouseX, int mouseY);
    void putGameObjectOnMapByMouse(int mouseX, int mouseY);    
    
    GameObject* getParentObjectByName(char *parentName);
    GameObject* getChildObjectByParentName(char *parentName);
    bool removeChildObjectByParentName(char *parentName);
    void loadGameObject(GameObject::GameObjectStruct);
    void removeParentObject();
    
    void applicationQuitCheck();
    void windowResizeCheck();
    void checkRefreshMap();
    
    void eraseTilesUnderMouse(int mouseX, int mouseY);
    
    void saveMapSystem(char *filePath);
    void openMapSystem(char *filePath);    
    
    SDL_Surface *getTileByMouse(int mouseX, int mouseY);
    void getGameObjectByMouse(int mouseX, int mouseY);
    
    SDL_Surface *mainMenuPanelImage;
    SDL_Surface *addRemoveExitPanelImage;
    
    int viewMode;
    bool run;
    
    int selectType;
    int selectNumber;
    int selectLayer;
    
    char *gameModule;
    
    Uint32 oldTime;
    
    string currentLanguage;
    
    string currentMapPath;
    
    RenderMode renderMode;
    
    void multithreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    void singleThreadSoftwareRender(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    
    void switchTile(int tileIndex, int replaceTo, int layerIndex);
    
    void renderScene(int fromCameraX, int fromCameraY, SDL_Surface *drawSurface);
    
    static vector<GameItemStruct> parseMultilineTextToGameItems(char* multilineText);
    map<string,string>mapProperties;

    
};

#endif	/* MAPEDITOR_H */

