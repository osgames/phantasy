/* 
 * File:   FileSystemManager.cpp
 * Author: User
 * 
 * Created on May 6, 2012, 12:39 AM
 */

#include "FileSystemManager.h"


FileSystemManager::FileSystemManager() {
}

FileSystemManager::FileSystemManager(const FileSystemManager& orig) {
}

FileSystemManager::~FileSystemManager() {
}

void FileSystemManager::getParsedConfigFilesFromParentDirectory(char *parentDirectoryPath,map<string,string>parsedConfigFiles[kMaxGameModules]) {

    char *gameModulesListRaw[kMaxGameModules];
    for (int a=0;a<kMaxGameModules;a++)
        gameModulesListRaw[a]=NULL;
    
    FileSystemManager::getDirectoryList(parentDirectoryPath, gameModulesListRaw);
    int parsedCount=0;
    
    for (int a=0;a<kMaxGameModules;a++)
        if (gameModulesListRaw[a]==NULL)
            break;
        else {
            char nowConfigPath[kMaxStringLength];
            snprintf(nowConfigPath,kMaxStringLength,"%s/%s/%s",parentDirectoryPath,gameModulesListRaw[a],kGameModuleConfigFileName);
            
            debugLog("Pre parse config file path:"<<nowConfigPath<<"\n");
            
            map<string,string>nowParsedConfig=ConfigParser::parseConfigFile(nowConfigPath,gameModulesListRaw[a]);

            if (!nowParsedConfig["name"].empty()) {
                debugLog("Add parsed config\n");
                debugLog("now parsed config name:"<<nowParsedConfig["name"]<<"\n");                
                parsedConfigFiles[parsedCount]=nowParsedConfig;
                parsedCount++;
            }
        }
     
     //free memory
    for (int a=0;a<kMaxGameModules;a++)
        if (gameModulesListRaw[a]!=NULL)
            free(gameModulesListRaw[a]);      
    
    
}


int FileSystemManager::getDirectoryList(char *directoryPath, char **gameModulesDirectoryList) {
    
    int itemsCount=0;
    
    DIR *gameModulesDIR;
    gameModulesDIR = opendir(directoryPath);
    
    if (gameModulesDIR==NULL) {
        debugLog("Can't open game modules directory:"<<kGameModulesPath<<"\n");
        return 0;
    }
    
    struct dirent *nowDirectory;
    
    while (nowDirectory = readdir (gameModulesDIR)) {
     
        char *directoryName=nowDirectory->d_name;
        
        debugLog("nowDirectory->d_name:"<<nowDirectory->d_name<<"\n");
        
        if (!strcmp(directoryName,"."))
            continue;
            
        debugLog("Game modules directory contains:"<<directoryName<<"\n");

        gameModulesDirectoryList[itemsCount]=(char*)malloc(kMaxStringLength*sizeof(char));
        //strncpy(gameModulesDirectoryList[itemsCount],directoryName,strlen(directoryName));
        snprintf(gameModulesDirectoryList[itemsCount],kMaxStringLength,"%s",directoryName);
        
        itemsCount++;
        
        if (itemsCount>kMaxGameModules-2)
            break;
        
        
    }
    
    closedir(gameModulesDIR);
    
    gameModulesDirectoryList[itemsCount]=NULL;
    return itemsCount;
    
}
