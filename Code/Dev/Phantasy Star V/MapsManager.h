/* 
 * File:   MapsManager.h
 * Author: demensdeum
 *
 * Created on December 3, 2011, 1:00 AM
 * 
 * Maps manager controls loading and save maps
 * 
 */

#ifndef MAPSMANAGER_H
#define	MAPSMANAGER_H

class MapsManager {
public:
    MapsManager();
    MapsManager(const MapsManager& orig);
    virtual ~MapsManager();
private:

};

#endif	/* MAPSMANAGER_H */

