/* 
 * File:   ugl.cpp
 * Author: guyfromthespace
 * 
 * Created on May 26, 2012, 8:23 AM
 */

#include "ugl.h"
#include <string.h>

using namespace std;

ugl::ugl() {
}

ugl::ugl(const ugl& orig) {
}

ugl::~ugl() {
}

char* showTextEditorWithTextAndReturnResult() {
    
}

bool ugl::showFileOpenDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription) {
    
    gtk_init(NULL, NULL);
    
    GtkWidget *openDialog;
    openDialog = gtk_file_chooser_dialog_new (windowLabel,
				      NULL,
				      GTK_FILE_CHOOSER_ACTION_OPEN,
				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				      GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
				      NULL);
    char filterString[kMaxStringLength];
    snprintf(filterString,kMaxStringLength,"*.%s",kGameMapExtension);
    
    GtkFileFilter *filter = gtk_file_filter_new();
    gtk_file_filter_set_name(filter,kGameMapExtensionDescription);
    gtk_file_filter_add_pattern(filter, filterString);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(openDialog),directoryPath);
    gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(openDialog),filter);

    
    bool result=false;
  gint dialogResult = gtk_dialog_run (GTK_DIALOG (openDialog));
  switch (dialogResult)
  {
      
      case GTK_RESPONSE_ACCEPT:
          result=true;
          break;
 
          
      default:
          result=false;
          break;
      
  }
  
        char *filename;
        filename=gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (openDialog));
        if (filename!=NULL) {
                strncpy(filePath,filename,kMaxStringLength);
                g_free (filename);
        }


        gtk_widget_destroy (openDialog);
        
        
        
while (gtk_events_pending ())
        gtk_main_iteration ();        
        
        return result;
  
}

void ugl::showTextEditorWithText(char *windowLabel ,char *textToChange) {
    
#ifdef GEDIT_SUPPORT
    const char *tempFileName="/tmp/algoEditorTextEditorTemporaryFile";
    {
        //create temporary file
        FILE *tempEditorTextFile=fopen(tempFileName,"w");
        fwrite(textToChange,1,strlen(textToChange),tempEditorTextFile);
        fclose(tempEditorTextFile);
    }
    
    //open text for editing
    {
        char systemPath[256];
        snprintf(systemPath,256,"gedit %s",tempFileName);
        system(systemPath);
    }
    
    //read edited text
    {
        FILE *tempEditorTextFile=fopen(tempFileName,"r");
        //fread(textToChange,1,strlen(textToChange),tempEditorTextFile);
        //memset(textToChange,0,kMaxScriptsBuffer);
        int character;
        int characterIndex=0;
        while ((character = fgetc(tempEditorTextFile)) != EOF) {
            textToChange[characterIndex]=character;
            characterIndex++;
        }        
        
        fclose(tempEditorTextFile);
    }
    
#endif
    
}

bool ugl::showFileSaveDialog(char *windowLabel, char *filePath, char *directoryPath, char *fileExtension, char *fileExtensionDescription) {
    
    gtk_init(NULL, NULL);
    
    GtkWidget *saveDialog;
    saveDialog = gtk_file_chooser_dialog_new (windowLabel,
				      NULL,
				      GTK_FILE_CHOOSER_ACTION_SAVE,
				      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				      GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
				      NULL);
    
    char filterString[kMaxStringLength];
    snprintf(filterString,kMaxStringLength,"*.%s",kGameMapExtension);
    
    GtkFileFilter *filter = gtk_file_filter_new();
    gtk_file_filter_set_name(filter,kGameMapExtensionDescription);
    gtk_file_filter_add_pattern(filter, filterString);
    gtk_file_chooser_set_current_name(GTK_FILE_CHOOSER(saveDialog),filePath);
    gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(saveDialog),directoryPath);
    gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(saveDialog),filter);
    
    
    bool result=false;
  gint dialogResult = gtk_dialog_run (GTK_DIALOG (saveDialog));
  switch (dialogResult)
  {
      
      case GTK_RESPONSE_ACCEPT:
          result=true;
          break;
 
          
      default:
          result=false;
          break;
      
  }
  
        char *filename;
        filename=gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (saveDialog));
        if (filename!=NULL) {
                strncpy(filePath,filename,kMaxStringLength);
                g_free (filename);
        }


        gtk_widget_destroy (saveDialog);
        
        
        
while (gtk_events_pending ())
        gtk_main_iteration ();        
        
        return result;
  
}