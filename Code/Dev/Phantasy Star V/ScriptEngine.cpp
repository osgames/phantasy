/* 
 * File:   GameObjectsMaster.cpp
 * Author: User
 * 
 * Created on May 26, 2012, 3:45 PM
 */

#include "ScriptEngine.h"

ScriptEngine::ScriptEngine() {
}

ScriptEngine::ScriptEngine(const ScriptEngine& orig) {
}

ScriptEngine::~ScriptEngine() {
}

vector<string> ScriptEngine::splitTextToVectorByWhitespace(char *inputMultilineText) {
    
    string multilineString=inputMultilineText;
    vector<string>outputVector;
    
    uint leftPosition=0;
    uint rightPosition=0;
    
    while (rightPosition!=string::npos) {
        
        rightPosition=multilineString.find(" ",leftPosition);
        string newScript=multilineString.substr(leftPosition,rightPosition-leftPosition);
        debugLog("newScript:"<<newScript<<"\n");
        leftPosition=rightPosition+1;
        
        if (newScript.length()>0)
                outputVector.push_back(newScript);
        
    }
    
    return outputVector;
    
}

void ScriptEngine::decompileScript(char* inputScriptCode, char* outputHumanString) {
    
    string outputString;
    
    for (int a=0;a<kMaxScriptLength-1;a++) {
        
        char nowScriptByte=inputScriptCode[a];
        
        if (!nowScriptByte)
            break;
            
            //decompile
            if (nowScriptByte<kSCRIPT_API_LENGTH) {
                
                //check for user data
                if (a!=0)
                    if (inputScriptCode[a-1]>=kSCRIPT_API_LENGTH)
                        outputString.append(" ");
                
                
                debugLog("append:"<<scriptEngineAPIhumanLanguage[(int)nowScriptByte]<<"\n");
                outputString.append(scriptEngineAPIhumanLanguage[(int)nowScriptByte]);
                outputString.append(" ");
            }
            
        else {
            

            outputString+=nowScriptByte;
            
        }
        
    }
    
    strncpy(outputHumanString,outputString.c_str(),kMaxStringLength);
}

void ScriptEngine::performCommand(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor) {
    
    char nowByte=script[cursor];
    cursor++;

    if (nowByte==kSCRIPT_CHANGE_MAP) {
        
        nowByte=script[cursor];
        
        //parse location and ask game engine to change location
        string location;
    
        while (nowByte) {
        
        if (nowByte>kSCRIPT_API_LENGTH) {
                location+=nowByte;
                cursor++;
                nowByte=script[cursor];
        }
        else {
        
            break;
                
        }
        
        }
        
        if (location.empty())
                return;
                
        //add command to engine queue
        gameEngine->addGameEngineCommandToQueue(kSCRIPT_CHANGE_MAP,location);
        
        
        
        
    }
    else if (nowByte==kSCRIPT_SHOW_DIALOG) {
        
        nowByte=script[cursor];
        
        //parse location and ask game engine to change location
        string dialogFileName;
    
        while (nowByte) {
        
        if (nowByte>kSCRIPT_API_LENGTH) {
                dialogFileName+=nowByte;
                cursor++;
                nowByte=script[cursor];
        }
        else {
        
            break;
                
        }
        
        }
        
        if (dialogFileName.empty())
                return;
        
        gameEngine->loadAndShowDialog(dialogFileName.c_str());
        
    }
    
}

bool ScriptEngine::ifTouchedBy(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor) {
    
    char nowByte=script[cursor];
    cursor++;

    if (!nowObject->isTouched())
        return false;
    
    string touchedByName;
    
    while (nowByte) {
    
    if (nowByte>kSCRIPT_API_LENGTH) {
        touchedByName+=nowByte;
        nowByte=script[cursor];
        cursor++;
    }
    else {
        
        if (touchedByName.empty())
            return false;
        if (!nowObject->getTouchedBy())
            return false;
        if (nowObject->getTouchedBy()->getName().empty())
            return false;
        
        const char* checkName=nowObject->getTouchedBy()->getName().c_str();
        
        if (strncmp(touchedByName.c_str(),checkName,kShortStringLength)==0) {
            //TODO add AND OR check
            ScriptEngine::performCommand(script,nowObject,gameEngine,cursor);
            return true;
        }
        else {
            return false;
        }
    }
    
    }
    
}

bool ScriptEngine::ifCalledBy(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor) {
    
    char nowByte=script[cursor];
    cursor++;

    if (!nowObject->isCalled())
        return false;
    
    string calledByName;
    
    while (nowByte) {
    
    if (nowByte>kSCRIPT_API_LENGTH) {
        calledByName+=nowByte;
        nowByte=script[cursor];
        cursor++;
    }
    else {
        
        if (calledByName.empty())
            return false;
        if (!nowObject->getCalledBy())
            return false;
        if (nowObject->getCalledBy()->getName().empty())
            return false;
        
        const char* checkName=nowObject->getCalledBy()->getName().c_str();
        
        if (strncmp(calledByName.c_str(),checkName,kShortStringLength)==0) {
            //TODO add AND OR check
            ScriptEngine::performCommand(script,nowObject,gameEngine,cursor);
            return true;
        }
        else {
            
            return false;
            
        }
    }
    
    }
    
}


bool ScriptEngine::checkIf(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor) {
    
    char nowByte=script[cursor];
    cursor++;

    if (nowByte==kSCRIPT_ENGINE_TOUCHED_BY) {
        
        return ScriptEngine::ifTouchedBy(script,nowObject,gameEngine,cursor);
        
    }
    else if (nowByte==kSCRIPT_ENGINE_CALLED_BY) {
        
        return ScriptEngine::ifCalledBy(script,nowObject,gameEngine,cursor);
        
    }
    
    return false;
    
}

void ScriptEngine::getFirstOperator(const char *script, GameObject *nowObject, GameEngine *gameEngine, int cursor) {

    char nowByte=kSCRIPT_ENGINE_NULL;

    while (true) {

        nowByte=script[cursor];
        
        if (nowByte==kSCRIPT_ENGINE_NULL) {
            debugLog("Stopped at cursor: "<<cursor<<"\n");
            break;
        }

        cursor++;
        
        if (nowByte==kSCRIPT_ENGINE_IF) {
                ScriptEngine::checkIf(script,nowObject,gameEngine,cursor);
        }
        

        
    }
    
    
}

void ScriptEngine::runScriptForObject(const char *script, GameObject *nowObject, GameEngine *gameEngine) {
    
    int cursor=0;
    //pass to first operator checker
    ScriptEngine::getFirstOperator(script,nowObject,gameEngine,cursor);
    
}

void ScriptEngine::compileString(char* inputString, char* outputScriptCode) {
    
    char scriptCode[kMaxScriptLength];
    int scriptCodeCursor=0;
    
    //TODO syntax check
    //split human code by whitespace
    
    vector<string>parsedHumanCode=ScriptEngine::splitTextToVectorByWhitespace(inputString);
    
    //parse
    for (int a=0;a<parsedHumanCode.size();a++) {
        
        const char *nowHumanCode=parsedHumanCode[a].c_str();
        
        for (int b=1;b<kSCRIPT_API_LENGTH;b++) {
            
            const char *nowScriptEngineAPICode=scriptEngineAPIhumanLanguage[b];
            
            if (strncmp(nowHumanCode,nowScriptEngineAPICode,kMaxScriptLength)==0) {
                
                //add script code
                //increase
                scriptCode[scriptCodeCursor]=b;
                scriptCodeCursor++;
                break;
                
            }
            else if (b==kSCRIPT_API_LENGTH-1) {
                //copy whole code to script
                //assume string NULL terminated
                
                int c=0;
                const char *stringToCopy=nowHumanCode;
                
                for (;scriptCodeCursor<kMaxScriptLength-1;scriptCodeCursor++) {
                    
                    if (stringToCopy[c]) {
                        scriptCode[scriptCodeCursor]=stringToCopy[c];
                    }
                    else {
                        break;
                    }
                    c++;
                }
            }
            
        }
        
    }
    
    //null terminate
    scriptCode[scriptCodeCursor]=kSCRIPT_ENGINE_NULL;
    
#ifdef DEBUG
    printf("Script code cursor terminate at: %d\n",scriptCodeCursor);
#endif
    
    strncpy(outputScriptCode,scriptCode,kMaxScriptLength);

#ifdef DEBUG
    printf("Human code:%s\nScript code:%s\n",inputString,outputScriptCode);
#endif    
    
}


