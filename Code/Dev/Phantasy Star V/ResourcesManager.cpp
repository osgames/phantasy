/* 
 * File:   ResourcesManager.cpp
 * Author: User
 * 
 * Created on May 6, 2012, 1:01 AM
 */

//TODO add help messages

#include "ResourcesManager.h"


ResourcesManager::ResourcesManager() {
}

ResourcesManager::ResourcesManager(const ResourcesManager& orig) {
}

ResourcesManager::~ResourcesManager() {
}

SDL_Surface* ResourcesManager::loadNoneBattleBackground() {

    char backgroundFilePath[kMaxStringLength];
    snprintf(backgroundFilePath,kMaxStringLength,"%s/%s/%s",kGameModulesPath,kGameModuleSystemModulePath,kBattleBackgroundNonePath);
    
    debugLog("Load none background:"<<backgroundFilePath<<"\n");
    
    return IMG_Load(backgroundFilePath);
    
}

SDL_Surface* ResourcesManager::loadBattleBackgroundWithName(string battleBackgroundName, string gameModuleName) {
    
    if (battleBackgroundName=="None") return ResourcesManager::loadNoneBattleBackground();
    
    char backgroundFilePath[kMaxStringLength];
    snprintf(backgroundFilePath,kMaxStringLength,"%s/%s/%s/%s.%s",kGameModulesPath,gameModuleName.c_str(),kBattleBackgroundsDirectory,battleBackgroundName.c_str(),kBattleBackgroundImageExtension);
    
    return IMG_Load(backgroundFilePath);
    
}

string ResourcesManager::selectGameModule(SDL_Surface *drawSurface,InputController *inputController) {
    
    //TODO rewrite this mess
    //parse modules to menu
    GameModulesManager *gameModulesManager=new GameModulesManager();
    gameModulesManager->refreshModulesList();
    
    GameModuleSettings **gameModulesList=gameModulesManager->getGameModulesList();
    
    char *gameModulesListNames[kMaxGameModules];
    gameModulesListNames[0]="..";//for exit
    int modulesCount=1;
    for (int a=0;a<kMaxGameModules;a++)
        if (gameModulesList[a]==NULL)
            break;
        else {
            gameModulesListNames[modulesCount]=gameModulesList[a]->getTitle();
            modulesCount++;
        }
    gameModulesListNames[modulesCount]=NULL;
    

    
    debugLog("Modules count:"<<modulesCount<<"\n");
    
    MenuRender::makeMenu(gameModulesListNames, 220, 180, drawSurface);
    //MenuController *menuController=new MenuController(inputController);
    int choice=MenuController::returnChoice(modulesCount, 220, 180, drawSurface,inputController);
    
    //TODO rewrite this
    if (choice==0) choice=kMenuChoiceExit;
    
    if (choice==kMenuChoiceExit) {
        delete(gameModulesManager);        
        return string("");
    }
    else {
        char *modulePath=gameModulesList[choice-1]->getPath();
        
        debugLog("Module path:"<<modulePath<<"\n");
        
        string outputString;
        if (modulePath!=NULL)
                outputString=modulePath;
        else {
            
            outputString=string("");
            debugLog("Game module path are NULL!\n");
        }
                

        delete(gameModulesManager);        
        return outputString;
    }
    

    
}

void ResourcesManager::loadObject(char* objectDirectory, vector<GameObject*>&gameObjects) {
    
    GameObject *loadedObject=NULL;
    loadedObject=GameObject::loadObjectFromFile(objectDirectory);
    if (!loadedObject) debugLog("Loaded object are NULL\n");
    gameObjects.push_back(loadedObject);
        

}

void ResourcesManager::loadTile(char* tileDirectory, vector<Tile*>&tiles) {
    
    
    char tilePath[kMaxStringLength];
    snprintf(tilePath,kMaxStringLength,"%s/%s",tileDirectory,kTilesetFileDefaultName);

    char configPath[kMaxStringLength];
    snprintf(configPath,kMaxStringLength,"%s/%s",tileDirectory,kGameModuleConfigFileName);
    
    map<string,string>tileConfigParsed=ConfigParser::parseConfigFile(configPath,NULL);    

    //check tileType
    
    int tileType=kTileTypeUnknown;
    string rawTileType=tileConfigParsed[kTileParserKeyTileType];
    
    if (!rawTileType.empty())
        debugLog("raw tile type: "<<rawTileType<<"\n");
    
    if (rawTileType==kTileParserTypeValueGround)
        tileType=kTileTypeGround;
    else if (rawTileType==kTileParserTypeValueRoad)
        tileType=kTileTypeRoad;
    else if (rawTileType==kTileParserTypeValueWall)
        tileType=kTileTypeWall;
    
    

    if (tileType==kTileTypeUnknown) {
        debugLog("No tile type for config on path:"<<configPath<<"\n");
        return;
    }
    
    debugLog("Load tile on path: "<<tilePath<<"\n");
    Tile *loadedTile=new Tile();    

    loadedTile->setImage(tilePath);
    
    if (loadedTile->getImage()!=NULL) {
     
        debugLog("Tileset load success for path: "<<tilePath<<"\n");
        loadedTile->setTileType(tileType);
        loadedTile->setFullPath(tileDirectory);
        tiles.push_back(loadedTile);
        
    }
    else
        debugLog("Tileset load error for path: "<<tilePath<<"\n");
}

void ResourcesManager::selectObjectForGameModulePath(string gameModuleChoicePath, SDL_Surface* drawSurface, InputController* inputController, vector<GameObject*>&gameObjects)
{
    
    char gameModuleObjectsDirectory[kMaxStringLength];
    snprintf(gameModuleObjectsDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModuleChoicePath.c_str(),kGameModuleObjectsDirectory);   
 
    debugLog("Select objects from directory: "<<gameModuleObjectsDirectory<<"\n");    
 
    map<string,string>objectsConfigs[kMaxObjects];
    FileSystemManager::getParsedConfigFilesFromParentDirectory(gameModuleObjectsDirectory,objectsConfigs);    
    
    //TODO preview
    char *objectsListNames[kMaxObjects];
    objectsListNames[0]="..";//for exit
    int objectsCount=1;

    for (int a=0;a<kMaxObjects;a++)

        if (!objectsConfigs[a]["name"].empty()) {
            objectsListNames[objectsCount]=(char*)malloc(sizeof(char)*kMaxStringLength);
            snprintf(objectsListNames[objectsCount],kMaxStringLength,"%s",objectsConfigs[a]["name"].c_str());
            objectsCount++;
        }
    objectsListNames[objectsCount]=NULL;    
    
    MenuRender::makeMenu(objectsListNames, 240, 220, drawSurface);
    //MenuController *menuController=new MenuController(inputController);
    int choice=MenuController::returnChoice(objectsCount, 240, 220, drawSurface, inputController);   
  
    //rewrite this
    for (int a=1;a<kMaxObjects;a++)
        if (objectsListNames[a]!=NULL)
                free(objectsListNames[a]);
        else
            break;    
 
    if (choice!=kMenuChoiceExit) {

        choice--;
        //load tile and return
        const char *selectedObjectDirectory=objectsConfigs[choice]["system_pathForConfigParser"].c_str();
        debugLog("Selected object:"<<selectedObjectDirectory<<"\n");
        //TODO load tile and return
        char objectFullPath[kMaxStringLength];
        snprintf(objectFullPath,kMaxStringLength,"%s/%s/%s/%s",kGameModulesPath,gameModuleChoicePath.c_str(),kGameModuleObjectsDirectory,selectedObjectDirectory);
        ResourcesManager::loadObject(objectFullPath,gameObjects);
        
    }    
    
}


void ResourcesManager::selectTileForGameModulePath(string gameModuleChoicePath, SDL_Surface* drawSurface, InputController* inputController, vector<Tile*>&tiles)
{
    
    char gameModuleTilesDirectory[kMaxStringLength];
    snprintf(gameModuleTilesDirectory,kMaxStringLength,"%s/%s/%s",kGameModulesPath,gameModuleChoicePath.c_str(),kGameModuleTilesDirectory);
    
    debugLog("Select tiles from directory: "<<gameModuleTilesDirectory<<"\n");
    
    
    //TODO right length for getParsedConfigFilesFromParentDirectory
    map<string,string>tilesConfigs[kMaxTiles];
    FileSystemManager::getParsedConfigFilesFromParentDirectory(gameModuleTilesDirectory,tilesConfigs);
    
    //TODO preview
    char *tilesListNames[kMaxTiles];
    tilesListNames[0]="..";//for exit
    int tilesCount=1;

    for (int a=0;a<kMaxTiles;a++)

        if (!tilesConfigs[a]["name"].empty()) {
            tilesListNames[tilesCount]=(char*)malloc(sizeof(char)*kMaxStringLength);
            snprintf(tilesListNames[tilesCount],kMaxStringLength,"%s",tilesConfigs[a]["name"].c_str());
            tilesCount++;
        }
    tilesListNames[tilesCount]=NULL;    
    
    MenuRender::makeMenu(tilesListNames, 240, 220, drawSurface);
    //MenuController *menuController=new MenuController(inputController);
    int choice=MenuController::returnChoice(tilesCount, 240, 220, drawSurface, inputController);   
    
    //rewrite this
    for (int a=1;a<kMaxTiles;a++)
        if (tilesListNames[a]!=NULL)
                free(tilesListNames[a]);
        else
            break;
    
    if (choice!=kMenuChoiceExit) {

        choice--;
        //load tile and return
        const char *selectedTileDirectory=tilesConfigs[choice]["system_pathForConfigParser"].c_str();
        debugLog("Selected tile:"<<selectedTileDirectory<<"\n");
        //TODO load tile and return
        char tilesFullPath[kMaxStringLength];
        snprintf(tilesFullPath,kMaxStringLength,"%s/%s/%s/%s",kGameModulesPath,gameModuleChoicePath.c_str(),kGameModuleTilesDirectory,selectedTileDirectory);
        ResourcesManager::loadTile(tilesFullPath,tiles);
        
    }
    

    
}

int ResourcesManager::addObjects(SDL_Surface *drawSurface, InputController *inputController, vector<GameObject*>&gameObjects) {
    
    string gameModuleChoicePath=ResourcesManager::selectGameModule(drawSurface,inputController);
    
    if (!gameModuleChoicePath.empty()) {
        debugLog("Selected game module: "<<gameModuleChoicePath<<"\n");
        
        ResourcesManager::selectObjectForGameModulePath(gameModuleChoicePath,drawSurface,inputController, gameObjects);
        return kResultContinue;
    }
    else {
        debugLog("Exit from game module choice\n");
        return kResultExit;
    }
    
}


int ResourcesManager::addTiles(SDL_Surface *drawSurface, InputController *inputController, vector<Tile*>&tiles) {
    
    string gameModuleChoicePath=ResourcesManager::selectGameModule(drawSurface,inputController);
    
    if (!gameModuleChoicePath.empty()) {
        debugLog("Selected game module: "<<gameModuleChoicePath<<"\n");
        
        ResourcesManager::selectTileForGameModulePath(gameModuleChoicePath,drawSurface,inputController, tiles);
        return kResultContinue;
    }
    else {
        debugLog("Exit from game module choice\n");
        return kResultExit;
    }
    
}
