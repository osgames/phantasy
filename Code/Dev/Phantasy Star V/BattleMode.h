/* 
 * File:   BattleMode.h
 * Author: User
 *
 * Created on July 6, 2012, 11:31 PM
 */

#ifndef BATTLEMODE_H
#define	BATTLEMODE_H

#include <map>
#include <vector>
#include <string>
#include "Fighter.h"
#include "debuglog.h"
#include "GameplayMenuMaster.h"
#include "RandomGenerator.h"
#include "InputController.h"

#include "GameItemsDatabase.h"
#include "GameMagicDatabase.h"
#include "GameTechniqueDatabase.h"
#include "GameSkillsDatabase.h"

using namespace std;

class BattleMode {
public:
    BattleMode(vector<Fighter>allFighters, SDL_Surface *newDrawSurface, InputController *newInputController,string newGameModule,GameItemsDatabase *newGameItemsDatabase, GameMagicDatabase *newGameMagicDatabase, GameTechniqueDatabase *newGameTechniqueDatabase, SDL_Surface *newBattleModeBackgroundImage);
    BattleMode(const BattleMode& orig);
    virtual ~BattleMode();
    
    static BattleModeResult startBattleMode(PlayerTeam *playerTeam, string enemiesSet, SDL_Surface *newDrawSurface, InputController *newInputController, string newGameModule, GameItemsDatabase *gameItemsDatabase, GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase, string newBattleModeBackgroundString);
    static vector<Fighter> loadFightersFromEnemiesSet(string enemiesSet, string gameModulePath, GameItemsDatabase *gameItemsDatabase,GameMagicDatabase *gameMagicDatabase, GameTechniqueDatabase *gameTechniqueDatabase, GameSkillsDatabase *gameSkillsDatabase);
    
private:

    BattleModeResult battleModeLoop();
    
    BattleModeAction getPlayerActionForFighter(Fighter *nowFighter, int nowFighterIndex);
    BattleModeAction getEnemyActionForFighter(Fighter *nowFighter, int nowFighterIndex);
    BattleModeAction getNeutralActionForFighter(Fighter *nowFighter, int nowFighterIndex);
    BattleModeAction getActionForFighter(Fighter *nowFighter, int nowFighterIndex);
    
    BattleModeAction getCommandForFighter(Fighter *nowFighter, int nowFighterIndex);
    
    void showBattleModeLose();
    void showBattleModeVictory();
    void showFightersStatus();
    
    void drawBattleModeBackground();
    
    void playAction(BattleModeAction nowAction);
    void playAttackAction(BattleModeAction nowAction);
    
    vector<Fighter>fighters;
    InputController *inputController;
    SDL_Surface *drawSurface;
    string gameModule;
    
    string battleModeBackgroundString;
    SDL_Surface *battleModeBackgroundImage;
    
    GameItemsDatabase *gameItemsDatabase;
    GameMagicDatabase *gameMagicDatabase;
    GameTechniqueDatabase *gameTechniqueDatabase;

    
};

#endif	/* BATTLEMODE_H */

