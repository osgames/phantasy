/* 
 * File:   SimpleRender.h
 * Author: User
 *
 * Created on March 8, 2012, 10:54 PM
 */

#include "Tile.h"
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include "GameObject.h"
#include "debuglog.h"
#include "Const.h"
#include <vector>
#include "debuglog.h"

#ifndef SIMPLERENDER_H
#define	SIMPLERENDER_H

using namespace std;

class RenderLayer {
public:
    RenderLayer();
    RenderLayer(const RenderLayer& orig);
    virtual ~RenderLayer();
    
    SDL_Thread* render(int cameraX, int cameraY, SDL_Surface* drawSurface, int maximumX, int maximumY, int destinationX, int destionationY);
    static void drawTile(int index,int drawX,int drawY,SDL_Surface *drawSurface, vector<Tile*>*tiles);
    static int renderInThread(void *data);
    static void renderSystem(int cameraX, int cameraY, SDL_Surface *drawSurface, int maximumX, int maximumY, int **map, vector<Tile*> *tiles, vector<GameObject*>&objects, int destinationX, int destinationY, bool loopMap);
    void loadTile(Tile *newTile);
    void loadObject(GameObject *newObject);
    static void drawObject(GameObject *nowObject,SDL_Surface *drawSurface,int cameraX,int cameraY,vector<GameObject*>&objects);
    void setMap(int **newMap);
    
    void setTiles(vector <Tile*> *newTiles);
    void removeAllObjects();
    
    int getObjectsCount();
    GameObject *getGameObject(int objectIndex);
    void removeObjectAtIndex(int objectIndex);
    void removeTilesetFromRenderLayer(int tilesetIndex);
    
    int getTileIndexAt(int mapX, int mapY);
    bool loopMap;
    
    void setLoopMap(bool newLoopMap);
    
    
private:


    vector <Tile*>*tiles;
    
    vector<GameObject*>objects;
    
    int **map;
    
    SDL_Thread *renderThread;
    
};

#endif	/* SIMPLERENDER_H */

